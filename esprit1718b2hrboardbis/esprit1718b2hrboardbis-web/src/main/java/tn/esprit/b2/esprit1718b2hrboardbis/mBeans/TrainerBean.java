package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceLocal;


@ViewScoped
@ManagedBean(name="trainerBean")
public class TrainerBean {
	@EJB
	private TrainerServiceLocal trainerServiceLocal;
	@EJB
	private SeanceServiceLocal seanceServiceLocal;
	private Trainer trainer;
	private List<Trainer> trainers;
	private String domain;
	private List<String> domaines;
	private boolean showForm;
	
	@PostConstruct
	private void init(){
		trainer = new Trainer();
		trainers= trainerServiceLocal.findAll();
		domaines= trainerServiceLocal.findAllDomains();
		showForm=false;
	}
	
	public void doSelect(){
		showForm=true;
	}
	
	public void doCancel(){
		showForm=false;
	}
	
	public void doAddTrainer(){
		trainerServiceLocal.update(trainer);
	}
	
	public String doDeleteTrainer(){
		trainerServiceLocal.delete(trainer);
		seanceServiceLocal.deleteWhenTrainerDeleted(trainer);
		trainers=trainerServiceLocal.findAll();
		showForm=false;
		return"";
	}
	
	public boolean isShowForm(){
		return showForm;
	}
	
	public void setShowForm(boolean showForm){
		this.showForm=showForm;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public List<Trainer> getTrainers() {
		return trainers;
	}

	public void setTrainers(List<Trainer> trainers) {
		this.trainers = trainers;
	}

	public List<String> getDomaines() {
		return domaines;
	}

	public void setDomaines(List<String> domaines) {
		this.domaines = domaines;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	public void rechercheByDomain(){
		List<Trainer> newListTrainers=new ArrayList<Trainer>();
			for (Trainer trainer : trainers) {
				if(trainer.getDomain().equalsIgnoreCase(domain))
					{newListTrainers.add(trainer);}
			}
			trainers=newListTrainers;
		
	}
	
	public void retablir(){
		trainers=trainerServiceLocal.findAll();
	}
}
