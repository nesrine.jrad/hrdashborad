package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;


import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskPK;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceLocal;

@ManagedBean
@ApplicationScoped
public class MyTasksBean {
	
	@EJB
	ProjectServiceLocal projectServiceLocal;
	
	@EJB
	TaskServiceLocal taskServiceLocal;
	
	@EJB
	EmployeeServiceLocal employeeServiceLocal;
	
	
	
	private boolean showForm;
	private int idProject;
	private int idUser;
	
	private String selectedOption="ToDO";
	private List<Task> myTasks;
	
	private Task taskToUpdate;
	
	@PostConstruct
	void init(){
		myTasks = new ArrayList<Task>();
		showForm = false;
		idProject = 0;
	}
	
	public String doGetMyTasks(int idProject) {
		this.idProject = idProject;
		this.myTasks = taskServiceLocal.getTasksByEmployeeAndStatusTask(idUser,StatusTask.ToDO,idProject);
		return "/myTasks.jsf?faces-redirect=true";
	}
	
	public String doGetMyTasks() {
		myTasks = taskServiceLocal.getTasksByEmployeeAndStatusTask(idUser,StatusTask.ToDO);
		System.out.print(myTasks.size());
		return "/myTasks.jsf?faces-redirect=true";
	}
	

	public List<Task> getMyTasks() {
		return myTasks;
	}

	public void setMyTasks(List<Task> myTasks) {
		this.myTasks = myTasks;
	}
	
	public String doGetMyTasksByStatus(){
		myTasks = taskServiceLocal.getTasksByEmployeeAndStatusTask(idUser, StatusTask.valueOf(selectedOption));
		return "/myTasks.jsf?faces-redirect=true";
	}
	
	public String doGetMyTasksByStatus(int idProject){
		myTasks = taskServiceLocal.getTasksByEmployeeAndStatusTask(idUser, StatusTask.valueOf(selectedOption),idProject);
		return "/myTasks.jsf?faces-redirect=true";
	}


	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}
	
	
	public void selectTask(String name,int idProject){
		showForm = !showForm;
		TaskPK taskPK = new TaskPK(idProject,idUser,name);
		taskToUpdate = taskServiceLocal.find(taskPK);
	}

	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}

	public Task getTaskToUpdate() {
		return taskToUpdate;
	}

	public void setTaskToUpdate(Task taskToUpdate) {
		this.taskToUpdate = taskToUpdate;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	
	public String doUpdate(){
		if(selectedOption.equals("DONE")){
			taskToUpdate.setDateEnd(Date.valueOf(LocalDate.now()));
		}
		else{	
			taskToUpdate.setStatusTask(StatusTask.valueOf(selectedOption));
		}
		taskServiceLocal.save(taskToUpdate);
		showForm = false;
		return "";
	}
	
	

}
