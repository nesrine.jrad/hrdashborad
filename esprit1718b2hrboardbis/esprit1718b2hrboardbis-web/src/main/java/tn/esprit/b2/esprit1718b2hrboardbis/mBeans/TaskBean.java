package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;


import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;


import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceLocal;

@ManagedBean
@ApplicationScoped
public class TaskBean {
	
	@EJB
	TaskServiceLocal taskServiceLocal;
	
	@EJB
	ProjectServiceLocal projectServiceLocal;
	
	@EJB
	EmployeeServiceLocal employeeServiceLocal;
	
	private int idProject;
	private int idEmployee;
	
	private String name;
	private String description;
	private Date dateTaskBeginning;
	private Date dateExpectedEnd;
	
	private Project project;
	private List<Task> tasks;
	
	private List<Employee> employees;
	
	private boolean showFormAdd;
	
	

	public int getIdProject() {
		return idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	@PostConstruct
	void init(){
		showFormAdd=false;
		
	}
	
	
	

	public boolean isShowFormAdd() {
		return showFormAdd;
	}

	public void setShowFormAdd(boolean showFormAdd) {
		this.showFormAdd = showFormAdd;
	}
	
	public void toogleDisplayForm(){
		showFormAdd = !showFormAdd;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateTaskBeginning() {
		return dateTaskBeginning;
	}

	public void setDateTaskBeginning(Date dateTaskBeginning) {
		this.dateTaskBeginning = dateTaskBeginning;
	}

	
	
	public Date getDateExpectedEnd() {
		return dateExpectedEnd;
	}

	public void setDateExpectedEnd(Date dateExpectedEnd) {
		this.dateExpectedEnd = dateExpectedEnd;
	}

	public String doGetListTasks(int idProject){
		
		project = projectServiceLocal.find(idProject);
		employees = projectServiceLocal.getListEmployees(idProject);
		tasks = taskServiceLocal.getTasksByProject(project);
		return"/projectDetails.jsf?faces-redirect=true";
	}
	
	public void doAddTask(){
		java.sql.Date date1 = new java.sql.Date(dateTaskBeginning.getTime());
		Employee employee = employeeServiceLocal.find(idEmployee);
		Task task = new Task(project, employee, name, date1, description);
		taskServiceLocal.save(task);
		tasks = taskServiceLocal.getTasksByProject(project);
		showFormAdd=false;
	}
	
	
	
	

}
