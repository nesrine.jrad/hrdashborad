package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceLocal;

@ManagedBean
@ViewScoped
public class TrainingBean {
	@EJB
	private TrainingServiceLocal trainingServiceLocal;
	@EJB
	private SeanceServiceLocal seanceServiceLocal;
	private Training training;
	private List<Training> trainings;
	private boolean showForm;
	
	private String title;
	private List<String> titles;
	
	@PostConstruct
	private void init(){
		training = new Training();
		trainings= trainingServiceLocal.findAll();
		titles= trainingServiceLocal.findAllTitles();
		showForm=false;
	}
	
	public void doSelect(){
		showForm=true;
	}
	
	
	public void doCancel(){
		showForm=false;
	}
	
	public void doAddTraining(){
		trainingServiceLocal.update(training);
	}
	
	public String doDeleteTraining(){
		trainingServiceLocal.delete(training);
		seanceServiceLocal.deleteWhenTrainingDeleted(training);
		trainings=trainingServiceLocal.findAll();
		showForm=false;
		return"";
	}
	
	public boolean isShowForm(){
		return showForm;
	}
	
	public void setShowForm(boolean showForm){
		this.showForm=showForm;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public List<Training> getTrainings() {
		return trainings;
	}

	public void setTrainings(List<Training> trainings) {
		this.trainings = trainings;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getTitles() {
		return titles;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}
	
	public void rechercheByTitle(){
		List<Training> newListTrainings=new ArrayList<Training>();
			for (Training training : trainings) {
				if(training.getTitle().equalsIgnoreCase(title))
					{newListTrainings.add(training);}
			}
			trainings=newListTrainings;
		
	}
	
	public void retablir(){
		trainings=trainingServiceLocal.findAll();
	}
	
}
