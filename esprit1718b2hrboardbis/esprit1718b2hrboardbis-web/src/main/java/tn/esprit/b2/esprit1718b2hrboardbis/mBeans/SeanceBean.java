package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

///////////////////////////////Excel///////////////////////
/*
import java.io.File;
import java.io.IOException;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import jxl.write.WriteException;

import jxl.write.biff.RowsExceededException;*/
////////////////////////////////////////////////////////////

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.SeancePk;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceLocal;
import utilities.SendMail;

@ViewScoped
@ManagedBean(name="seanceBean")
public class SeanceBean {
	@EJB
	private SeanceServiceLocal seanceServiceLocal;
	@EJB
	private EmployeeServiceLocal employeeServiceLocal;
	@EJB
	private TrainingServiceLocal trainingServiceLocal;
	@EJB
	private TrainerServiceLocal trainerServiceLocal;
	private Seance seance;
	private SeancePk seancePk;
	private List<Seance> seances;
	private List<Employee> employees;
	public int[] data ;
	private boolean showForm;
	
	private String title;
	private List<String> titles;
	
	private List<Seance> seancesEmployee;
	private String titleEmployee;
	private List<String> titlesEmployee;
	private Training training;
	
	@PostConstruct
	private void init(){
		seance = new Seance();
		seancePk=new SeancePk();
        employees=employeeServiceLocal.findAll();
        seances=seanceServiceLocal.findAll();
        titles= trainingServiceLocal.findAllTitles();
        seancesEmployee=seanceServiceLocal.findByEmployee(Identity.connectedID);
        titlesEmployee=new ArrayList<String>();
        for (Seance s : seancesEmployee) {
			if(s.getSeancePk().getIdEmployee()==Identity.connectedID){
				training=trainingServiceLocal.find(s.getSeancePk().getIdTraining());
				if (!titlesEmployee.contains(training.getTitle())){
					titlesEmployee.add(training.getTitle());
				}
			}
		}
		showForm=false;
	}
	
	public void doSelect(){
		showForm=true;
	}
	
	public void doCancel(){
		showForm=false;
	}
	
	public void doAddSeance2(){
			seance.setSeancePk(seancePk);
			seanceServiceLocal.update(seance);
	}
	
	public void doAddSeanceWithMail(){
		int i=0;
		for (i=0;i<data.length;i++) {
			
			seancePk.setIdEmployee(data[i]);
			seance.setSeancePk(seancePk);
			
			//service Mail///////////////////////////////////////////////////
			SendMail mail=new SendMail();
			String objectMail="Vous avez été affecté à la formation "+
			trainingServiceLocal.find(seance.getSeancePk().getIdTraining()).getTitle()+
			", qui sera animé par notre formateur "+
			trainerServiceLocal.find(seance.getSeancePk().getIdTrainer()).getName()+" "+
			trainerServiceLocal.find(seance.getSeancePk().getIdTrainer()).getFamilyName()+"."
			+"Date de formation : "+seance.getSeancePk().getDateSeance()+"."
			+"Salle : "+seance.getSalle()+".";
			System.out.println("#######################");
			System.out.println(employeeServiceLocal.find(data[i]).getEmail());
			System.out.println("#######################");
			mail.sendmail("houssem.hamila@gmail.com",
					employeeServiceLocal.find(data[i]).getEmail(),
					"ris2k01forlife",
					objectMail);
			//////////////////////////////////////////////////////////////////
			seanceServiceLocal.update(seance);
		}
	}
	
	/*
	  public void doAddSeanceWithExcel(){
	 
		int i=0;
		for (i=0;i<data.length;i++) {
			////////////////////////Excel//////////////////////////////
			WritableWorkbook workbook = null;
			try {
				workbook = Workbook.createWorkbook(new File("D:\\Suivie.xlsx"));
				
				WritableSheet sheet = workbook.createSheet("test", 0); 
				
				Label label = new Label(0, 0, "position A1");
				sheet.addCell(label);

				Number number = new jxl.write.Number(0, 1, 3.1459);
				sheet.addCell(number); 
				
				workbook.write(); 
				
			} 
			catch (IOException e) {
				e.printStackTrace();
			} 
			catch (RowsExceededException e) {
				e.printStackTrace();
			}
			catch (WriteException e) {
				e.printStackTrace();
			}
			finally {
				if(workbook!=null){
					try {
						workbook.close();
					} 
					catch (WriteException e) {
						e.printStackTrace();
					} 
					catch (IOException e) {
						e.printStackTrace();
					} 
				}
			}

			//////////////////////////////////////////////////////////
			seancePk.setIdEmployee(data[i]);
			seance.setSeancePk(seancePk);
			seanceServiceLocal.update(seance);
		}
	}
	*/
	
	public String doDeleteSeance(){
		seanceServiceLocal.deleteMyWay(seance.getSeancePk());
		seances=seanceServiceLocal.findAll();
		showForm=false;
		return"";
	}
	
	public boolean isShowForm(){
		return showForm;
	}
	
	public void setShowForm(boolean showForm){
		this.showForm=showForm;
	}

	public Seance getSeance() {
		return seance;
	}

	public void setSeance(Seance seance) {
		this.seance = seance;
	}

	public List<Seance> getSeances() {
		return seances;
	}

	public void setSeances(List<Seance> seances) {
		this.seances = seances;
	}

	public SeancePk getSeancePk() {
		return seancePk;
	}

	public void setSeancePk(SeancePk seancePk) {
		this.seancePk = seancePk;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public int[] getData() {
		return data;
	}

	public void setData(int[] data) {
		this.data = data;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getTitles() {
		return titles;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}
	
	public void rechercheByTitle(){
		List<Seance> newListSeances=new ArrayList<Seance>();
		Training t=new Training();
			for (Seance seance : seances) {
				t=trainingServiceLocal.find(seance.getSeancePk().getIdTraining());
				if(t.getTitle().equalsIgnoreCase(title))
					{newListSeances.add(seance);}
			}
			seances=newListSeances;
	}
	
	public void rechercheByTitleEmployee(){
		List<Seance> newListSeances=new ArrayList<Seance>();
		Training training=new Training();
		for (Seance seance : seancesEmployee) {
			training=trainingServiceLocal.find(seance.getSeancePk().getIdTraining());
			if(training.getTitle().equalsIgnoreCase(titleEmployee))
			{newListSeances.add(seance);}
		}
		seancesEmployee=newListSeances;
	}
	
	public void retablirEmployee(){
		seancesEmployee=seanceServiceLocal.findByEmployee(Identity.connectedID);
	}
	
	public void retablir(){
		seances=seanceServiceLocal.findAll();
	}

	public List<Seance> getSeancesEmployee() {
		return seancesEmployee;
	}

	public void setSeancesEmployee(List<Seance> seancesEmployee) {
		this.seancesEmployee = seancesEmployee;
	}

	public String getTitleEmployee() {
		return titleEmployee;
	}

	public void setTitleEmployee(String titleEmployee) {
		this.titleEmployee = titleEmployee;
	}

	public List<String> getTitlesEmployee() {
		return titlesEmployee;
	}

	public void setTitlesEmployee(List<String> titlesEmployee) {
		this.titlesEmployee = titlesEmployee;
	}
}
