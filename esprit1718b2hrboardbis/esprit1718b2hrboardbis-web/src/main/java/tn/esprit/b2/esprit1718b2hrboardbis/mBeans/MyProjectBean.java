package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceLocal;

@ManagedBean
@ApplicationScoped
public class MyProjectBean {

	@EJB
	ProjectServiceLocal projectServiceLocal;

	@EJB
	EmployeeServiceLocal employeeServiceLocal;
	
	@EJB
	TaskServiceLocal taskServiceLocal;
	
	private List<Project> myProjects;
	private int idEmployee;
	private int nbToDO;
	private int nbDoing;
	private int nbDone;
	
	
	public String doAssignProjects(){
		myProjects = projectServiceLocal.getProjectsByEmployee(idEmployee);
		nbToDO = taskServiceLocal.getTasksByEmployeeAndStatusTask(idEmployee, StatusTask.ToDO).size();
		nbDoing = taskServiceLocal.getTasksByEmployeeAndStatusTask(idEmployee, StatusTask.DOING).size();
		nbDone = taskServiceLocal.getTasksByEmployeeAndStatusTask(idEmployee, StatusTask.DONE).size();
		return "/myProjects.jsf?faces-redirect=true";
	}

	public List<Project> getMyProjects() {
		return myProjects;
	}

	public void setMyProjects(List<Project> myProjects) {
		this.myProjects = myProjects;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public int getNbToDO() {
		return nbToDO;
	}

	public void setNbToDO(int nbToDO) {
		this.nbToDO = nbToDO;
	}

	public int getNbDoing() {
		return nbDoing;
	}

	public void setNbDoing(int nbDoing) {
		this.nbDoing = nbDoing;
	}

	public int getNbDone() {
		return nbDone;
	}

	public void setNbDone(int nbDone) {
		this.nbDone = nbDone;
	}
	

	
	
}
