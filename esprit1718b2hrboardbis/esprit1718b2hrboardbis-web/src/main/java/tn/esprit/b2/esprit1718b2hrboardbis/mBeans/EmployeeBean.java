package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;

@ManagedBean
@ViewScoped
public class EmployeeBean {
	@EJB
	private EmployeeServiceLocal employeeServiceLocal;
	private Employee employee;
	private List<Employee> employees;
	private boolean showForm;
	
	@PostConstruct
	private void init(){
		employee = new Employee();
		employees= employeeServiceLocal.findAll();
		showForm=false;
	}
	
	public void doSelect(){
		showForm=true;
	}
	
	public void doCancel(){
		showForm=false;
	}
	
	public void doAddEmployee(){
		employeeServiceLocal.update(employee);
	}
	
	public String doDeleteEmployee(){
		employeeServiceLocal.delete(employee);
		employees=employeeServiceLocal.findAll();
		showForm=false;
		return"";
	}
	
	public boolean isShowForm(){
		return showForm;
	}
	
	public void setShowForm(boolean showForm){
		this.showForm=showForm;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
}
