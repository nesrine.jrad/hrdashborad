package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesLocal;

@ManagedBean
@ViewScoped
public class DepartmentBean {
	
	private List<Department> departments;
	@EJB
	private DepartmentServicesLocal departmentServicesLocal;
	
	@PostConstruct
	public void init()
	{
		departments= new ArrayList<Department>();
		departments=departmentServicesLocal.findAll();
		System.out.println("sizedepartment"+departments.size());
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

}
