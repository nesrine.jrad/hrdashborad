package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.AdditionalHour;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.AdditionalHourType;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AdditionalHourServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;

@ManagedBean
@ViewScoped
public class SupplementaryHoursBean {
     
	@EJB
	AdditionalHourServiceLocal additionalHourServiceLocal;
	@EJB
	EmployeeServiceLocal employeeServiceLocal;
	
	List<AdditionalHour> additionalHours;
	AdditionalHour additionalHour;
	AdditionalHourType additionalHourType;
	String type;
	Employee emplyee;
	List<Employee> employees;
	List<AdditionalHour> additionalHoursEmp;
	int id;
	String name;
	java.util.Date dateHeureSupp ;
	private Date dd;
	
	
	@PostConstruct
	private void init() {
		additionalHours = additionalHourServiceLocal.findAll();
	    employees= employeeServiceLocal.findAll();
		additionalHour  = new AdditionalHour();
		emplyee = new Employee();
		dateHeureSupp = new java.util.Date();
		additionalHoursEmp = this.getAdditionalHoursbyEmployee(Identity.connectedID);
	
	}
	
	public String AddSuppHours(){
		
		if(type.equals("DOUBLE")){
			for (Employee employee: employees) {
				String namee=employee.getName()+" "+employee.getFamilyName();
				if(namee.equals(name)){

					AdditionalHour additionalHournew = new AdditionalHour(new java.sql.Date(dateHeureSupp.getTime()),AdditionalHourType.DOUBLE, employee);
					additionalHournew.setDuration(additionalHour.getDuration());
					additionalHourServiceLocal.update(additionalHournew);
					System.out.println("YES 123456798");
				}
			}
		}else{
			for (Employee employee: employees) {
				String namee=employee.getName()+" "+employee.getFamilyName();
				if(namee.equals(name)){

					AdditionalHour additionalHournew = new AdditionalHour(new java.sql.Date(dateHeureSupp.getTime()),AdditionalHourType.SIMPLE, employee);
					additionalHournew.setDuration(additionalHour.getDuration());
					additionalHourServiceLocal.update(additionalHournew);
					System.out.println("YES 123456798");
				}
			System.out.println("SIMPLEEEEEEEE" + type+additionalHour.getDuration() +"   "+ name +" alalalalalal" + dateHeureSupp.toString());
		}
			
		}
		
		
		return "SupplementaryHouHR?faces-redirect=true";
	}

	public String deleteA (AdditionalHour additionalHourD){
		additionalHourServiceLocal.delete(additionalHourD);
		return "/SupplementaryHouHR?faces-redirect=true";
	}
	public List<AdditionalHour> getAdditionalHours() {
		return additionalHourServiceLocal.findAll();
	}

	public void setAdditionalHours(List<AdditionalHour> additionalHours) {
		this.additionalHours = additionalHours;
	}

	public AdditionalHour getAdditionalHour() {
		return additionalHour;
	}

	public void setAdditionalHour(AdditionalHour additionalHour) {
		this.additionalHour = additionalHour;
	}

	public AdditionalHourType getAdditionalHourType() {
		return additionalHourType;
	}

	public void setAdditionalHourType(AdditionalHourType additionalHourType) {
		this.additionalHourType = additionalHourType;
	}

	
	
	public void typeIsDouble(){
		
		this.additionalHourType= AdditionalHourType.DOUBLE;
		System.out.println("++++++++++"+additionalHourType);
	}
	
	public void typeIsSImple(){
		
		this.additionalHourType= AdditionalHourType.SIMPLE;
		System.out.println("++++++++++"+additionalHourType);
		
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void testDisplay()
	{
		System.out.println(type);
		
	}

	public Employee getEmplyee() {
		return emplyee;
	}

	public void setEmplyee(Employee emplyee) {
		this.emplyee = emplyee;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
	public List<Employee> searchEmployee(){
		List<Employee> Allemployees = new ArrayList<>();
		
		
		return Allemployees;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public java.util.Date getDateHeureSupp() {
		return dateHeureSupp;
	}

	public void setDateHeureSupp(java.util.Date dateHeureSupp) {
		this.dateHeureSupp = dateHeureSupp;
	}

	
	public List<AdditionalHour> getAdditionalHoursEmp() {
		return additionalHoursEmp;
	}

	public void setAdditionalHoursEmp(List<AdditionalHour> additionalHoursEmp) {
		this.additionalHoursEmp = additionalHoursEmp;
	}

	public List<AdditionalHour> getAdditionalHoursbyEmployee(int id){
	  List<AdditionalHour> listAdditionalHours =new ArrayList<AdditionalHour>();
		for (AdditionalHour additionalHourEmp : additionalHours) {
			if(additionalHourEmp.getEmployee().getId()==id){
				
				listAdditionalHours.add(additionalHourEmp);
				
			}
		}
		
		return listAdditionalHours;
		
		
	}
	
	public String resultPerSh(AdditionalHourType additionalHourTypeSH, float durationSH){
		if(additionalHourTypeSH.equals(AdditionalHourType.DOUBLE)){
			durationSH *=20;
			String result = durationSH + " TND";
			return result;
		}else{
			durationSH *=15;
			String result = durationSH + " TND";
			return result;
			
		}
		
	}
	
	public String ResultPerMonth(){
		float week=0;
		List<AdditionalHour> additionalHoursEmpc;
		additionalHoursEmpc=this.getAdditionalHoursbyEmployee(Identity.connectedID);;
		java.util.Date today = new java.util.Date();
		 for (AdditionalHour additionalHourl : additionalHoursEmpc) {
			 System.out.println(today.getMonth() +"    +-*/      "+additionalHourl.getDateOfHour().getMonth());
			    if(additionalHourl.getType().equals(AdditionalHourType.DOUBLE)){
			    	if(additionalHourl.getDateOfHour().getYear()==today.getYear()){
			    	 if(additionalHourl.getDateOfHour().getMonth()==today.getMonth())
			    		 
			    	 {
			    		 week+=additionalHourl.getDuration()*20;
			    		 System.out.print("DOUBLE MONTH");
			    		 
			    	 }	
			    }
			    else{
			    	 if(additionalHourl.getType().equals(AdditionalHourType.SIMPLE)){
				    	 if(additionalHourl.getDateOfHour().getMonth()==today.getMonth())
				    	{week+=additionalHourl.getDuration()*15;
				    	 System.out.print("SIMPLE MONTH");}
				    }
			    	
			    }
		}}
		 String resultMonth = week +" ";
		 return resultMonth;
		
	}
	
public String ResultPerYear(){
	float year=0;
	List<AdditionalHour> additionalHoursEmpc;
	additionalHoursEmpc=this.getAdditionalHoursbyEmployee(Identity.connectedID);
	java.util.Date today = new java.util.Date();
	 for (AdditionalHour additionalHourl : additionalHoursEmpc) {
		 System.out.println(today.getYear() +"    ****     "+additionalHourl.getDateOfHour().getYear());
		    if(additionalHourl.getType().equals(AdditionalHourType.DOUBLE)){
		    	 if(additionalHourl.getDateOfHour().getYear()==today.getYear())
		    	{year+=additionalHourl.getDuration()*20;
		    	System.out.println("DOUBLE YEAR");
		    	}
		    }
		    else{
		    	 System.out.print(today.getYear() +"    +-*/      "+additionalHourl.getDateOfHour().getYear());
		    	 if(additionalHourl.getType().equals(AdditionalHourType.SIMPLE)){
			    	 if(additionalHourl.getDateOfHour().getYear()==today.getYear())
			    	{year+=additionalHourl.getDuration()*15;
			    	System.out.println("SIMPLE YEAR");
			    	}
			    }
		    	
		    }
	}
	 String resultYear = year +" ";
	 return resultYear;
	}
	
public void filter() {
	System.out.println(dd+"+-*/89654++++++++++++++++");
	List<AdditionalHour> additionalHourR = new ArrayList<>();
	additionalHours = additionalHourServiceLocal.findAll();
	//java.sql.Date sqlDateF= new java.sql.Date(dd.getTime());
	//System.out.println(sqlDateF+"+-*/89654++++++++++++++++");
	for (Iterator<AdditionalHour> iterator = additionalHours.iterator(); iterator.hasNext(); ) {
	    AdditionalHour value = iterator.next();
	    if(value.getDateOfHour().equals(dd))
		{  
	    	additionalHourR.add(value);
		}
	
	}
	additionalHours=additionalHourR;
	
	
}

public Date getDd() {
	return dd;
}

public void setDd(Date dd) {
	this.dd = dd;
}
	
	
	
	
}
