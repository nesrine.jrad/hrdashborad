package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.sql.Date;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.context.PrimeFacesContext;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.swing.text.StyledEditorKit.BoldAction;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TypeContract;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceLocal;


@ManagedBean
@javax.faces.bean.ViewScoped
//@ApplicationScoped

public class JobOfferBean {
	private boolean delete;
private Integer idJobOffer;
private JobOffer jobOffer;
private List<Department> departments;
private List<JobRequest> jobRequests= new ArrayList<JobRequest>();
private List<String> departmentsName;
private List<JobOffer> jobOffers;
private TypeContract[] contract;
private List<Skill> skillsAdd;
private List<Skill> skillsFinal;
private List<String> time;
private List<Skill> skills;
private Department department= new Department();
private Integer idDepartment;
private String poste;
private String description;
private Float salary;
private String contrat;
private String timeJob;
private Boolean showRequests=false;
@EJB
private SkillServiceLocal skillServiceLocal;
@EJB
private JobOfferServicesLocal jobOfferServicesLocal;
@EJB
private DepartmentServicesLocal DepartmentServicesLocal;
@PostConstruct
public void init()
{	jobOffer= new JobOffer();
    skillsAdd= new ArrayList<Skill>();
    skillsFinal= new ArrayList<Skill>();
      Skill skill= new Skill();
	jobOffers=jobOfferServicesLocal.findAll();
	departments=DepartmentServicesLocal.findAll();
	System.out.println("hii");
    System.out.println("size111"+departments.size());
	System.out.println("size"+jobOffers.size());
	jobOffers.forEach(System.out::println);
	departmentsName=DepartmentServicesLocal.getName();
	System.out.println("size name:"+departmentsName.size());
	contract=TypeContract.values();
	System.out.println(contract.length);
	skills=skillServiceLocal.findAll();
	System.out.println("skills size:"+skills.size());
	delete=false;

}


public List<JobOffer> getJobOffers() {
	return jobOffers;
}

public void setJobOffers(List<JobOffer> jobOffers) {
	this.jobOffers = jobOffers;
}

public List<Department> getDepartments() {
	return departments;
}

public void setDepartments(List<Department> departments) {
	this.departments = departments;
}

public List<String> getDepartmentsName() {
	return departmentsName;
}

public void setDepartmentsName(List<String> departmentsName) {
	this.departmentsName = departmentsName;
}

public TypeContract[] getContract() {
	return contract;
}

public void setContract(TypeContract[] contract) {
	this.contract = contract;
}

public List<String> getTime() {
	return time;
}

public void setTime(List<String> time) {
	this.time = time;
}

public List<Skill> getSkills() {
	return skills;
}

public void setSkills(List<Skill> skills) {
	this.skills = skills;
}

 public Department getDepartment() {
	return department;
}

public void setDepartment(Department department) {
	this.department = department;
}

public Integer getIdDepartment() {
	return idDepartment;
}

public void setIdDepartment(Integer idDepartment) {
	this.idDepartment = idDepartment;
}

public String getPoste() {
	return poste;
}

public void setPoste(String poste) {
	this.poste = poste;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Float getSalary() {
	return salary;
}

public void setSalary(Float salary) {
	this.salary = salary;
}

public String getContrat() {
	return contrat;
}

public void setContrat(String contrat) {
	this.contrat = contrat;
}

public String getTimeJob() {
	return timeJob;
}

public void setTimeJob(String timeJob) {
	this.timeJob = timeJob;
}

public JobOffer getJobOffer() {
	return jobOffer;
}


public void setJobOffer(JobOffer jobOffer) {
	this.jobOffer = jobOffer;
}


public Integer getIdJobOffer() {
	return idJobOffer;
}


public void setIdJobOffer(Integer idJobOffer) {
	this.idJobOffer = idJobOffer;
}


public List<Skill> getSkillsAdd() {
	return skillsAdd;
}


public void setSkillsAdd(List<Skill> skillsAdd) {
	this.skillsAdd = skillsAdd;
}


public String doAddJobOffer()
 { Date dateOfPublishing=java.sql.Date.valueOf(java.time.LocalDate.now());
	
	 System.out.println(salary);
	 System.out.println(poste);
	 System.out.println(description);
	 System.out.println(timeJob);
	 System.out.println(contrat);
	 System.out.println(idDepartment);
	 System.out.println("skills sze"+skillsAdd.size());
	 department=DepartmentServicesLocal.find(idDepartment);
	 //jobOffer= new JobOffer();
	 jobOffer.setDateOfPublishing(dateOfPublishing);
	 jobOffer.setDepartment(department);
	 jobOffer.setSalary(salary);
	 jobOffer.setDescription(description);
	 jobOffer.setName(poste);
	 jobOffer.setTimeOfJob(timeJob);
	 jobOffer.setSkills(skillsAdd);
	 jobOffer.getSkills().forEach(System.out::println);
	 jobOffer.setTypeContrat(TypeContract.valueOf(contrat));
	 
	 //sJobOffer jobOffer= new JobOffer(dateOfPublishing, poste, description, salary, timeJob,TypeContract.valueOf(contrat), department);


	// department=DepartmentServicesLocal.find(entityID)
	// System.out.println(jobOffer.getDepartment().getId());
	// System.out.println(jobOffer.toString());
	 jobOfferServicesLocal.save(jobOffer);
	 return "/JobOfferHome?faces-redirect=true";
 }

public boolean isDelete() {
	return delete;
}


public void setDelete(boolean delete) {
	this.delete = delete;
}


public void doFormulaire(JobOffer jobOffer)

{	System.out.println("111"+delete);
	delete=true;
	this.jobOffer=jobOffer;
	 List<Skill> skillsFinal= new ArrayList<Skill>();
	 for (Skill skill : skills) {
		 List<JobOffer> jobOffers= new ArrayList<JobOffer>();
		 jobOffers.addAll(skill.getLiJobOffers());
		 for (JobOffer jobOffer1 : jobOffers) {
			if(jobOffer1.getId()==jobOffer.getId())
			{
				skillsFinal.add(skill);
			}
		}
	}
	 this.skillsFinal.clear();
	 this.skillsFinal.addAll(skillsFinal);
	 skillsFinal.forEach(System.out::println);
	System.out.println(this.jobOffer.toString());
	System.out.println("222"+delete);
	
}
public String doDelete(JobOffer jobOffer)
{
	System.out.println("suppression");
	jobOfferServicesLocal.delete(jobOffer);
	return "/JobOfferHome?faces-redirect=true";
}
public void doAddSkiil(Skill skill)
{  System.out.println("avant"+skillsAdd.size());
	skillsAdd.add(skill);
	System.out.println("apres"+skillsAdd.size());
}


public List<Skill> getSkillsFinal() {
	return skillsFinal;
}


public void setSkillsFinal(List<Skill> skillsFinal) {
	this.skillsFinal = skillsFinal;
}
public String JobRequestJobOffer(JobOffer jobOffer)
{  
	
	System.out.println("9dima"+jobOffer.getId());
	return "/JobRequestJobOffer?faces-redirect=true"  ;
}




public Boolean getShowRequests() {
	return showRequests;
}


public void setShowRequests(Boolean showRequests) {
	this.showRequests = showRequests;
}	

public List<JobRequest> getJobRequests() {
	return jobRequests;
}


public void setJobRequests(List<JobRequest> jobRequests) {
	this.jobRequests = jobRequests;
}


public void doShowRequests(JobOffer jobOffer)
{  System.out.println(jobOffer.toString());
System.out.println((jobOffer.getJobRequest()).size());
 jobRequests.addAll(jobOffer.getJobRequest());
     jobRequests.forEach(System.out::println);
	showRequests=true;
	delete=false;
}
public void doHideRequest()
{
	showRequests=false;
}
}
