package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Justification;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesLocal;

import utilities.SendMail;

@ManagedBean
@ViewScoped
public class AbsenceBean {
	@EJB
	AbsenceServiceLocal absenceServiceLocal;
	@EJB
	DepartmentServicesLocal departmentServicesLocal;
	private List<Absence> absences;
	private List<Absence> absencesHr;
	private Boolean showForm;
	private Date dd;
	private String mail;
	private String password;
	private String text;
	private String email="kadhem94@gmail.com";
	private String pass="kasouma1994";
	private List<Absence> absences2;
	private PieChartModel pieModel1;
	private BarChartModel barModel;
	
	public List<Absence> getAbsences2() {
		return absences2;
	}

	public void setAbsences2(List<Absence> absences2) {
		this.absences2 = absences2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	

	@PostConstruct
	private void init() {
		absences = new ArrayList<>();
		absences= absenceServiceLocal.getAbsenceByUserId(Identity.connectedID);
		Collections.reverse(absences);
		absences2= this.getAbsencesDepartementManager();
		Collections.reverse(absences2);
		absencesHr =absenceServiceLocal.findAll();
		Collections.reverse(absencesHr);
		showForm = false;
		createPieModels();
		 createBarModels();
	
	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

	public List<Absence> getAbsences() {
		return absences;
	}

	public void setAbsences(List<Absence> absences) {
		this.absences = absences;
	}

	public Boolean getShowForm() {
		return showForm;
	}

	public void setShowForm(Boolean showForm) {
		this.showForm = showForm;
	}
	
	public Date getDd() {
		Date date= new Date();
		return date;
	}
	public void setDd(Date dd) {
		this.dd = dd;
	}
	
	
	public String sendJustification() {
		System.out.println(email + "*********"+ pass);
		SendMail sm =new SendMail();
		sm.sendmail(email, "m2adashbored@gmail.com", pass, "Justify Absence : \n Maladie");
		return "/absencesPage?faces-redirect=true";
	}
	
	
	public void filter() {
		System.out.println(dd+"+-*/89654++++++++++++++++");
		List<Absence> absencesR = new ArrayList<>();
		absences= absenceServiceLocal.getAbsenceByUserId(Identity.connectedID);
		//java.sql.Date sqlDateF= new java.sql.Date(dd.getTime());
		//System.out.println(sqlDateF+"+-*/89654++++++++++++++++");
		for (Iterator<Absence> iterator = absences.iterator(); iterator.hasNext(); ) {
		    Absence value = iterator.next();
		    if(!value.getFromDate().equals(dd))
			{   
			absencesR.add(value);
			}
		
		}
		for (Absence absenceR : absencesR) {
			absences.remove(absenceR);
		}
		
		
	}
	
	public void filter2() {
		System.out.println(dd+"+-*/89654++++++++++++++++");
		List<Absence> absencesR = new ArrayList<>();
		absences= this.getAbsencesDepartementManager();
		for (Iterator<Absence> iterator = absences.iterator(); iterator.hasNext(); ) {
		    Absence value = iterator.next();
		    if(value.getFromDate().equals(dd))
			{   
			absencesR.add(value);
			}
		
		}
		
		absences2=absencesR;
		
		
	}
	
	public void filter3() {
		System.out.println(dd+"+-*/89654++++++++++++++++");
		List<Absence> absencesR = new ArrayList<>();
		
		absences= absenceServiceLocal.findAll();
		//java.sql.Date sqlDateF= new java.sql.Date(dd.getTime());
		//System.out.println(sqlDateF+"+-*/89654++++++++++++++++");
		for (Iterator<Absence> iterator = absences.iterator(); iterator.hasNext(); ) {
		    Absence value = iterator.next();
		    if(value.getFromDate().equals(dd))
			{   
			absencesR.add(value);
			}
		
		}
		absencesHr=absencesR;
		
		
	}
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public List<Absence> getAbsencesDepartementManager() {
		List<Absence> l= new ArrayList<>();
		List<Department> departments = departmentServicesLocal.findAll();
		for (Department department : departments) {
			 for (Employee employee : department.getEmployees()) {
				   
				   if(employee.getId()==Identity.connectedID)
				   {
					
					   for (Employee employee2 : department.getEmployees()) {
						     if(employee2.getId()!=Identity.connectedID)
						    	 l.addAll(absenceServiceLocal.getAbsenceByUserId(employee2.getId()));
					}
					  
				   }
				   
			}
			   
			   
		}
		
		return absences2 =l;
	}

	public List<Absence> getAbsencesHr() {
		return absencesHr;
	}

	public void setAbsencesHr(List<Absence> absencesHr) {
		this.absencesHr = absencesHr;
	}
	
	public String justifiedHr(Absence absenceshr) {
		System.out.println(absenceshr.getJustified());
		absenceshr.setJustified(Justification.JUSTIFIED);
		System.out.println(absenceshr.getJustified());
		absenceServiceLocal.update(absenceshr);
		return "absencesPageHr?faces-redirect=true";
	}
	
	public String updatePageHr(){
		if(absencesHr.size()<absenceServiceLocal.findAll().size()){
			absencesHr = absenceServiceLocal.findAll();
		}
		return "/absencePageHr?faces-redirect=true";
		
	}
	
	public String updatePageManager(){
		if(absences2.size()<this.getAbsencesDepartementManager().size()){
			absencesHr = this.getAbsencesDepartementManager();
		}
		return "/AbsencesPageManagerSuivi?faces-redirect=true";
		
	}
	
	 private void createPieModel1() {
		 int just=0;
		 int nonjust = 0;
		 List<Absence> absencesPie=absenceServiceLocal.findAll();
		 for (Absence absence : absencesPie) {
			if(absence.isJustified()){
				just++;
			}else{
				
				nonjust++;
			}
		}
	        pieModel1 = new PieChartModel();
	         
	        pieModel1.set("Justified", just);
	        pieModel1.set("Non Justifed", nonjust);
	   
	         
	        pieModel1.setTitle("Simple Pie");
	        pieModel1.setLegendPosition("w");
	    }
	
	 private void createPieModels() {
	        createPieModel1();
	        
	    }

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}
	private void createBarModels() {
        createBarModel();
       
    }
	
	private void createBarModel() {
        barModel = initBarModel();
         
        barModel.setTitle("Bar Chart");
        barModel.setLegendPosition("ne");
         
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Absences Charts");
         
        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Absences");
        yAxis.setMin(0);
        yAxis.setMax(10);
    }
	 private BarChartModel initBarModel() {
		 List<Absence> absencesBar= absenceServiceLocal.findAll();
		 int justifed2017=0;
		 int justfied2018=0;
		 
		 int nonJustifed2017=0;
		 int nonJustfied2018=0;
		 
		 String yeartodaye;
		 String lastyear;
		 java.util.Date utilDate= new java.util.Date();
		 java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		 //118 = 2018
		 // 117=2017
		 int year1=(sqlDate.getYear()-100);
		 int year2=(sqlDate.getYear()-101);
		 yeartodaye="20"+year1;
		 lastyear="20"+year2;
		 for (Absence absence : absencesBar) {
		       if(absence.getFromDate().getYear()==(year1+100)){
		    	    if(absence.isJustified()){
		    	    	justfied2018++;
		    	    }else{
		    	    	nonJustfied2018++;
		    	    }
		    	   
		       }else{ if(absence.getFromDate().getYear()==(year2+100)){
		    	   if(absence.isJustified()){
		    		   justifed2017++;
		    	    }else{
		    	    	nonJustifed2017++;
		    	    }}
		       }
		}
	        BarChartModel model = new BarChartModel();
	 
	        ChartSeries boys = new ChartSeries();
	        boys.setLabel("Justifed");
	        
	        boys.set(lastyear, justifed2017);
	        
	        boys.set(yeartodaye, justfied2018);
	 
	        ChartSeries girls = new ChartSeries();
	        girls.setLabel("NonJustifed");
	     
	        girls.set(lastyear, nonJustifed2017);
	        girls.set(yeartodaye, nonJustfied2018);
	 
	        model.addSeries(boys);
	        model.addSeries(girls);
	         
	        return model;
	    }
	
}
