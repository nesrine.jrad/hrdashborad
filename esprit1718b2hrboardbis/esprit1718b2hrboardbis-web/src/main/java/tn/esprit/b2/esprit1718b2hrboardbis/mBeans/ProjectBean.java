package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.context.PrimeFacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.chart.PieChartModel;
import org.primefaces.component.dialog.*;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Status;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceLocal;

@ManagedBean
@ApplicationScoped
public class ProjectBean {

	@EJB
	ProjectServiceLocal projectServiceLocal;

	@EJB
	EmployeeServiceLocal employeeServiceLocal;

	@EJB
	TaskServiceLocal taskServiceLocal;

	private String name;
	private String selectedOption;
	private Date dateOfBeginning;
	private Date expectedDateEnd;
	private List<Project> projects;

	private Map<String,Integer> employeesToSelect;
	private List<Integer> selectedEmployees;
	private List<Employee> employees;

	private int nbDone;
	private int nbToDo;
	private int nbDoing;

	private Project project;
	private Project projectToUpdate;
	
	private PieChartModel pieModel1;

	private boolean showFormUpdate;
	boolean showPie;

	@PostConstruct
	void init() {
		projects = projectServiceLocal.findAll();
		employees = employeeServiceLocal.findAll();
		employeesToSelect = new HashMap<>();
		for(Employee e: employees){
			employeesToSelect.put(e.getName()+" "+e.getFamilyName()+ " number of projects working on " + getNumberProjects(e.getId()) , Integer.valueOf(e.getId()));
		}
		showPie = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateOfBeginning() {
		return dateOfBeginning;
	}

	public void setDateOfBeginning(Date dateOfBeginning) {
		this.dateOfBeginning = dateOfBeginning;
	}

	public Date getExpectedDateEnd() {
		return expectedDateEnd;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void setExpectedDateEnd(Date expectedDateEnd) {
		this.expectedDateEnd = expectedDateEnd;
	}

	public String doAddProject() throws IOException {
		java.sql.Date date1 = new java.sql.Date(dateOfBeginning.getTime());
		java.sql.Date date2 = new java.sql.Date(expectedDateEnd.getTime());
		Project project = new Project(name, date1, date2);
		projectServiceLocal.save(project);
		affectEmployees();
		projects = projectServiceLocal.findAll();
		return "/listProjects?faces-redirect=true";
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public int getNbDone() {
		return nbDone;
	}

	public void setNbDone(int nbDone) {
		this.nbDone = nbDone;
	}

	public int getNbToDo() {
		return nbToDo;
	}

	public void setNbToDo(int nbToDo) {
		this.nbToDo = nbToDo;
	}

	public int getNbDoing() {
		return nbDoing;
	}

	public void setNbDoing(int nbDoing) {
		this.nbDoing = nbDoing;
	}

	public Project getProject() {
		return project;
	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String doDelete(int id) throws IOException {
		projectServiceLocal.delete(projectServiceLocal.find(id));
		projects = projectServiceLocal.findAll();
		return "listProjects?faces-redirect=true";
	}

	public void doSeeStats(int id) {
		project = projectServiceLocal.find(id);
		nbDone = projectServiceLocal.getListTasksOfProjectByStatus(id, StatusTask.DONE).size();
		nbToDo = projectServiceLocal.getListTasksOfProjectByStatus(id, StatusTask.ToDO).size();
		nbDoing = projectServiceLocal.getListTasksOfProjectByStatus(id, StatusTask.DOING).size();
		createPieModel();
		showPie = true;

	}

	public boolean isShowPie() {
		return showPie;
	}

	public void setShowPie(boolean showPie) {
		this.showPie = showPie;
	}

	private void createPieModel() {
		pieModel1 = new PieChartModel();

		pieModel1.set("TO DO", nbToDo);
		pieModel1.set("DOING", nbDoing);
		pieModel1.set("DONE", nbDone);

		pieModel1.setShowDataLabels(true);

		pieModel1.setTitle("Stats");
		pieModel1.setLegendPosition("w");

	}

	public Map<String, Integer> getEmployeesToSelect() {
		return employeesToSelect;
	}

	public void setEmployeesToSelect(Map<String, Integer> employeesToSelect) {
		this.employeesToSelect = employeesToSelect;
	}

	public List<Integer> getSelectedEmployees() {
		return selectedEmployees;
	}

	public void setSelectedEmployees(List<Integer> selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}

	private void affectEmployees(){
		int id = projectServiceLocal.findIdOfLastRecord();
		Project project = projectServiceLocal.find(id);
		for(Integer idEmployee : selectedEmployees){
			Employee employee = employeeServiceLocal.find(idEmployee.intValue());
			Task task = new Task(project,employee,"",project.getDateOfBeginning(),"");
			taskServiceLocal.save(task);
		}
	}
	
	

	/*
	 * public void affectEmployee(Integer idEmployee){
	 * if(employeesToAffect.contains(idEmployee)){
	 * employeesToAffect.remove(idEmployee); } else{
	 * employeesToAffect.add(idEmployee); }
	 * 
	 * }
	 */
	
	public int getNumberProjects(int idEmployee){
		Employee employee = employeeServiceLocal.find(idEmployee);
		int nb = projectServiceLocal.getProjectsByEmployee(idEmployee, Status.IN_PROGRESS).size();
			return nb;
	}

	public boolean isShowFormUpdate() {
		return showFormUpdate;
	}

	public void setShowFormUpdate(boolean showFormUpdate) {
		this.showFormUpdate = showFormUpdate;
	}

	public void toggleDisplayFormUpdate(int idProject){
		
		Project oldProject = projectToUpdate;
		if (oldProject!=null && oldProject.getId()==idProject) {
			showFormUpdate = false;
		} else {
			projectToUpdate = projectServiceLocal.find(idProject);
			showFormUpdate = true;
		}
		
	}

	public String doUpdate(){
		projectToUpdate.setStatus(Status.valueOf(selectedOption));
		projectServiceLocal.update(projectToUpdate);
		projects = projectServiceLocal.findAll();
		showFormUpdate=false;
		return "";
	}

	public String getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}

	public Project getProjectToUpdate() {
		return projectToUpdate;
	}

	public void setProjectToUpdate(Project projectToUpdate) {
		this.projectToUpdate = projectToUpdate;
	}


}
