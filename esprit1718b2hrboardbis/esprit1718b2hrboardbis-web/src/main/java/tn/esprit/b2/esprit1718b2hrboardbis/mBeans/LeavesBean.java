package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceLocal;

@ManagedBean
@ViewScoped
public class LeavesBean {
	@EJB
    LeaveServiceLocal leaveServiceLocal;
	@EJB
	DepartmentServicesLocal departmentServicesLocal;
	@EJB
	EmployeeServiceLocal employeeServiceLocal;
	private int idUpdateLeave;
	private  Date begin;
	private Date end;
	private  Date beginInsert;
	private Date endInsert;
	private List<Leave> leaves;
	private List<Leave> leavesHr;
	private List<Leave> leavesDepartement;
	private Leave leave;
	private boolean showUpdate;
	private Leave leaveInsert;
	private Date searchDate;
	private Date dd;
	private Boolean  dateInvalide=false;
    private Boolean msgvide=false;
    private Boolean invaludDates=false;

	public Boolean getDateInvalide() {
		return dateInvalide;
	}



	public void setDateInvalide(Boolean dateInvalide) {
		this.dateInvalide = dateInvalide;
	}



	public Boolean getMsgvide() {
		return msgvide;
	}



	public void setMsgvide(Boolean msgvide) {
		this.msgvide = msgvide;
	}



	public void setLeavesDepartement(List<Leave> leavesDepartement) {
		this.leavesDepartement = leavesDepartement;
	}

	
	private Date currentDate = new Date();

	public Date getCurrentDate() {
	    return currentDate;
	}
	
	

	public Date getDd() {
		Date date= new Date();
		return date;
	}
	public void setDd(Date dd) {
		this.dd = dd;
	}
	public void filter() {
		System.out.println(dd+"+-*/89654++++++++++++++++");
		List<Leave> leavesR = new ArrayList<>();
		leaves= leaveServiceLocal.findAll();
		//java.sql.Date sqlDateF= new java.sql.Date(dd.getTime());
		//System.out.println(sqlDateF+"+-*/89654++++++++++++++++");
		for (Iterator<Leave> iterator = leaves.iterator(); iterator.hasNext(); ) {
		    Leave value = iterator.next();
		    if(value.getFromDate().equals(dd))
			{  
		    	leavesR.add(value);
			}
		
		}
		leavesHr=leavesR;
		
		
	}
	
	public void filterEmp() {
		System.out.println(dd+"+-*/89654++++++++++++++++");
		List<Leave> leavesR = new ArrayList<>();
		leaves= leaveServiceLocal.getLeaveByManagerDepartement(Identity.connectedID);
		//java.sql.Date sqlDateF= new java.sql.Date(dd.getTime());
		//System.out.println(sqlDateF+"+-*/89654++++++++++++++++");
		for (Iterator<Leave> iterator = leaves.iterator(); iterator.hasNext(); ) {
		    Leave value = iterator.next();
		    if(value.getFromDate().equals(dd))
			{  
		    	leavesR.add(value);
			}
		
		}
		leaves=leavesR;
		
		
	}
	public Date getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}
	
	public Date getBeginInsert() {
		return beginInsert;
	}

	public void setBeginInsert(Date beginInsert) {
		this.beginInsert = beginInsert;
		leaveInsert.setFromDate(new java.sql.Date(beginInsert.getTime()));
	}

	public Date getEndInsert() {
		return endInsert;
	}

	public void setEndInsert(Date endInsert) {

		this.endInsert=endInsert;
		leaveInsert.setToDate(new java.sql.Date(endInsert.getTime()));
	}

	public Leave getLeaveInsert() {
		return leaveInsert;
	}

	public void setLeaveInsert(Leave leaveInsert) {
		this.leaveInsert = leaveInsert;
	}

	
	
	public Date getBegin() {
		return leave.getFromDate();
	}

	public void setBegin(Date begin) {
		System.out.println("***************"+begin);
		this.begin=begin;
		leave.setFromDate(new java.sql.Date(begin.getTime()));
	}

	public Date getEnd() {
		return leave.getToDate();
	}

	public void setEnd(Date end) {
		System.out.println("***************"+end);
		this.end=end;
		leave.setToDate(new java.sql.Date(end.getTime()));
	}

	public int getIdUpdateLeave() {
		return idUpdateLeave;
	}

	public void setIdUpdateLeave(int idUpdateLeave) {
		this.idUpdateLeave = idUpdateLeave;
	}

	@PostConstruct
	private void init() {
		leaves = new ArrayList<>();
		leavesDepartement = new ArrayList<>();
		leaves= leaveServiceLocal.getLeavesByUserId(Identity.connectedID);
		Collections.reverse(leaves);
		leavesDepartement = getLeavesDepartementManager();
		leavesHr=leaveServiceLocal.findAll();
		Collections.reverse(leavesDepartement);
		leaveInsert= new Leave();
		showUpdate= true;
		
		
	}

	public boolean isShowUpdate() {
		return showUpdate;
	}
	
	public void setShowUpdate(boolean showUpdate) {
		this.showUpdate = showUpdate;
	}

	public List<Leave> getLeaves() {
		return leaves;
	}
    public void deleteLeavesById(int id){
    	Leave leave = leaveServiceLocal.find(id);
    	leaveServiceLocal.delete(leave);
    	leaves= leaveServiceLocal.getLeavesByUserId(Identity.connectedID);
    	
    }
    public String updateLeave(Leave leave){
    	
    	System.out.println("++++++++++++"+leave.getDescription() + "*******"+leave.getFromDate());
    	System.out.print(leave.toString());
    	leave.setFromDate(new java.sql.Date(leave.getFromDate().getTime()));
    	leave.setToDate(new java.sql.Date(leave.getToDate().getTime()));
    	leave.setApproved(false);
    	leaveServiceLocal.update(leave);
    	this.showUpdate=true;
    	
    	return "/LeavesPage?faces-redirect=true";
    	
    }
    
    public String SaveLeave(Leave leave){
    	dateInvalide=false;
    	msgvide=false;
    	java.util.Date utilDate= new Date();
    	
    	if(leave.getFromDate().before(new java.sql.Date(utilDate.getTime()))){
    		dateInvalide=true;
    		return "";
    	}
    	if(leave.getFromDate()==null){
    		dateInvalide= true;
    		return "";
    	
    	}
    	if(leave.getToDate()==null){
    		dateInvalide= true;
    		return "";
    	
    	}else{
    		dateInvalide=true;}
    	if(leave.getFromDate().after(leave.getToDate())){
    		if(leave.getDescription().equals("")){
        		msgvide =true;
        	}
    		return "";
 	}
    	if(leave.getDescription().equals("")){
    		msgvide =true;
    		return "";
    	}
    	else{
        System.out.println("****************"+leave.toString());
        Employee employee= employeeServiceLocal.find(Identity.connectedID);
        leave.setEmployee(employee);
    	leaveServiceLocal.save(leave);
    	leave= null;
    	this.showUpdate=true;}
    	return "/LeavesPage?faces-redirect=true";
    	
    }
    
    public String ConfirmLeave(Leave leave){
        leave.setApproved(true);
        
    	leaveServiceLocal.update(leave);
    	leave= null;
    	this.showUpdate=true;
    	return "/LeavesPageHR?faces-redirect=true";
    	
    }
    
    
    
    
	public void setLeaves(List<Leave> leaves) {
		this.leaves = leaves;
	}
	public Leave getLeave() {
		return leave;
	}

	public void setLeave(Leave leave) {
		this.leave = leave;
	}
	public void doShowUpdate() {
		this.showUpdate = true;
	}
	public void doHideUpdate() {
		System.out.print("***********************************************");
		this.showUpdate = false;
	}
	
	
	public List<Leave> getLeavesDepartementManager() {
		List<Leave> l= new ArrayList<>();
		List<Department> departments = departmentServicesLocal.findAll();
		for (Department department : departments) {
			 for (Employee employee : department.getEmployees()) {
				   
				   if(employee.getId()==Identity.connectedID)
				   {
					
					   for (Employee employee2 : department.getEmployees()) {
						     if(employee2.getId()!=Identity.connectedID)
						    	 l.addAll(leaveServiceLocal.getLeavesByUserId(employee2.getId()));
					}
					  
				   }
				   
			}
			   
			   
		}
		
		return leavesDepartement =l;
	}
	
	public List<Leave> getLeavesDepartement() {
		return leavesDepartement;
	}



	public void filter2() {
		System.out.println(dd+"+-*/89654++++++++++++++++");
		List<Leave> leavesR = new ArrayList<>();
		//java.sql.Date sqlDateF= new java.sql.Date(dd.getTime());
		//System.out.println(sqlDateF+"+-*/89654++++++++++++++++");
		for (Iterator<Leave> iterator =  this.getLeavesDepartementManager().iterator(); iterator.hasNext(); ) {
		    Leave value = iterator.next();
		    if(!value.getFromDate().equals(dd))
			{    
		    	leavesR.add(value);
			}
		
		}
		for (Leave leave : leavesR) {
			leavesDepartement.remove(leave);
		}
		
		
	}
	
	public String updateManager()
	{
		
		if(leavesDepartement.size()<this.getLeavesDepartementManager().size()){
			leavesDepartement=getLeavesDepartementManager();
			
		}
		return "/LeavesPageManager?faces-redirect=true";
		
	}



	public List<Leave> getLeavesHr() {
		return leavesHr;
	}



	public void setLeavesHr(List<Leave> leavesHr) {
		this.leavesHr = leavesHr;
	}



	public Boolean getInvaludDates() {
		return invaludDates;
	}



	public void setInvaludDates(Boolean invaludDates) {
		this.invaludDates = invaludDates;
	}
	
	
	
}
