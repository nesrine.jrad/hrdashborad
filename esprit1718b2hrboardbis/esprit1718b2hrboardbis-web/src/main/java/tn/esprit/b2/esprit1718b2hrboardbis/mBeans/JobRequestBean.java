package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.xml.crypto.Data;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesLocal;

@javax.faces.bean.ManagedBean
@ViewScoped
public class JobRequestBean {
	
	private JobRequest jobRequest;
	private JobOffer jobOffer;
	private String name;
	private String LastName;
	private Date dateOfBirth;
	private String phone;
	private String Mail;
	private boolean showQuizz=false;
	private boolean showList=true;
	private Integer evaluation=0;
	private String q1;
	private String q2;
	private String q3;
	private String q4;
	private String q5;
	private String q6;
	private String q7;
	private String q8;
	private String q9;
	private String q10;
	@EJB
	private JobRequestServicesLocal jobRequestServicesLocal;
	@EJB
	private JobOfferServicesLocal JobOfferServicesLocal;
	List<JobRequest> jobRequests= new ArrayList<JobRequest>();
	private boolean showForm=false;
	
	@PostConstruct
	private void init() {
		jobRequest= new JobRequest();
		System.out.println("aaaaaaaaaa");
		jobRequests=jobRequestServicesLocal.findAll();
		jobOffer=new JobOffer();
		
	}
	
     public JobRequest getJobRequest() {
		return jobRequest;
	}

	public void setJobRequest(JobRequest jobRequest) {
		this.jobRequest = jobRequest;
	}

	public String doAddJobrequest()
     {    Date date=java.sql.Date.valueOf(java.time.LocalDate.now());
     	jobRequest.setDateOfRequest(date);
     	jobRequest.setDateOfBirthCandidate(date);
     	jobRequest.setJobOffer(jobOffer);
     	jobRequest.setEmailOfCandidate(Mail);
     	jobRequest.setPhoneOfCandidate(phone);
     	jobRequest.setNameCandidate(name);
     	jobRequest.setLastNameCandidate(LastName);
     	if(q1=="true")
		{
			evaluation++;
		}
		if(q2.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q3.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q4.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q5.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q6.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q7.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q8.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q9.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
     	jobRequest.setMark(evaluation);
     	System.out.println(jobRequest.toString());
    	 jobRequestServicesLocal.save(jobRequest);
    	return "JobOfferCondidat?faces-redirect=true";
     }
     public String doTest()
     {
     	return "/login?faces-redirect=true";

     }

	public List<JobRequest> getJobRequests() {
		return jobRequests;
	}

	public void setJobRequests(List<JobRequest> jobRequests) {
		this.jobRequests = jobRequests;
	}
    public void doShowForm(JobOffer jobOffer)
    {  this.jobOffer=jobOffer;
    	System.out.println(jobOffer.toString());
    	showForm=true;
    	showList=false;
    	showQuizz=false;
    }
	public boolean isShowForm() {
		return showForm;
	}

	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}

	public JobOffer getJobOffer() {
		return jobOffer;
	}

	public void setJobOffer(JobOffer jobOffer) {
		this.jobOffer = jobOffer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return Mail;
	}

	public void setMail(String mail) {
		Mail = mail;
	}
     public void doHideForm()
     {   showList=true;
    	 showForm=false;
    	 showQuizz=false;
     }

	public boolean isShowQuizz() {
		return showQuizz;
	}

	public void setShowQuizz(boolean showQuizz) {
		this.showQuizz = showQuizz;
	}
     public void doShowQuizz()
     {   showList=false;
    	 showQuizz=true;
    	 showForm=false;
     }

	public boolean isShowList() {
		return showList;
	}

	public void setShowList(boolean showList) {
		this.showList = showList;
	}

	public String isQ1() {
		return q1;
	}

	public void setQ1(String q1) {
		this.q1 = q1;
	}

	public String isQ2() {
		return q2;
	}

	public void setQ2(String q2) {
		this.q2 = q2;
	}

	public String getQ3() {
		return q3;
	}

	public void setQ3(String q3) {
		this.q3 = q3;
	}

	public String getQ4() {
		return q4;
	}

	public void setQ4(String q4) {
		this.q4 = q4;
	}

	public String getQ5() {
		return q5;
	}

	public void setQ5(String q5) {
		this.q5 = q5;
	}

	public String getQ6() {
		return q6;
	}

	public void setQ6(String q6) {
		this.q6 = q6;
	}

	public String getQ7() {
		return q7;
	}

	public void setQ7(String q7) {
		this.q7 = q7;
	}

	public String getQ8() {
		return q8;
	}

	public void setQ8(String q8) {
		this.q8 = q8;
	}

	public String getQ9() {
		return q9;
	}

	public void setQ9(String q9) {
		this.q9 = q9;
	}

	public String getQ10() {
		return q10;
	}

	public void setQ10(String q10) {
		this.q10 = q10;
	}

	public String getQ1() {
		return q1;
	}

	public String getQ2() {
		return q2;
	}

	public void doShowValues()
	{  		System.out.println(name);
	System.out.println(LastName);


		System.out.println(q1);
		System.out.println(q2);

		System.out.println(q3);

		System.out.println(q4);

		System.out.println(q5);
		if(q1=="true")
		{
			evaluation++;
		}
		if(q2.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q3.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q4.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q5.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q6.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q7.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q8.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
		if(q9.equalsIgnoreCase("true"))
		{
			evaluation++;
		}
	
		System.out.println("evalu"+evaluation);

	}

	public Integer getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(Integer evaluation) {
		this.evaluation = evaluation;
	}
	public String doDelete(JobRequest jobRequest)
	{
		jobRequestServicesLocal.delete(jobRequest);
		return "JobRequestsHome??faces-redirect=true";
	}
     
}
