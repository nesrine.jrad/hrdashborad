package tn.esprit.b2.esprit1718b2hrboardbis.mBeans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.User;
import tn.esprit.b2.esprit1718b2hrboardbis.services.UserServiceLocal;

@ManagedBean
@SessionScoped
public class Identity {
	private boolean isLogged = false;
	private User user = new User();
	static int connectedID;
	@EJB
	private UserServiceLocal userServiceLocal;

	public String logout() {
		isLogged = false;
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		connectedID = 0;
		return "/login?faces-redirect=true";
	}

	public String doLogin() {
		String navigateTo = "";
		Employee userLoggedIn = (Employee) userServiceLocal.login(user.getLogin(), user.getPassword());
		if (userLoggedIn != null) {
			isLogged = true;
			user = userLoggedIn;
			connectedID = user.getId();
			if(userLoggedIn.getRoleEmployee().equals(RoleEmployee.HRMANAGER)){ //HR MANAGER
				navigateTo = "/HomeHRManager?faces-redirect=true";}
			else if(userLoggedIn.getRoleEmployee().equals(RoleEmployee.MANAGER)){ //MANAGER
				navigateTo = "/HomeManager?faces-redirect=true";}
			else if (userLoggedIn.getRoleEmployee().equals(RoleEmployee.EMPLOYEE))											//EMPLOYEE
				{navigateTo = "/HomeEmployee?faces-redirect=true";}
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Veuillez insérer un login et un mot de passe valide", ""));
			return "/login?faces-redirect=true";
		}
		return navigateTo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setLogged(boolean isLogged) {
		this.isLogged = isLogged;
	}

	public Boolean getIsLogged() {
		return isLogged;
	}

	public void setIsLogged(Boolean isLogged) {
		this.isLogged = isLogged;
	}
}
