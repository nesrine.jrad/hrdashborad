package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.sql.Date;

public class TaskListView {
	private int idProject;
	private int idEmployee;
	private String nameTask;
	private String nameProject;
	private String nameEmployee;
	private StatusTask statusTask;
	private Date dateOfbeginning;
	private Date dateOfEnd;
	private Project project;
	private Employee employee;
	private String descripiton;
	
	
	public TaskListView() {
		super();
	}
	
	
	public TaskListView(Task task) {
		super();
		this.idEmployee = task.getEmployee().getId();
		this.idProject = task.getProject().getId();
		this.nameTask = task.getTaskPk().getName();
		this.nameProject = task.getProject().getName();
		this.nameEmployee = task.getEmployee().getName();
		this.statusTask = task.getStatusTask();
		this.dateOfbeginning = task.getDateTaskBeginning();
		this.dateOfEnd = task.getDateEnd();
		this.descripiton = task.getDescription();
	
		this.project = project;
		this.employee = employee;
	}


	public String getNameProject() {
		return nameProject;
	}
	public void setNameProject(String nameProject) {
		this.nameProject = nameProject;
	}
	public String getNameEmployee() {
		return nameEmployee;
	}
	public void setNameEmployee(String nameEmployee) {
		this.nameEmployee = nameEmployee;
	}
	public StatusTask getStatusTask() {
		return statusTask;
	}
	public void setStatusTask(StatusTask statusTask) {
		this.statusTask = statusTask;
	}
	public Date getDateOfbeginning() {
		return dateOfbeginning;
	}
	public void setDateOfbeginning(Date dateOfbeginning) {
		this.dateOfbeginning = dateOfbeginning;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public String getNameTask() {
		return nameTask;
	}


	public void setNameTask(String nameTask) {
		this.nameTask = nameTask;
	}
	
	


	public int getIdProject() {
		return idProject;
	}


	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}


	public int getIdEmployee() {
		return idEmployee;
	}


	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}


	public Date getDateOfEnd() {
		return dateOfEnd;
	}


	public void setDateOfEnd(Date dateOfEnd) {
		this.dateOfEnd = dateOfEnd;
	}


	public String getDescripiton() {
		return descripiton;
	}


	public void setDescripiton(String descripiton) {
		this.descripiton = descripiton;
	}
	
	
	

}
