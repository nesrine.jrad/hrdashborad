package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

@Embeddable

public class SeancePk implements Serializable {
	private int idEmployee;
	private int idTrainer;
	private int idTraining;
	private Date dateSeance;
	private static final long serialVersionUID = 1L;

	public SeancePk() {
		super();
	}   

	public SeancePk(int idEmployee, int idTrainer, int idTraining, Date dateSeance) {
		super();
		this.idTrainer = idTrainer;
		this.idEmployee = idEmployee;
		this.idTraining = idTraining;
		this.dateSeance = dateSeance;
	}

	public int getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	public int getIdTrainer() {
		return idTrainer;
	}

	public void setIdTrainer(int idTrainer) {
		this.idTrainer = idTrainer;
	}

	public int getIdTraining() {
		return idTraining;
	}

	public void setIdTraining(int idTraining) {
		this.idTraining = idTraining;
	}

	public Date getDateSeance() {
		return dateSeance;
	}

	public void setDateSeance(Date dateSeance) {
		this.dateSeance = dateSeance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateSeance == null) ? 0 : dateSeance.hashCode());
		result = prime * result + idEmployee;
		result = prime * result + idTrainer;
		result = prime * result + idTraining;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeancePk other = (SeancePk) obj;
		if (dateSeance == null) {
			if (other.dateSeance != null)
				return false;
		} else if (!dateSeance.equals(other.dateSeance))
			return false;
		if (idEmployee != other.idEmployee)
			return false;
		if (idTrainer != other.idTrainer)
			return false;
		if (idTraining != other.idTraining)
			return false;
		return true;
	}
	
	
	
}
