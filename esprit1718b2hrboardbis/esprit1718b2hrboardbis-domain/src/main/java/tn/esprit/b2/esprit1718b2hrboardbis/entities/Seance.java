package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Seance
 *
 */
@Entity
public class Seance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8291103180734657883L;
	@EmbeddedId
	SeancePk seancePk;
	private String salle;
	
	
	@ManyToOne
	@JoinColumn(name="idTrainer", referencedColumnName ="id", updatable=false, insertable = false)
	private Trainer trainer;
	
	@ManyToOne
	@JoinColumn(name="idEmployee", referencedColumnName="id", updatable = false, insertable = false)
	private Employee employee;
	
	@ManyToOne
	@JoinColumn(name="idTraining", referencedColumnName="id", updatable = false, insertable = false)
	private Training training;
	
	public Seance() {
		super();
	}
	
	public Seance(int idEmployee, int idTrainer, int idTraining, Date dateSeance,String salle) {
		super();
		this.seancePk = new SeancePk(idEmployee,idTrainer,idTraining,dateSeance);
		this.salle=salle;
	}

	public SeancePk getSeancePk() {
		return seancePk;
	}

	public void setSeancePk(SeancePk seancePk) {
		this.seancePk = seancePk;
	}

	public String getSalle() {
		return salle;
	}

	public void setSalle(String salle) {
		this.salle = salle;
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}
	
	
	
}
