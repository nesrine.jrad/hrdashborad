package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Task
 *
 */
@Entity

public class Task implements Serializable {
	
	@EmbeddedId
	private TaskPK taskPk;
	@Enumerated(EnumType.STRING)
	private StatusTask statusTask;
	private Date dateTaskBeginning;
	private Date dateEnd;
	private String description;
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="idProject", referencedColumnName ="id", updatable=false, insertable = false)
	private Project project;
	
	@ManyToOne
	@JoinColumn(name="idEmployee", referencedColumnName="id", updatable = false, insertable = false)
	private Employee employee;
	
	public Task() {
		super();
	}
	
	
	
	
	public Task(Project project, Employee employee, String name, Date dateTaskBeginning, String description) {
		super();
		this.taskPk = new TaskPK(project.getId(), employee.getId(), name);
		this.project = project;
		this.employee = employee;
		this.statusTask = StatusTask.ToDO;
		this.dateTaskBeginning = dateTaskBeginning;
		this.dateEnd=null;
		this.description = description;
	}


	public TaskPK getTaskPk() {
		return taskPk;
	}

	public void setTaskPk(TaskPK taskPk) {
		this.taskPk = taskPk;
	}

	public StatusTask getStatusTask() {
		return statusTask;
	}

	public void setStatusTask(StatusTask statusTask) {
		this.statusTask = statusTask;
	}


	public Project getProject() {
		return project;
	}


	public void setProject(Project project) {
		this.project = project;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public Date getDateTaskBeginning() {
		return dateTaskBeginning;
	}


	public void setDateTaskBeginning(Date dateTaskBeginning) {
		this.dateTaskBeginning = dateTaskBeginning;
	}


	public Date getDateEnd() {
		return dateEnd;
	}


	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
		this.statusTask = StatusTask.DONE;
	}




	public String getDescription() {
		return description;
	}




	public void setDescription(String description) {
		this.description = description;
	}

	
	
	

	
	
	
   
}
