package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Leave
 *
 */
@Entity
@Table(name="TimeOff")
public class Leave implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	private Date fromDate;
	private Date toDate;
	private String description;
	private boolean approved; 
	
	@ManyToOne 
	private Employee employee;
	private static final long serialVersionUID = 1L;

	public Leave() {
		super();
	}   
	
	
	public Leave(String name, Date fromDate, Date toDate, String description, boolean approved, Employee employee) {
		super();
		this.name = name;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.description = description;
		this.approved = approved;
		this.employee = employee;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
   
}
