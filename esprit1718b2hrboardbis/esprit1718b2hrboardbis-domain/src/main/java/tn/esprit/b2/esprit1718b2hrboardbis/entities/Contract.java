package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Contract
 *
 */
@Entity

public class Contract implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private TypeContract typeContract;
	private Date dateOfHiring;
	private Date dateOfEnding;
	@OneToOne
	private Employee employee; 

	
	private static final long serialVersionUID = 1L;

	public Contract() {
		super();
	}

	public Contract(TypeContract typeContract, Date dateOfHiring, Date dateOfEnding, Employee employee) {
		super();
		this.typeContract = typeContract;
		this.dateOfHiring = dateOfHiring;
		this.dateOfEnding = dateOfEnding;
		this.employee = employee;
	}

	public TypeContract getTypeContract() {
		return typeContract;
	}

	public void setTypeContract(TypeContract typeContract) {
		this.typeContract = typeContract;
	}

	public Date getDateOfHiring() {
		return dateOfHiring;
	}

	public void setDateOfHiring(Date dateOfHiring) {
		this.dateOfHiring = dateOfHiring;
	}

	public Date getDateOfEnding() {
		return dateOfEnding;
	}

	public void setDateOfEnding(Date dateOfEnding) {
		this.dateOfEnding = dateOfEnding;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "Contract [typeContract=" + typeContract + ", dateOfHiring=" + dateOfHiring + ", dateOfEnding="
				+ dateOfEnding + ", employee=" + employee + "]";
	}
	
   
}
