package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Skill
 *
 */
@Entity

public class Skill implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;  
	private String description;

	@ManyToMany (mappedBy="skills",fetch=FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.MERGE})
	private List <Employee> employees;

	@ManyToMany(mappedBy="skills",fetch=FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.MERGE})
	private List<JobOffer> JobOffers;

	private static final long serialVersionUID = 1L;

	public Skill() {
		super();
	}   
	
	public Skill( String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "Skill [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

	public List<JobOffer> getLiJobOffers() {
		return JobOffers;
	}

	public void setLiJobOffers(List<JobOffer> liJobOffers) {
		this.JobOffers = liJobOffers;
	}

	public List<Employee> getEmployees() {
		return employees;
	}
	
}
