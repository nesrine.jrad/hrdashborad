package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Employee
 *
 */
@Entity

public class Employee extends User implements Serializable {


	private String name;
	private String familyName;
	private Date dateOfBirth;
	private Date dateOfHiring;
	private String email;
	private String phoneNumber;
	private String address;
	private float salary;
	@Enumerated(EnumType.STRING)
	private RoleEmployee roleEmployee;
	@Enumerated(EnumType.STRING)
	private TypeContract typeContract;
	@ManyToMany
	private List<Department> departments;
	
	@ManyToMany 
	private List<Skill> skills;
	
	@OneToMany (mappedBy="employee",cascade= CascadeType.PERSIST, fetch=FetchType.EAGER)
	private List<Seance> seances;
	 
	@OneToMany (mappedBy="employee",cascade= CascadeType.PERSIST, fetch=FetchType.EAGER)
	private List<Task> tasks;
	
	@OneToMany(mappedBy="employee",cascade= CascadeType.PERSIST, fetch=FetchType.EAGER)
	List<Interview> Interview= new ArrayList<Interview>();
	
	@OneToMany (mappedBy="employee")
	private List<Leave> leaves;
	
	@OneToMany (mappedBy="employee")
	private List<Absence> absences;
	
	@OneToMany (mappedBy="employee")
	private List<AdditionalHour> additionalHours ;
	
	
	
	private static final long serialVersionUID = 1L;

	

   
	public Employee() {
		super();
	}


	

	public Employee(String name, String familyName, Date dateOfBirth, Date dateOfHiring, String email,
			String phoneNumber, String address, float salary, RoleEmployee roleEmployee,
			TypeContract typeContract, String login, String password,List<Department> departments) {
		super(login,password);
		this.name = name;
		this.familyName = familyName;
		this.dateOfBirth = dateOfBirth;
		this.dateOfHiring = dateOfHiring;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.salary = salary;
		this.roleEmployee = roleEmployee;
		this.typeContract = typeContract;
		this.departments=departments;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getFamilyName() {
		return familyName;
	}




	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}




	public Date getDateOfBirth() {
		return dateOfBirth;
	}




	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}




	public Date getDateOfHiring() {
		return dateOfHiring;
	}




	public void setDateOfHiring(Date dateOfHiring) {
		this.dateOfHiring = dateOfHiring;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getPhoneNumber() {
		return phoneNumber;
	}




	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}




	public String getAddress() {
		return address;
	}




	public void setAddress(String address) {
		this.address = address;
	}




	public float getSalary() {
		return salary;
	}




	public void setSalary(float salary) {
		this.salary = salary;
	}




	public RoleEmployee getRoleEmployee() {
		return roleEmployee;
	}




	public void setRoleEmployee(RoleEmployee roleEmployee) {
		this.roleEmployee = roleEmployee;
	}




	public TypeContract getTypeContract() {
		return typeContract;
	}




	public void setTypeContract(TypeContract typeContract) {
		this.typeContract = typeContract;
	}




	public List<Skill> getSkills() {
		return skills;
	}




	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}





	public List<Task> getTasks() {
		return tasks;
	}




	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}




	public List<Leave> getLeaves() {
		return leaves;
	}




	public void setLeaves(List<Leave> leaves) {
		this.leaves = leaves;
	}




	public List<Absence> getAbsences() {
		return absences;
	}




	public void setAbsences(List<Absence> absences) {
		this.absences = absences;
	}




	public List<Department> getDepartments() {
		return departments;
	}




	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}




	public List<Interview> getInterview() {
		return Interview;
	}




	public void setInterview(List<Interview> interview) {
		Interview = interview;
	}




	public void assignTasksToThisEmployee(List<Task> tasks){
		this.setTasks(tasks);
		for(Task task: tasks){
			task.setEmployee(this);
		}
	}




	public List<Seance> getSeances() {
		return seances;
	}




	public void setSeances(List<Seance> seances) {
		this.seances = seances;
	}




	@Override
	public String toString() {
		return  name + " " + familyName;
		
	}



	public void setId2(Object id2) {
		// TODO Auto-generated method stub
		
	}
	public String getFamilyName2() {
		return familyName;
	}




	public void setFamilyName2(String familyName) {
		this.familyName = familyName;
	}

	
	public String getName2() {
		return name;
	}




	public void setName2(String name) {
		this.name = name;
	}



	
	
}
