package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: JobOffer
 *
 */
@Entity

public class JobOffer implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private Date dateOfPublishing;
	private String name;
	private String description;
	private Float Salary;
	private String TimeOfJob;
	private String Domain;
	@Enumerated(EnumType.STRING)
	private TypeContract typeContrat;
	@OneToMany(mappedBy="jobOffer" ,cascade= {CascadeType.REMOVE},fetch=FetchType.EAGER)
	private List<JobRequest> jobRequest;
	@ManyToOne
	private Department department;
	@ManyToMany
	private List<Skill> skills;
	private int nbrOfRequest;

	private static final long serialVersionUID = 1L;
	
	public List<JobRequest> getJobRequest() {
		return jobRequest;
	}

	public void setJobRequest(List<JobRequest> jobRequest) {
		this.jobRequest = jobRequest;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
    
	
	public JobOffer(Date dateOfPublishing, String name, String description, Float salary, String timeOfJob,
			TypeContract typeContrat, Department department) {
		super();
		this.dateOfPublishing = dateOfPublishing;
		this.name = name;
		this.description = description;
		Salary = salary;
		TimeOfJob = timeOfJob;
		this.typeContrat = typeContrat;
		this.department = department;
		
	}

	public JobOffer(String name, String description, Float salary, String timeOfJob, String domain,
			TypeContract typeContrat, List<JobRequest> jobRequest, Department department, List<Skill> skills,
			int nbrOfRequest) {
		super();
		this.name = name;
		this.description = description;
		Salary = salary;
		TimeOfJob = timeOfJob;
		Domain = domain;
		this.typeContrat = typeContrat;
		this.jobRequest = jobRequest;
		this.department = department;
		this.skills = skills;
		this.nbrOfRequest = nbrOfRequest;
	}

	public JobOffer(Date dateOfPublishing, String name,String description)
	{
		super();
		this.dateOfPublishing=dateOfPublishing;
		this.description=description;
		this.name=name;
	}  

	public JobOffer( Date dateOfPublishing, String name, String description, List<JobRequest> jobRequest) {
		super();
		
		this.dateOfPublishing = dateOfPublishing;
		this.name = name;
		this.description = description;
		this.jobRequest = jobRequest;
	}
	



	public JobOffer(Date dateOfPublishing, String name, String description, Float salary, String timeOfJob,
			TypeContract typeContrat, List<JobRequest> jobRequest, Department department, int nbrOfRequest) {
		super();
		this.dateOfPublishing = dateOfPublishing;
		this.name = name;
		this.description = description;
		Salary = salary;
		TimeOfJob = timeOfJob;
		this.typeContrat = typeContrat;
		this.jobRequest = jobRequest;
		this.department = department;
		this.nbrOfRequest = nbrOfRequest;
	}

	public JobOffer(Date dateOfPublishing, String name, String description, List<JobRequest> jobRequest,
			Department department, int nbrOfRequest) {
		super();
		this.dateOfPublishing = dateOfPublishing;
		this.name = name;
		this.description = description;
		this.jobRequest = jobRequest;
		this.department = department;
		this.nbrOfRequest = nbrOfRequest;
	}
	

//	public JobOffer(Date dateOfPublishing, String name, String description, Float salary, String timeOfJob,
//			TypeContract typeContrat, Department department) {
//		super();
//		this.dateOfPublishing = dateOfPublishing;
//		this.name = name;
//		this.description = description;
//		Salary = salary;
//		TimeOfJob = timeOfJob;
//		this.typeContrat = typeContrat;
//		this.department = department;
//		
//	}

	public int getNbrOfRequest() {
		return nbrOfRequest;
	}

	public void setNbrOfRequest(int nbrOfRequest) {
		this.nbrOfRequest = nbrOfRequest;
	}

	public JobOffer() {
		super();
	}   
	



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public Date getDateOfPublishing() {
		return this.dateOfPublishing;
	}

	public void setDateOfPublishing(Date dateOfPublishing) {
		this.dateOfPublishing = dateOfPublishing;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		String str;
		for (JobRequest jobRequest2 : jobRequest) {
			
			
			System.out.println(jobRequest2.getId());
			System.out.println(jobRequest2.getLastNameCandidate());
			System.out.println(jobRequest2.getPhoneOfCandidate());
			
			
		}
		
		
		return "JobOffer [id=" + id + ", dateOfPublishing=" + dateOfPublishing + ", name=" + name + ", description="
				+ description +   ", department=" + department + ", nbrOfRequest="
				+ nbrOfRequest  + " ]";
		
		
	}

	public Float getSalary() {
		return Salary;
	}

	public void setSalary(Float salary) {
		Salary = salary;
	}

	public String getTimeOfJob() {
		return TimeOfJob;
	}

	public void setTimeOfJob(String timeOfJob) {
		TimeOfJob = timeOfJob;
	}

	public TypeContract getTypeContrat() {
		return typeContrat;
	}

	public void setTypeContrat(TypeContract typeContrat) {
		this.typeContrat = typeContrat;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}

	public String getDomain() {
		return Domain;
	}

	public void setDomain(String domain) {
		Domain = domain;
	}

   
}
