package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.sql.Date;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;



public class EmployeeView {
	private Integer id;
	private String name;
	private String familyName;
	private Date dateOfBirth;
	private Date dateOfHiring;
	private String email;
	private String phoneNumber;
	private String address;
	private float salary;
	@Enumerated(EnumType.STRING)
	private RoleEmployee roleEmployee;
	@Enumerated(EnumType.STRING)
	private TypeContract typeContract;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Date getDateOfHiring() {
		return dateOfHiring;
	}
	public void setDateOfHiring(Date dateOfHiring) {
		this.dateOfHiring = dateOfHiring;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public RoleEmployee getRoleEmployee() {
		return roleEmployee;
	}
	public void setRoleEmployee(RoleEmployee roleEmployee) {
		this.roleEmployee = roleEmployee;
	}
	public TypeContract getTypeContract() {
		return typeContract;
	}
	public void setTypeContract(TypeContract typeContract) {
		this.typeContract = typeContract;
	}

	public EmployeeView(Integer id,String name, String familyName, Date dateOfBirth, Date dateOfHiring, String email,
			String phoneNumber, String address, float salary, RoleEmployee roleEmployee, TypeContract typeContract) {
		super();
		this.id=id;
		this.name = name;
		this.familyName = familyName;
		this.dateOfBirth = dateOfBirth;
		this.dateOfHiring = dateOfHiring;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.salary = salary;
		this.roleEmployee = roleEmployee;
		this.typeContract = typeContract;
		//this.departments = departments;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	

}
