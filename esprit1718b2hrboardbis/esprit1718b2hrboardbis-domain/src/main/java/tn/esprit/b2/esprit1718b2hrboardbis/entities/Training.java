package tn.esprit.b2.esprit1718b2hrboardbis.entities;


import java.util.*;
import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Training
 *
 */
@Entity

public class Training implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String title;
	private String description;
	private Date date;
	private static final long serialVersionUID = 1L;

	/*@ManyToMany
	private List<Skill> skills;
////////////////////////////////////////////////////////////*/
	@OneToMany (mappedBy="training",cascade= CascadeType.PERSIST, fetch=FetchType.EAGER)
	private List<Seance> seances;
	
	public Training() {
		super();
	}
	public Training(String title, String description, Date date) {
		super();
	
		this.title = title;
		this.description = description;
		this.date=date;
	}
	
	public Training(String title, String description, Date date,Integer id) {
		this.id=id;
		this.title = title;
		this.description = description;
		this.date=date;
	}
	
	
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return title;
	}
	
	
	
	
	
	
	
}
