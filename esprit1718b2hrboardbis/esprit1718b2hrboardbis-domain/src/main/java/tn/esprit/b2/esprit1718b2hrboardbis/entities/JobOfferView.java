package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

public class JobOfferView {
	private int id;
	private Date dateOfPublishing;
	private String name;
	private String description;
	private Float Salary;
	private String TimeOfJob;
	@Enumerated(EnumType.STRING)
	private TypeContract typeContrat;
	private String department;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDateOfPublishing() {
		return dateOfPublishing;
	}
	public void setDateOfPublishing(Date dateOfPublishing) {
		this.dateOfPublishing = dateOfPublishing;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Float getSalary() {
		return Salary;
	}
	public void setSalary(Float salary) {
		Salary = salary;
	}
	public String getTimeOfJob() {
		return TimeOfJob;
	}
	public void setTimeOfJob(String timeOfJob) {
		TimeOfJob = timeOfJob;
	}
	public TypeContract getTypeContrat() {
		return typeContrat;
	}
	public void setTypeContrat(TypeContract typeContrat) {
		this.typeContrat = typeContrat;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public JobOfferView(int id, Date dateOfPublishing, String name, String description, Float salary, String timeOfJob,
			TypeContract typeContrat, String department) {
		super();
		this.id = id;
		this.dateOfPublishing = dateOfPublishing;
		this.name = name;
		this.description = description;
		Salary = salary;
		TimeOfJob = timeOfJob;
		this.typeContrat = typeContrat;
		this.department = department;
	}

	

}
