package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;

/**
 * Entity implementation class for Entity: Interview
 *
 */
@Entity

public class Interview implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToOne 
	private JobRequest jobRequest;
	private Date dateOfInterview;
	private String timeOfInterview;
	private float evalution;
	@ManyToOne (cascade={CascadeType.REMOVE})
	private Employee employee;
	private static final long serialVersionUID = 1L;

	public Interview(Date dateOfInterview) {
		super();
		this.dateOfInterview = dateOfInterview;
	}
	public Interview(JobRequest jobRequest, Date dateOfInterview) {
		super();
		this.jobRequest = jobRequest;
		this.dateOfInterview = dateOfInterview;
	}
	
	public String getTimeOfInterview() {
		return timeOfInterview;
	}
	public void setTimeOfInterview(String timeOfInterview) {
		this.timeOfInterview = timeOfInterview;
	}
	public float getEvalution() {
		return evalution;
	}
	public void setEvalution(float evalution) {
		this.evalution = evalution;
	}
	public Interview(JobRequest jobRequest, Date dateOfInterview, String timeOfInterview, float evalution,Employee employee) {
		super();
		this.jobRequest = jobRequest;
		this.dateOfInterview = dateOfInterview;
		this.timeOfInterview = timeOfInterview;
		this.evalution = evalution;
		this.employee=employee;
	}
	public Interview() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public JobRequest getJobRequest() {
		return this.jobRequest;
	}

	public void setJobRequest(JobRequest jobRequest) {
		this.jobRequest = jobRequest;
	}   
	public Date getDateOfInterview() {
		return this.dateOfInterview;
	}

	public void setDateOfInterview(Date dateOfInterview) {
		this.dateOfInterview = dateOfInterview;
	}
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	@Override
	public String toString() {
		return "Interview [id=" + id + ", jobRequest=" + jobRequest + ", dateOfInterview=" + dateOfInterview
				+ ", timeOfInterview=" + timeOfInterview + ", evalution=" + evalution + "]";
	}
   
}
