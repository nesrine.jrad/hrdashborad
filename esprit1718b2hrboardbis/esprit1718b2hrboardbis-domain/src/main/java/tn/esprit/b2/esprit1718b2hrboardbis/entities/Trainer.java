package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Trainer
 *
 */
@Entity

public class Trainer implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private String familyName;
	private String email;
	private String phoneNumber;
	private String domain;
	
	private static final long serialVersionUID = 1L;
	
	@OneToMany (mappedBy="trainer",cascade= CascadeType.PERSIST, fetch=FetchType.EAGER)
	private List<Seance> seances;


	public Trainer() {
		super();
	} 
	
	
	public Trainer(String name, String familyName, String email , String phoneNumber,String domain) {
		super();
		this.name = name;
		this.familyName = familyName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.domain = domain;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return name + " "+ familyName ;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFamilyName() {
		return familyName;
	}


	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getDomain() {
		return domain;
	}


	public void setDomain(String domain) {
		this.domain = domain;
	}   
}
