package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.File;
import java.io.Serializable;

import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: JobRequest
 *
 */
@Entity

public class JobRequest implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private Date dateOfRequest;
	private String nameCandidate;
	private String lastNameCandidate;
	private Date dateOfBirthCandidate;
	private String phoneOfCandidate;
	private String emailOfCandidate;
	//@OneToOne(cascade={ CascadeType.REMOVE,},fetch=FetchType.EAGER)
	//private Interview interview;
	@ManyToOne(fetch=FetchType.EAGER)
	private JobOffer jobOffer;
	private String state;
	private Integer mark;
	//private File cv;
	
	private static final long serialVersionUID = 1L;

	
	public JobRequest(Date dateOfRequest, String nameCandidate, String lastNameCandidate, Date dateOfBirthCandidate,
			String phoneOfCandidate, String emailOfCandidate, JobOffer jobOffer) {
		super();
		this.dateOfRequest = dateOfRequest;
		this.nameCandidate = nameCandidate;
		this.lastNameCandidate = lastNameCandidate;
		this.dateOfBirthCandidate = dateOfBirthCandidate;
		this.phoneOfCandidate = phoneOfCandidate;
		this.emailOfCandidate = emailOfCandidate;
		this.jobOffer = jobOffer;
	}



	public JobRequest(Date dateOfRequest, String nameCandidate, String lastNameCandidate, Date dateOfBirthCandidate,
			String phoneOfCandidate, String emailOfCandidate, JobOffer jobOffer, String state) {
		super();
		this.dateOfRequest = dateOfRequest;
		this.nameCandidate = nameCandidate;
		this.lastNameCandidate = lastNameCandidate;
		this.dateOfBirthCandidate = dateOfBirthCandidate;
		this.phoneOfCandidate = phoneOfCandidate;
		this.emailOfCandidate = emailOfCandidate;
		this.jobOffer = jobOffer;
		this.state = state;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public JobRequest() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public Date getDateOfRequest() {
		return this.dateOfRequest;
	}

	public void setDateOfRequest(Date dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}
	public String getNameCandidate() {
		return nameCandidate;
	}
	public void setNameCandidate(String nameCandidate) {
		this.nameCandidate = nameCandidate;
	}
	public String getLastNameCandidate() {
		return lastNameCandidate;
	}
	public void setLastNameCandidate(String lastNameCandidate) {
		this.lastNameCandidate = lastNameCandidate;
	}
	public Date getDateOfBirthCandidate() {
		return dateOfBirthCandidate;
	}
	public void setDateOfBirthCandidate(Date dateOfBirthCandidate) {
		this.dateOfBirthCandidate = dateOfBirthCandidate;
	}
	public JobOffer getJobOffer() {
		return jobOffer;
	}
	public void setJobOffer(JobOffer jobOffer) {
		this.jobOffer= jobOffer;
	}
	public String getPhoneOfCandidate() {
		return phoneOfCandidate;
	}
	public void setPhoneOfCandidate(String phoneOfCandidate) {
		this.phoneOfCandidate = phoneOfCandidate;
	}
	public String getEmailOfCandidate() {
		return emailOfCandidate;
	}
	public void setEmailOfCandidate(String emailOfCandidate) {
		this.emailOfCandidate = emailOfCandidate;
	}



	/*public Interview getInterview() {
		return interview;
	}



	public void setInterview(Interview interview) {
		this.interview = interview;
	}*/



	@Override
	public String toString() {
		return "JobRequest [id=" + id + ", dateOfRequest=" + dateOfRequest + ", nameCandidate=" + nameCandidate
				+ ", lastNameCandidate=" + lastNameCandidate + ", dateOfBirthCandidate=" + dateOfBirthCandidate
				+ ", phoneOfCandidate=" + phoneOfCandidate + ", emailOfCandidate=" + emailOfCandidate + ", jobOffer name=" + jobOffer.getName() + ", state=" + state + "]";
	}



	public Integer getMark() {
		return mark;
	}



	public void setMark(Integer mark) {
		this.mark = mark;
	}

   
}
