package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: TaskPK
 *
 */
@Embeddable

public class TaskPK implements Serializable {

	
	private int idProject;
	private int idEmployee;
	private String name;
	private static final long serialVersionUID = 1L;

	public TaskPK() {
		super();
	}   
	
	


	public TaskPK(int idProject, int idEmployee, String name) {
		super();
		this.idProject = idProject;
		this.idEmployee = idEmployee;
		this.name = name;
	}





	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idEmployee;
		result = prime * result + idProject;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskPK other = (TaskPK) obj;
		if (idEmployee != other.idEmployee)
			return false;
		if (idProject != other.idProject)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}




	




	


	
	
	
	
   
	
}
