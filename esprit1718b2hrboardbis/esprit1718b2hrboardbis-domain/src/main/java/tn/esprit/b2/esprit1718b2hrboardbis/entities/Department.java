package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Department
 *
 */
@Entity

public class Department implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	@ManyToMany(mappedBy="departments",fetch=FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.REMOVE,CascadeType.MERGE})
	private List<Employee> employees;
	@OneToMany(mappedBy="department" ,cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	private List<JobOffer> jobOffers;
	private static final long serialVersionUID = 1L;

	public Department() {
		super();
	}
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	

	public List<JobOffer> getJobOffers() {
		return jobOffers;
	}


	public void setJobOffers(List<JobOffer> jobOffers) {
		this.jobOffers = jobOffers;
	}


	public Department(String name, List<Employee> employees, List<JobOffer> jobOffers) {
		super();
		this.name = name;
	
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	 

	public List<Employee> getEmployees() {
		return employees;
	}


	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}


	@Override
	public String toString() {

for (Employee employee : employees) {
System.out.println(	employee.toString());
}
		return "Department [id=" + id + ", name=" + name + "]";
		
	}
   
}
