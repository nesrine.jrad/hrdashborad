package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.util.Date;

public class SeanceListView {
	private Training training;
	private Trainer trainer;
	private Employee employee;
	private Date date;
	private String salle;
	private SeancePk seancePk;
	
	
	public SeanceListView() {
		super();
	}
	
	
	public SeanceListView(Seance seance) {
		super();
		this.training =seance.getTraining();
		this.trainer = seance.getTrainer();
		this.employee = seance.getEmployee();
		this.date = seance.getSeancePk().getDateSeance();
		this.salle = seance.getSalle();
		this.seancePk=seance.getSeancePk();
	}


	public Training getTraining() {
		return training;
	}


	public void setTraining(Training training) {
		this.training = training;
	}


	public Trainer getTrainer() {
		return trainer;
	}


	public void setTrainer(Trainer trainer) {
		this.trainer = trainer;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getSalle() {
		return salle;
	}


	public void setSalle(String salle) {
		this.salle = salle;
	}


	public SeancePk getSeancePk() {
		return seancePk;
	}


	public void setSeancePk(SeancePk seancePk) {
		this.seancePk = seancePk;
	}
	
	
	
	
	

}
