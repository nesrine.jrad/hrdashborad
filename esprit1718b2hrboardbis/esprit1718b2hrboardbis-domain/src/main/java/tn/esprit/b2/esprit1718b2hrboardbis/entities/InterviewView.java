package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

public class InterviewView {
    private int id;
	private String jobRequest;
	private Date dateOfInterview;
	private String timeOfInterview;
	private float evalution;
	private Integer employee;
	private String jobOffer;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getJobRequest() {
		return jobRequest;
	}
	public void setJobRequest(String jobRequest) {
		this.jobRequest = jobRequest;
	}
	public Date getDateOfInterview() {
		return dateOfInterview;
	}
	public void setDateOfInterview(Date dateOfInterview) {
		this.dateOfInterview = dateOfInterview;
	}
	public String getTimeOfInterview() {
		return timeOfInterview;
	}
	public void setTimeOfInterview(String timeOfInterview) {
		this.timeOfInterview = timeOfInterview;
	}
	public float getEvalution() {
		return evalution;
	}
	public void setEvalution(float evalution) {
		this.evalution = evalution;
	}
	public Integer getEmployee() {
		return employee;
	}
	public void setEmployee(Integer employee) {
		this.employee = employee;
	}
	public String getJobOffer() {
		return jobOffer;
	}
	public void setJobOffer(String jobOffer) {
		this.jobOffer = jobOffer;
	}
	public InterviewView(int id, String jobRequest, Date dateOfInterview, String timeOfInterview, float evalution,
			Integer employee, String jobOffer) {
		super();
		this.id = id;
		this.jobRequest = jobRequest;
		this.dateOfInterview = dateOfInterview;
		this.timeOfInterview = timeOfInterview;
		this.evalution = evalution;
		this.employee = employee;
		this.jobOffer = jobOffer;
	}
	

}
