package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: AdditionalHour
 *
 */
@Entity

public class AdditionalHour implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private Date dateOfHour;
	private AdditionalHourType type;
	private float duration;
	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	@ManyToOne
	private Employee employee;
	private static final long serialVersionUID = 1L;

	public AdditionalHour() {
		super();
	}

	public AdditionalHour(Date dateOfHour, AdditionalHourType type, Employee employee) {
		super();
		this.dateOfHour = dateOfHour;
		this.type = type;
		this.employee = employee;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateOfHour() {
		return dateOfHour;
	}

	public void setDateOfHour(Date dateOfHour) {
		this.dateOfHour = dateOfHour;
	}

	public AdditionalHourType getType() {
		return type;
	}

	public void setType(AdditionalHourType type) {
		this.type = type;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
   
}
