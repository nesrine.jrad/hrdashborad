package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Project
 *
 */
@Entity

public class Project implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name;
	private Date dateOfBeginning;
	private Date expectedDateEnd;
	private Date dateOfEnd;
	
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@OneToMany (mappedBy="project",cascade =CascadeType.REMOVE, fetch=FetchType.EAGER)
	private List<Task> tasks;
	private static final long serialVersionUID = 1L;

	public Project() {
		super();
	}
	
	
	
	public Project(String name, Date dateOfBeginning, Date expectedDateEnd) {
		super();
		this.name = name;
		this.dateOfBeginning = dateOfBeginning;
		this.expectedDateEnd=expectedDateEnd;
		this.dateOfEnd=null;
		this.status = Status.IN_PROGRESS;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public Date getDateOfBeginning() {
		return this.dateOfBeginning;
	}

	public void setDateOfBeginning(Date dateOfBeginning) {
		this.dateOfBeginning = dateOfBeginning;
	}
	public Date getDateOfEnd() {
		return dateOfEnd;
	}
	public void setDateOfEnd(Date dateOfEnd) {
		this.dateOfEnd = dateOfEnd;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		if (status.toString().equals("DONE")){
			this.dateOfEnd= Date.valueOf(LocalDate.now());
		}
		this.status = status;
	}
	public List<Task> getTasks() {
		return tasks;
	}
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	
	
	public Date getExpectedDateEnd() {
		return expectedDateEnd;
	}



	public void setExpectedDateEnd(Date expectedDateEnd) {
		this.expectedDateEnd = expectedDateEnd;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public void assignTasksToThisProject(List<Task> tasks){
		this.setTasks(tasks);
		for(Task task: tasks){
			task.setProject(this);
		}
	}
   
	
}
