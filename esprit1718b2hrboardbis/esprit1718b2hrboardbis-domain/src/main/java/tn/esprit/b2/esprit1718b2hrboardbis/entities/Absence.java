package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Absence
 *
 */
@Entity

public class Absence implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private Date fromDate;
	private Date toDate;
	private Justification justified;
	@ManyToOne
	private Employee employee;
	private static final long serialVersionUID = 1L;

	public Absence() {
		super();
	} 
	
	
	public Absence(Date fromDate, Date toDate, Justification justified, Employee employee) {
		super();
		
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.justified = justified;
		this.employee = employee;
	}


	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}   
	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Justification getJustified() {
		return justified;
	}
	public void setJustified(Justification justified) {
		this.justified = justified;
	}


	@Override
	public String toString() {
		return "Absence [fromDate=" + fromDate + ", toDate=" + toDate + ", justified=" + justified + ", employee="
				+ employee + "]";
	}
	public Boolean isJustified(){
		if(justified==Justification.JUSTIFIED){
			
			return true;
		}
		return false ;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	
	
   
}
