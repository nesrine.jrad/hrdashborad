package tn.esprit.b2.esprit1718b2hrboardbis.entities;

import java.sql.Date;



public class JobRequestView {
private int id;
	
	private Date dateOfRequest;
	private String nameCandidate;
	private String lastNameCandidate;
	private Date dateOfBirthCandidate;
	private String phoneOfCandidate;
	private String emailOfCandidate;
	private String post;
	private String state;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDateOfRequest() {
		return dateOfRequest;
	}
	public void setDateOfRequest(Date dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}
	public String getNameCandidate() {
		return nameCandidate;
	}
	public void setNameCandidate(String nameCandidate) {
		this.nameCandidate = nameCandidate;
	}
	public String getLastNameCandidate() {
		return lastNameCandidate;
	}
	public void setLastNameCandidate(String lastNameCandidate) {
		this.lastNameCandidate = lastNameCandidate;
	}
	public Date getDateOfBirthCandidate() {
		return dateOfBirthCandidate;
	}
	public void setDateOfBirthCandidate(Date dateOfBirthCandidate) {
		this.dateOfBirthCandidate = dateOfBirthCandidate;
	}
	public String getPhoneOfCandidate() {
		return phoneOfCandidate;
	}
	public void setPhoneOfCandidate(String phoneOfCandidate) {
		this.phoneOfCandidate = phoneOfCandidate;
	}
	public String getEmailOfCandidate() {
		return emailOfCandidate;
	}
	public void setEmailOfCandidate(String emailOfCandidate) {
		this.emailOfCandidate = emailOfCandidate;
	}

	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public JobRequestView(int id, Date dateOfRequest, String nameCandidate,
			String lastNameCandidate, Date dateOfBirthCandidate, String phoneOfCandidate,
			String emailOfCandidate, String post, String state) {
		super();
		this.id = id;
		this.dateOfRequest = dateOfRequest;
		this.nameCandidate = nameCandidate;
		this.lastNameCandidate = lastNameCandidate;
		this.dateOfBirthCandidate = dateOfBirthCandidate;
		this.phoneOfCandidate = phoneOfCandidate;
		this.emailOfCandidate = emailOfCandidate;
		this.post = post;
		this.state = state;
	}
	
	


}
