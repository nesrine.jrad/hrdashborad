package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.recrutement;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;

public class TestDisplayAllInterview {
	public static void main (String[] argh) throws NamingException {
		Context context=new InitialContext();
		List<Interview> interviews= new ArrayList<Interview>();
		InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
		interviews=interviewServicesRemote.findAll();
		interviews.forEach(System.out::println);
	}
	
}
