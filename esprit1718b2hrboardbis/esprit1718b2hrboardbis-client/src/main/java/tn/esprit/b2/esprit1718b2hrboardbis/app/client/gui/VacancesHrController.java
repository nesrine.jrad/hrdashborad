/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXRadioButton;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.SendMail;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class VacancesHrController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXListView<Leave> ListHrLeaves;
    @FXML
    private ToggleGroup radiohrleaves;
    @FXML
    private DatePicker datePickerLeaves;
    @FXML
    private JFXButton btnHrLeavesSearch;
    @FXML
    private ImageView imageSearch;
    @FXML
    private ImageView imgPDF;
    private ObservableList<Leave> data=FXCollections.observableArrayList();
    Context context;
    LeaveServiceRemote leaveServiceRemote ;
	EmployeeServiceRemote employeeServiceRemote;
	int connectID;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	connectID=1;
  	  try {
			 context= new InitialContext();
			 leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
			 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         imageSearch.setImage(new Image("assets/search.png"));
         imgPDF.setImage(new Image("assets/pdf.png"));
         
     
         
          HBox hbox = new HBox();
       data.clear();
       List<Leave> leaves = leaveServiceRemote.findAll();
       Collections.reverse(leaves);
       for (Leave leave : leaves) {
			data.add(leave);
		}
               ListHrLeaves.setCellFactory(new Callback<ListView<Leave>, ListCell<Leave>>(){
           @Override
           public ListCell<Leave> call(ListView<Leave> args0) {
               Image img= new Image("assets/interview(1).png");
               ImageView imageview=new ImageView(img);
               Label label=new Label("");
               JFXButton buttAnnuler=new JFXButton();
               //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
              // buttAnnuler.setStyle("-fx-background-image: url('/assets/delete.png');-fx-font-size: 1em; ");
               buttAnnuler.setMinHeight(30);
               buttAnnuler.setMinWidth(30);
               JFXButton buttProfil=new JFXButton();
              // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
              buttProfil.setMinHeight(30);
               buttProfil.setMinWidth(32);
               JFXButton buttconfirm=new JFXButton();
               buttconfirm.setMinHeight(30);
               buttconfirm.setMinWidth(30);
               buttconfirm.setStyle("-fx-background-image: url('/assets/confirm.png');");
               buttconfirm.setAlignment(Pos.CENTER_RIGHT);
               HBox hbox = new HBox();
               hbox.getChildren().addAll(imageview, label,buttProfil,buttAnnuler,buttconfirm);
               
              
               
              
               
               
               ListCell<Leave> cell;
               int i = 0 ;
               cell = new ListCell<Leave>(){
                   
                   @Override
                   protected void updateItem(Leave r ,boolean b){
                       
                       super.updateItem(r,b);
                       
                         if(r != null){
                        	 if(r.isApproved()==false){
                      		
                     		 hbox.setStyle("-fx-background-color:#ff9999;");
                   	   }else{                         
                          buttconfirm.setDisable(true);
                   		 hbox.setStyle("-fx-background-color:#bef67a;");}
                        	 if(r.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now()))){
                        		 
                        		 buttconfirm.setDisable(true);
                        	 }
                             label.setText(r.getEmployee().getName() +" " +r.getEmployee().getFamilyName() + " Description :  "+r.getDescription()+" "+ r.getFromDate() + " "+ r.getToDate());
                           setGraphic(hbox);
                           buttconfirm.setOnAction((event) -> {
                               
                               
                               System.out.print("confirm");
                  Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
alert.setTitle("Confirm Demand");
alert.setHeaderText("you will confrim  this  demand , one email will be send to the employee ");
alert.setContentText("Are you ok with this?");

Optional<ButtonType> result = alert.showAndWait();
if (result.get() == ButtonType.OK){
	leaveServiceRemote.confirmLeave(r.getId());
	  initialize(url,rb);
	  SendMail mail =new SendMail();
	  mail.sendmail("m2adashbored@gmail.com","kadhem94@gmail.com", "hrbis2018", "your leave  of"+ r.getFromDate() +" to "+r.getToDate() +" is Confirmed "+"\n \nM2A HR dashbored ");
} else {
// ... user chose CANCEL or closed the dialog
}
                      

             });  
                         
                         }
                          
                         
                         
                      
                             
                           
                          
                           
                            
                           
                           
                       
                    
                       }
                       
                     
                
               };
               
               
            return cell;    
           }
            
          
       
                });
        ListHrLeaves.setItems(data);
      
       
        
       
    }    
    
 
    
     @FXML 
    public void HomeHrManagerInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }
    
     @FXML 
    public void filter() throws IOException{ 
    	 
    	 JFXRadioButton radioJustification=(JFXRadioButton)radiohrleaves.getSelectedToggle();
 	 	System.out.println(radioJustification.getText());
 	 	if(radioJustification.getText().equals("passed")){
 	 		connectID=1;
 	    	  try {
 				 context= new InitialContext();
 				 leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
 				 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
 			} catch (NamingException e1) {
 				// TODO Auto-generated catch block
 				e1.printStackTrace();
 			}
 				
 	         Image img = new Image("assets/home.png");
 	         imgBtHome.setImage(img);
 	         img = new Image("assets/projectmenu.png");
 	         imgbtProjects.setImage(img);
 	         img = new Image("assets/lecture.png");
 	         imgBtTrainig.setImage(img);
 	         img = new Image("assets/luggage.png");
 	         imgBtVac.setImage(img);
 	         img = new Image("assets/calendar.png");
 	         imgBtAbsences.setImage(img);
 	         img = new Image("assets/users.png");
 	         imgBtProfile.setImage(img);
 	         img = new Image("assets/chatting.png");
 	        imgBtCommunication.setImage(img);
 	         img = new Image("assets/salary.png");
 	         imgBtSalaire.setImage(img);
 	          img = new Image("assets/logout.png");
 	         imgBtlogout.setImage(img);
 	          img = new Image("assets/hi.png");
 	         imgHi.setImage(img);
 	         img = new Image("assets/hrhead.jpg");
 	         headimg.setImage(img);
 	         img = new Image("assets/M2A.png");
 	         logoImg.setImage(img);
 	         
 	     
 	        //imgPDF.setImage(new Image("assets/pdf.png"));
 	        HBox hbox = new HBox();
 	       data.clear();
 	             List<Leave> leaves = leaveServiceRemote.findAll();
 	             Collections.reverse(leaves);
 	             for (Leave leave : leaves) {
 	            	 if(leave.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now())))
 					data.add(leave);
 				}
 	            ListHrLeaves.setCellFactory(new Callback<ListView<Leave>, ListCell<Leave>>(){
 	               @Override
 	               public ListCell<Leave> call(ListView<Leave> args0) {
 	                   Image img= new Image("assets/interview(1).png");
 	                   ImageView imageview=new ImageView(img);
 	                   Label label=new Label("");
 	                   JFXButton buttAnnuler=new JFXButton();
 	                   //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
 	                  // buttAnnuler.setStyle("-fx-background-image: url('/assets/delete.png');-fx-font-size: 1em; ");
 	                   buttAnnuler.setMinHeight(30);
 	                   buttAnnuler.setMinWidth(30);
 	                   JFXButton buttProfil=new JFXButton();
 	                  // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
 	                  buttProfil.setMinHeight(30);
 	                   buttProfil.setMinWidth(32);
 	                   JFXButton buttconfirm=new JFXButton();
 	                   buttconfirm.setMinHeight(30);
 	                   buttconfirm.setMinWidth(30);
 	                   buttconfirm.setStyle("-fx-background-image: url('/assets/confirm.png');");
 	                   buttconfirm.setAlignment(Pos.CENTER_RIGHT);
 	                   HBox hbox = new HBox();
 	                   hbox.getChildren().addAll(imageview, label,buttProfil,buttAnnuler,buttconfirm);
 	                   
 	                  
 	                   
 	                  
 	                   
 	                   
 	                   ListCell<Leave> cell;
 	                   int i = 0 ;
 	                   cell = new ListCell<Leave>(){
 	                       
 	                       @Override
 	                       protected void updateItem(Leave r ,boolean b){
 	                           
 	                           super.updateItem(r,b);
 	                           
 	                             if(r != null){
 	                            	 if(r.isApproved()==false){
 	                          		
 	                         		 hbox.setStyle("-fx-background-color:#ff9999;");
 	                       	   }else{                         
 	                              buttconfirm.setDisable(true);
 	                       		 hbox.setStyle("-fx-background-color:#bef67a;");}
 	                            	 if(r.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now()))){
 	                            		 
 	                            		 buttconfirm.setDisable(true);
 	                            	 }
 	                                 label.setText(r.getEmployee().getName() +" " +r.getEmployee().getFamilyName() + " Description :  "+r.getDescription()+" "+ r.getFromDate() + " "+ r.getToDate());
 	                               setGraphic(hbox);
 	                               buttconfirm.setOnAction((event) -> {
 	                                   
 	                                   
 	                                   System.out.print("confirm");
 	                      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
 	    alert.setTitle("Confirm Demand");
 	    alert.setHeaderText("you will confrim  this  demand , one email will be send to the employee ");
 	    alert.setContentText("Are you ok with this?");

 	    Optional<ButtonType> result = alert.showAndWait();
 	    if (result.get() == ButtonType.OK){
 	    	leaveServiceRemote.confirmLeave(r.getId());
 	    	  FXMLLoader loader = new FXMLLoader(getClass().getResource("Vacances.fxml"));
              Parent root;
			try {
				root = (Parent) loader.load();
				Scene newScene = new Scene(root);
	              Stage newStage = new Stage();
	              newStage.setScene(newScene);
	              newStage.show();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
              
              
              Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
              // do what you have to do
              stage.close();
 	    	  SendMail mail =new SendMail();
 	    	  mail.sendmail("m2adashbored@gmail.com","kadhem94@gmail.com", "hrbis2018", "your leave  of"+ r.getFromDate() +" to "+r.getToDate() +" is Confirmed "+"\n \nM2A HR dashbored ");
 	    } else {
 	    // ... user chose CANCEL or closed the dialog
 	    }
 	                          

 	                 });  
 	                             
 	                             }
 	                              
 	                             
 	                             
 	                          
 	                                 
 	                               
 	                              
 	                               
 	                                
 	                               
 	                               
 	                           
 	                        
 	                           }
 	                           
 	                         
 	                    
 	                   };
 	                   
 	                   
 	                return cell;    
 	               }
 	                
 	              
 	           
 	                    });
 	            ListHrLeaves.setItems(data);
 	          
 	 		
 	 	}else if(radioJustification.getText().equals("Waiting")){
 	 		connectID=1;
 	    	  try {
 				 context= new InitialContext();
 				 leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
 				 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
 			} catch (NamingException e1) {
 				// TODO Auto-generated catch block
 				e1.printStackTrace();
 			}
 				
 	         Image img = new Image("assets/home.png");
 	         imgBtHome.setImage(img);
 	         img = new Image("assets/projectmenu.png");
 	         imgbtProjects.setImage(img);
 	         img = new Image("assets/lecture.png");
 	         imgBtTrainig.setImage(img);
 	         img = new Image("assets/luggage.png");
 	         imgBtVac.setImage(img);
 	         img = new Image("assets/calendar.png");
 	         imgBtAbsences.setImage(img);
 	         img = new Image("assets/users.png");
 	         imgBtProfile.setImage(img);
 	         img = new Image("assets/chatting.png");
 	        imgBtCommunication.setImage(img);
 	         img = new Image("assets/salary.png");
 	         imgBtSalaire.setImage(img);
 	          img = new Image("assets/logout.png");
 	         imgBtlogout.setImage(img);
 	          img = new Image("assets/hi.png");
 	         imgHi.setImage(img);
 	         img = new Image("assets/hrhead.jpg");
 	         headimg.setImage(img);
 	         img = new Image("assets/M2A.png");
 	         logoImg.setImage(img);
 	         
 	  
 	       imgPDF.setImage(new Image("assets/pdf.png"));
 	        HBox hbox = new HBox();
 	       data.clear();
 	             List<Leave> leaves = leaveServiceRemote.findAll();
 	             Collections.reverse(leaves);
 	             for (Leave leave : leaves) {
 	            	 if(leave.getFromDate().after(java.sql.Date.valueOf(java.time.LocalDate.now())))
 					data.add(leave);
 				}
 	            ListHrLeaves.setCellFactory(new Callback<ListView<Leave>, ListCell<Leave>>(){
 	               @Override
 	               public ListCell<Leave> call(ListView<Leave> args0) {
 	                   Image img= new Image("assets/interview(1).png");
 	                   ImageView imageview=new ImageView(img);
 	                   Label label=new Label("");
 	                   JFXButton buttAnnuler=new JFXButton();
 	                   //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
 	                  // buttAnnuler.setStyle("-fx-background-image: url('/assets/delete.png');-fx-font-size: 1em; ");
 	                   buttAnnuler.setMinHeight(30);
 	                   buttAnnuler.setMinWidth(30);
 	                   JFXButton buttProfil=new JFXButton();
 	                  // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
 	                  buttProfil.setMinHeight(30);
 	                   buttProfil.setMinWidth(32);
 	                   JFXButton buttconfirm=new JFXButton();
 	                   buttconfirm.setMinHeight(30);
 	                   buttconfirm.setMinWidth(30);
 	                   buttconfirm.setStyle("-fx-background-image: url('/assets/confirm.png');");
 	                   buttconfirm.setAlignment(Pos.CENTER_RIGHT);
 	                   HBox hbox = new HBox();
 	                   hbox.getChildren().addAll(imageview, label,buttProfil,buttAnnuler,buttconfirm);
 	                   
 	                  
 	                   
 	                  
 	                   
 	                   
 	                   ListCell<Leave> cell;
 	                   int i = 0 ;
 	                   cell = new ListCell<Leave>(){
 	                       
 	                       @Override
 	                       protected void updateItem(Leave r ,boolean b){
 	                           
 	                           super.updateItem(r,b);
 	                           
 	                             if(r != null){
 	                            	 if(r.isApproved()==false){
 	                          		
 	                         		 hbox.setStyle("-fx-background-color:#ff9999;");
 	                       	   }else{                         
 	                              buttconfirm.setDisable(true);
 	                       		 hbox.setStyle("-fx-background-color:#bef67a;");}
 	                            	 if(r.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now()))){
 	                            		 
 	                            		 buttconfirm.setDisable(true);
 	                            	 }
 	                                 label.setText(r.getEmployee().getName() +" " +r.getEmployee().getFamilyName() + " Description :  "+r.getDescription()+" "+ r.getFromDate() + " "+ r.getToDate());
 	                               setGraphic(hbox);
 	                               buttconfirm.setOnAction((event) -> {
 	                                   
 	                                   
 	                                   System.out.print("confirm");
 	                      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
 	    alert.setTitle("Confirm Demand");
 	    alert.setHeaderText("you will confrim  this  demand , one email will be send to the employee ");
 	    alert.setContentText("Are you ok with this?");

 	    Optional<ButtonType> result = alert.showAndWait();
 	    if (result.get() == ButtonType.OK){
 	    	leaveServiceRemote.confirmLeave(r.getId());
 	    	  FXMLLoader loader = new FXMLLoader(getClass().getResource("Vacances.fxml"));
              Parent root;
			try {
				root = (Parent) loader.load();
				Scene newScene = new Scene(root);
	              Stage newStage = new Stage();
	              newStage.setScene(newScene);
	              newStage.show();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
              
              
              Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
              // do what you have to do
              stage.close();
 	    	  SendMail mail =new SendMail();
 	    	  mail.sendmail("m2adashbored@gmail.com","kadhem94@gmail.com", "hrbis2018", "your leave  of"+ r.getFromDate() +" to "+r.getToDate() +" is Confirmed "+"\n \nM2A HR dashbored ");
 	    } else {
 	    // ... user chose CANCEL or closed the dialog
 	    }
 	                          

 	                 });  
 	                             
 	                             }
 	                              
 	                             
 	                             
 	                          
 	                                 
 	                               
 	                              
 	                               
 	                                
 	                               
 	                               
 	                           
 	                        
 	                           }
 	                           
 	                         
 	                    
 	                   };
 	                   
 	                   
 	                return cell;    
 	               }
 	                
 	              
 	           
 	                    });
 	            ListHrLeaves.setItems(data);
 	          
 	 		
 	 	}
     
    }
    
     @FXML 
    public void pdfGenerator() throws IOException, NamingException{ 
    	 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
 	  List<Leave>leaves;
 	   leaves= leaveServiceRemote.findAll();
 		Collections.reverse(leaves);
          
 	  Document document = new Document();
       try{
           PdfWriter.getInstance(document, new FileOutputStream("/home/kadhem/LeavesList"+java.sql.Date.valueOf(java.time.LocalDate.now())+".pdf"));
           document.open();
           Font code = new Font(FontFamily.COURIER, 20, Font.BOLD, BaseColor.GREEN);
           Paragraph p1 = new Paragraph("Leaves before "+java.sql.Date.valueOf(java.time.LocalDate.now()),code);
           
           document.addHeader("Leaves Request","Before"+java.sql.Date.valueOf(java.time.LocalDate.now()));
           document.addTitle("Leaves  of All Employees");
           com.itextpdf.text.Image img = com.itextpdf.text.Image.getInstance("/home/kadhem/git/esprit1718b2hrboardbis/esprit1718b2hrboardbis/esprit1718b2hrboardbis-client/src/main/java/assets/M2A.png");
           img.setAbsolutePosition(40f, 10f);
              document.add(img);
          document.add(p1);
        
           for (Leave leave : leaves) {
        	   Font code1 = new Font(FontFamily.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK);
         	  Paragraph p2 = new Paragraph("Name :"+leave.getEmployee().getName()+" FamilyName: "+leave.getEmployee().getFamilyName()+" From:"+ leave.getFromDate()+ " To: "+leave.getToDate() +" Description: "+leave.getDescription(),code1);	
         	
         	  document.add(p2);
           }
           
				
         
       }
       catch(Exception e){
           System.out.println(e);
       }
       document.close();
    
    }
     
     @FXML
     public void absencInterface() throws IOException{
    	 
    	 
    	 
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("absencesHr.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();

     }
    
}
