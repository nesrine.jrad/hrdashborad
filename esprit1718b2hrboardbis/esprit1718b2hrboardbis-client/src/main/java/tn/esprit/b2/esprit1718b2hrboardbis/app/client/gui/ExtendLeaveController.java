/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.SendMail;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class ExtendLeaveController implements Initializable {

    @FXML
    private DatePicker dateFromDemand;
    @FXML
    private DatePicker dateDemandTo;
    @FXML
    private JFXButton btnDemand;
    @FXML
    private ImageView imgM2A;
    LeaveServiceRemote leaveServiceRemote ;
    Context context;
    public int idLeave;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
    	 try {
			context= new InitialContext();
			leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 

       imgM2A.setImage(new Image("assets/M2A.png"));
       
       btnDemand.setOnAction((event) -> {
    	   
    	  Leave l =  leaveServiceRemote.find(idLeave);
    	  l.setApproved(false);
    	  if((dateFromDemand.getValue().isBefore(java.time.LocalDate.now()))||(dateDemandTo.getValue().isBefore(java.time.LocalDate.now()))
                  ||(dateDemandTo.getValue().isBefore(dateFromDemand.getValue()))){
                    Alert alert = new Alert(AlertType.WARNING);
                                alert.setTitle("error in input data");
                                alert.setHeaderText("check dates");
                                
                                
                                ButtonType buttonTypeCancel = new ButtonType("non", ButtonData.CANCEL_CLOSE);
                                
                                alert.getButtonTypes().setAll(buttonTypeCancel);
                                
                                Optional<ButtonType> result = alert.showAndWait();
          }
          else{ 
        	  l.setFromDate(java.sql.Date.valueOf(dateFromDemand.getValue()));
        	  l.setToDate(java.sql.Date.valueOf(dateDemandTo.getValue()));
        	  leaveServiceRemote.update(l);
        	  
        	  Alert alert = new Alert(AlertType.WARNING);
              alert.setTitle("Extend leave ");
              alert.setHeaderText("request send to Hr manager");
              SendMail mail =new SendMail();
        	  mail.sendmail("kadhem94@gmail.com","m2adashbored@gmail.com", "kasouma1994", "Extend Leave\n"+l.getDescription()+"\nFrom :"+l.getFromDate()+"\n To :"+l.getToDate());

              
              ButtonType buttonTypeCancel = new ButtonType("ok", ButtonData.CANCEL_CLOSE);
              
              alert.getButtonTypes().setAll(buttonTypeCancel);
              
              Optional<ButtonType> result = alert.showAndWait();
              FXMLLoader loader = new FXMLLoader(getClass().getResource("Vacances.fxml"));
              Parent root;
			try {
				root = (Parent) loader.load();
				Scene newScene = new Scene(root);
	              Stage newStage = new Stage();
	              newStage.setScene(newScene);
	              newStage.show();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
              
              
              Stage stage = (Stage) dateFromDemand.getScene().getWindow();
              // do what you have to do
              stage.close();
          }
    	   
       });
    }
	public int getIdLeave() {
		return idLeave;
	}
	public void setIdLeave(int idLeave) {
		this.idLeave = idLeave;
	}    
    
}
