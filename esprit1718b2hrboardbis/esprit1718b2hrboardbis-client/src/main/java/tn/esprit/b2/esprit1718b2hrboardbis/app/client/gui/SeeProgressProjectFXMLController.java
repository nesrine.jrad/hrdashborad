/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSpinner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.SendMail;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Status;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskListView;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class SeeProgressProjectFXMLController implements Initializable {

    @FXML
    private AnchorPane mainAnchor;
    @FXML
    private Label remainingDaysLabel;
    @FXML
    private Label progressLabel;
    @FXML
    private PieChart progressPieChart;
    @FXML
    private TableView<Employee> employeesTable;
    @FXML
    private TableColumn<Employee, String> idNameEmployeeColumn;
    @FXML
    private TableColumn<Employee, String> idFamilyNameColumn;
    @FXML
    private JFXButton makeProjectDoneBtn;
    @FXML
    private JFXButton addNewTasksBtn;
    @FXML
    private JFXSpinner spinnerAnimation;
    @FXML
    private Label numberEmployeesLabel;
    @FXML
    private JFXButton seeTasksBtn;
    @FXML
    private JFXButton seeEmployeesBtn;
    @FXML
    private TableView<TaskListView> tasksTable;
    @FXML
    private TableColumn<TaskListView, String> idNameTaskColumn;
    @FXML
    private TableColumn<TaskListView, StatusTask> idStatusTaskColumn;
    
    private ObservableList<Employee> dataEmployees;
    private ObservableList<TaskListView> dataTasks;
    ///id project should not be static
    private int idProject=1;
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	tasksTable.setVisible(false);
    	progressPieChart.setVisible(false);
    	spinnerAnimation.setVisible(false);
    	makeProjectDoneBtn.setVisible(false);
    	tasksTable.setItems(null);
		employeesTable.setItems(null);
    	idNameTaskColumn.setCellValueFactory(new PropertyValueFactory<>("nameTask"));
    	idStatusTaskColumn.setCellValueFactory(new PropertyValueFactory<>("statusTask"));


    }    

    @FXML
    private void addNewTasks(ActionEvent event) throws NamingException, IOException {
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AddTasksToProjects.fxml"));
        Parent root = (Parent) loader.load();
        AddTasksToProjectsController addTasksToProjectsController = new AddTasksToProjectsController();
        addTasksToProjectsController = loader.getController();
        addTasksToProjectsController.getTheProjectFromPreviousScreen(projectServiceRemote.find(idProject));
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    	
    }


    @FXML
    private void displayTasks(ActionEvent event) throws NamingException {
    	employeesTable.setVisible(false);
    	tasksTable.setVisible(true);
    	seeEmployeesBtn.setVisible(true);
    	seeTasksBtn.setVisible(false);	
		tasksTable.setItems(dataTasks);
		
    }

    @FXML
    private void displayEmployees(ActionEvent event) throws NamingException {
    	seeEmployeesBtn.setVisible(false);
    	seeTasksBtn.setVisible(true);
    	tasksTable.setVisible(false);
    	employeesTable.setVisible(true);
    	employeesTable.setItems(dataEmployees);
    }

    @FXML
    private void updateTableViews(MouseEvent event) throws NamingException {
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		List<Employee> employees = projectServiceRemote.getListEmployees(idProject);
		Project project = projectServiceRemote.find(idProject);
		List<Task> tasks = taskServiceRemote.getTasksByProject(project);
		String numberEmployees = employees.size()+" # Are working on this project";
		numberEmployeesLabel.setText(numberEmployees);
		dataEmployees = FXCollections.observableList(employees);
		dataTasks = this.convertListIntoObservableList(tasks);
		
		List<Task> tasksToDo = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.ToDO);
		List<Task> tasksDoing = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.DOING);
		List<Task> tasksDone = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.DONE);
		int totalTasksToDo = tasksToDo.size();
		int totalTasksDoing = tasksDoing.size();
		int totalTasksDone = tasksDone.size();
		int sumOfTasks = totalTasksToDo + totalTasksDoing + totalTasksDone;
		
		progressPieChart.getData().clear();
		
		PieChart.Data numberTasksToDo = new PieChart.Data("TO DO " ,totalTasksToDo);
		PieChart.Data numberTasksDoing = new PieChart.Data("DOING",totalTasksDoing);
		PieChart.Data numberTasksDone = new PieChart.Data("DONE",totalTasksDone);
	
		progressPieChart.getData().add(numberTasksToDo);
		progressPieChart.getData().add(numberTasksDoing);
		progressPieChart.getData().add(numberTasksDone);
		
		if (project.getStatus()==Status.DONE){
			addNewTasksBtn.setVisible(false);
			String progress = "DONE";
			progressLabel.setText(progress);
			progressLabel.setTextFill(Color.web("#006400"));
		}
		else if (project.getExpectedDateEnd()==null){
			String progress = "Date end not determined";
			progressLabel.setText(progress);
			remainingDaysLabel.setText("");
		}
		else if (Date.valueOf(LocalDate.now()).after(project.getExpectedDateEnd())){
			String progress = "IN DELAY";
			progressLabel.setText(progress);
			progressLabel.setTextFill(Color.web("#ff0000"));
			long diff =  Date.valueOf(LocalDate.now()).getTime() - project.getExpectedDateEnd().getTime();
			diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			String remaingDays = diff + " days in delay" ;				
			remainingDaysLabel.setText(remaingDays);
		}
		else {
			String progress = "IN PROGRESS";
			progressLabel.setText(progress);
			long diff = project.getExpectedDateEnd().getTime() - Date.valueOf(LocalDate.now()).getTime();
			diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			String remaingDays = diff + " remaining days to the expected end date" ;				
			remainingDaysLabel.setText(remaingDays);
		}
		
		if (sumOfTasks == totalTasksDone) {
			makeProjectDoneBtn.setVisible(true);
		}
		
    	
    }
    
  //Convert a List<Task> into Observable<TaskListView>
    public ObservableList<TaskListView> convertListIntoObservableList (List<Task> tasks) throws NamingException{
    	List<TaskListView> tasksListView = new ArrayList<>();
    	for(Task task : tasks){
    		TaskListView taskListView = new TaskListView(task);
    		tasksListView.add(taskListView);
    		
    	}
    	return FXCollections.observableList(tasksListView);

    }
    
    @FXML
    private void makeProjectDone(ActionEvent event) throws NamingException, IOException {
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");

		TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(
				"esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		Project project = projectServiceRemote.find(idProject);
		List<Task> tasksToDo = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.ToDO);
		List<Task> tasksDoing = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.DOING);
		if (tasksToDo.isEmpty() && tasksDoing.isEmpty())
		{project.setStatus(Status.DONE);
		project.setDateOfEnd(Date.valueOf(LocalDate.now()));
		projectServiceRemote.update(project);
		Alert alert = new Alert(Alert.AlertType.INFORMATION, "Your project is done ! Congratulations");
		alert.showAndWait();
		makeProjectDoneBtn.setVisible(false);
		SendMail sm = new SendMail();
		List<Employee> employees = projectServiceRemote.getListEmployees(idProject);
		for(Employee employee : employees){
			String mailsend = "m2adashbored@gmail.com";
			String mailrecevr = employee.getEmail();
			String passworduser = "hrbis2018";
			String msg = "The project " + project.getName() + "is officially done! it started on "+ project.getDateOfBeginning() + " and ended on "+ project.getDateOfEnd();
			sm.sendmail(mailsend, mailrecevr, passworduser, msg );
		}}
		else{
			Alert alert = new Alert(Alert.AlertType.ERROR, "You have some undone tasks");
			alert.showAndWait();
		}
    	
    }

	public int getIdProject() {
		return idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
    
	
    public void setIdProjectFromLastScreen(int idProject){
    	this.idProject = idProject ;
    	Context context;
		try {
			
			context = new InitialContext();
			ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
			TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
			List<Employee> employees = projectServiceRemote.getListEmployees(idProject);
			seeEmployeesBtn.setVisible(false);
			
			Project project = projectServiceRemote.find(idProject);
			String numberEmployees = employees.size()+" # Are working on this project";
			numberEmployeesLabel.setText(numberEmployees);
			dataEmployees = FXCollections.observableList(employees);
			idNameEmployeeColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
			idFamilyNameColumn.setCellValueFactory(new PropertyValueFactory<>("familyName"));

			employeesTable.setItems(dataEmployees);
			
			
			List<Task> tasks = taskServiceRemote.getTasksByProject(project);
			if (tasks != null) {
				dataTasks = this.convertListIntoObservableList(tasks);
			}
			
			List<Task> tasksToDo = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.ToDO);
			List<Task> tasksDoing = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.DOING);
			List<Task> tasksDone = projectServiceRemote.getListTasksOfProjectByStatus(idProject, StatusTask.DONE);
			int totalTasksToDo = tasksToDo.size();
			int totalTasksDoing = tasksDoing.size();
			int totalTasksDone = tasksDone.size();
			int sumOfTasks = totalTasksToDo + totalTasksDoing + totalTasksDone;

			PieChart.Data numberTasksToDo = new PieChart.Data("TO DO " ,totalTasksToDo);
			PieChart.Data numberTasksDoing = new PieChart.Data("DOING",totalTasksDoing);
			PieChart.Data numberTasksDone = new PieChart.Data("DONE",totalTasksDone);
		
			progressPieChart.getData().add(numberTasksToDo);
			progressPieChart.getData().add(numberTasksDoing);
			progressPieChart.getData().add(numberTasksDone);
			
			progressPieChart.setVisible(true);

			if (project.getStatus()==Status.DONE){
				String progress = "DONE";
				progressLabel.setText(progress);
				progressLabel.setTextFill(Color.web("#006400"));
			}
			else if (project.getExpectedDateEnd()==null){
				String progress = "Date end not determined";
				progressLabel.setText(progress);
				remainingDaysLabel.setText("");
			}
			else if (Date.valueOf(LocalDate.now()).after(project.getExpectedDateEnd())){
				String progress = "IN DELAY";
				progressLabel.setText(progress);
				progressLabel.setTextFill(Color.web("#ff0000"));
				long diff =  Date.valueOf(LocalDate.now()).getTime() - project.getExpectedDateEnd().getTime();
				diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
				String remaingDays = diff + " days in delay" ;				
				remainingDaysLabel.setText(remaingDays);
			}
			else {
				String progress = "IN PROGRESS";
				progressLabel.setText(progress);
				long diff = project.getExpectedDateEnd().getTime() - Date.valueOf(LocalDate.now()).getTime();
				diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
				String remaingDays = diff + remainingDaysLabel.getText() ;				
				remainingDaysLabel.setText(remaingDays);
			}
			
			if (sumOfTasks == totalTasksDone && project.getStatus().equals(Status.IN_PROGRESS)) {
				makeProjectDoneBtn.setVisible(true);
			}			
			
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
}
