package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

public class TestDeleteEmployee {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		employeeServiceRemote.delete(employeeServiceRemote.find(1));

	}

}
