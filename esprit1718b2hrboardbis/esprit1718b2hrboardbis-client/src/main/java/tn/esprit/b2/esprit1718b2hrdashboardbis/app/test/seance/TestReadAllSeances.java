package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.seance;

import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;

public class TestReadAllSeances {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");

		List<Seance> seances= new ArrayList<Seance>();
		seances=seanceServiceRemote.findAll();
		seances.forEach(System.out::println);
	}
}
