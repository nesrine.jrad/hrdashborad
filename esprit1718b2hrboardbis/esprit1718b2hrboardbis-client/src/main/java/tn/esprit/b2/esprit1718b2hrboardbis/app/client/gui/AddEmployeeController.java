/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import javafx.collections.FXCollections;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.SendMail;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Contract;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ContratServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * FXML Controller class
 *
 * @author USER
 */
public class AddEmployeeController implements Initializable {

    @FXML
    private JFXTextField JobOfferSalary;
    @FXML
    private JFXComboBox<String> JobOfferContract;
    @FXML
    private ImageView AjoutImg;
    @FXML
    private ImageView BAckImg;
    @FXML
    private DatePicker dateOfEnding;
    @FXML
    private JFXButton AddEmployee;
    @FXML
    private JFXButton Back;
    @FXML
    private ImageView imageJnab;
    private Interview interview;
    Context context;
    ObservableList<String> listeOfContracts=FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/socity.jpg");
    	imageJnab.setImage(img);
        img= new Image("assets/save.png");
    	AjoutImg.setImage(img);
    	img= new Image("assets/back.png");
    	BAckImg.setImage(img);
    	
       
        RoleEmployee[] roleEmployeeList;
 		roleEmployeeList=RoleEmployee.values();
 		for (RoleEmployee roleEmployee : roleEmployeeList) {
 			System.out.println(roleEmployee);
 			String role=String.valueOf(roleEmployee);
 			listeOfContracts.add(role);
 			System.out.println(role);
 			
 		}
 		listeOfContracts.forEach(System.out::println);

		JobOfferContract.setItems(listeOfContracts);	
		dateOfEnding.setValue(LocalDate.now());
    }    

    private static void GenererAlerte(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(msg);
        alert.showAndWait(); 
    }
    @FXML
    private void AddEmployee(ActionEvent event) throws NamingException {
    	java.sql.Date gettedDatePickerDate = java.sql.Date.valueOf(dateOfEnding.getValue());
    	java.sql.Date datenow = java.sql.Date.valueOf(LocalDate.now());
  
    	String msg="";
    	if (JobOfferContract.getValue()==null) {
    		msg="Field Contract is required !";
    	    GenererAlerte(msg);
    	}

    	else if (JobOfferSalary.getText().equals(null)) {
    		msg="You have to affect Salary to this Employee !";
    	    GenererAlerte(msg);
    	}
    	else if (gettedDatePickerDate==null || gettedDatePickerDate.before(java.sql.Date.valueOf(java.time.LocalDate.now()))) {
			msg="Field Date is invalid !";
		    GenererAlerte(msg);
    	}
    	else
    	{
    		
    	context= new InitialContext();
    	
    	Employee employee= new Employee();
    	employee.setName(interview.getJobRequest().getNameCandidate());
    	employee.setFamilyName(interview.getJobRequest().getLastNameCandidate());
    	employee.setDateOfBirth(interview.getJobRequest().getDateOfBirthCandidate());
    	List<Department>departments= new ArrayList<Department>();
    	departments.add(interview.getJobRequest().getJobOffer().getDepartment());
    	employee.setDepartments(departments);
    	employee.setEmail(interview.getJobRequest().getEmailOfCandidate());
    	String login= generate(8);
    	System.out.println("login="+login);
    	employee.setLogin(login);
    	String pswd=generate(5);
    	employee.setPassword(pswd);
    	System.out.println("password="+pswd);
    	employee.setPhoneNumber(interview.getJobRequest().getPhoneOfCandidate());
    	employee.setSalary(Float.valueOf(JobOfferSalary.getText()));
    	employee.setTypeContract(interview.getJobRequest().getJobOffer().getTypeContrat());
    	employee.setDateOfHiring(java.sql.Date.valueOf(java.time.LocalDate.now()));
    	employee.setRoleEmployee(RoleEmployee.valueOf(JobOfferContract.getValue()));
		
		
	//	System.out.println(employee.getSkills().size());
    	List<Skill> skills= interview.getJobRequest().getJobOffer().getSkills();
    	List<Skill> skillsF= new ArrayList<Skill>();
    	skillsF.clear();
    	Context context = new InitialContext();
		SkillServiceRemote skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");
		skills=skillServiceRemote.findAll();
		for (Skill skill : skills) {
			List<JobOffer> skills2= skill.getLiJobOffers();
			for (JobOffer jobOffer : skills2) {
			if(jobOffer.getId()==interview.getJobRequest().getJobOffer().getId())
				{
				System.out.println(jobOffer.getDescription());
				System.out.println(skill.getName());
				skillsF.add(skill);
				}
				
				
				
			}
		}
		employee.setSkills(skillsF);
		System.out.println(employee.getSkills().toString());
		
    	
    	EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		employeeServiceRemote.save(employee);
		SendMail sm =new SendMail();
		  sm.sendmail("m2adashbored@gmail.com",employee.getEmail(), "hrbis2018", "your login to M2A platfrom \n Login:"+employee.getLogin()+" \n Password :"+employee.getPassword());

		Contract contract= new Contract();
		List<Employee> employees=employeeServiceRemote.findEmployeeByName(employee.getName(), employee.getFamilyName());
		Employee employee2= new Employee();
		for (Employee employee3 : employees) {
			employee2=employee3;
		}
		contract.setEmployee(employee2);
		contract.setDateOfEnding(Date.valueOf(dateOfEnding.getValue()));
		contract.setTypeContract(employee2.getTypeContract());
		contract.setDateOfHiring(employee2.getDateOfHiring());;
		ContratServicesRemote contratServicesRemote= (ContratServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ContratServices!tn.esprit.b2.esprit1718b2hrboardbis.services.ContratServicesRemote");
		contratServicesRemote.save(contract);
		  Stage stage = (Stage) AddEmployee.getScene().getWindow();
	         // do what you have to do
	         stage.close();
		
		Alert alert = new Alert(AlertType.WARNING);
  	    alert.setTitle("information");
        alert.setHeaderText("Success Operation");
        
        
        ButtonType buttonTypeCancel = new ButtonType("Ok", ButtonData.CANCEL_CLOSE);
        
        alert.getButtonTypes().setAll(buttonTypeCancel);
        
        Optional<ButtonType> result = alert.showAndWait();  }
    }

    @FXML
    private void back(ActionEvent event) {
    	  Stage stage = (Stage) AddEmployee.getScene().getWindow();
	         // do what you have to do
	         stage.close();
    }
    public Interview getInterview() {
		return interview;
	}

	public void setInterview(Interview interview) {
		this.interview = interview;
	}
   
	public  void recupererInterview(Interview interview)
    {
    	setInterview(interview);
        JobOfferSalary.setText(String.valueOf(interview.getJobRequest().getJobOffer().getSalary()));

    }
	public String generate(int length)
	{
		    String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
		    String pass = "";
		    for(int x=0;x<length;x++)
		    {
		       int i = (int)Math.floor(Math.random() * 62); 
		       pass += chars.charAt(i);
		    }
		    System.out.println(pass);
		    return pass;
	}


    
}
    

