/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Contract;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ContratServicesRemote;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class InformationEmployeesController implements Initializable {

    @FXML
    private Label NameEmployee;
    @FXML
    private Label LastNameEmployee;
    @FXML
    private Label RoleEmployee;
    @FXML
    private Label TypeContract;
    @FXML
    private Label DateOfHiring;
    @FXML
    private Label DateOfEnding;
    @FXML
    private ImageView ImageJnab;
    private Employee Employee;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/socity.jpg");
    	ImageJnab.setImage(img);
    }

	public Employee getEmployee() {
		return Employee;
	}

	public void setEmployee(Employee employee) {
		Employee = employee;
	}    
    public void setEmployees(Employee employee) throws NamingException
    {  Context context= new InitialContext();
    	setEmployee(employee);
    	NameEmployee.setText(employee.getName());
    	LastNameEmployee.setText(employee.getFamilyName());
    	RoleEmployee.setText(String.valueOf(employee.getRoleEmployee()));
    	TypeContract.setText(String.valueOf(employee.getTypeContract()));
    	DateOfHiring.setText(String.valueOf(employee.getDateOfHiring()));
    	List<Contract> contracts= new ArrayList<Contract>();
    	ContratServicesRemote contratServicesRemote= (ContratServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ContratServices!tn.esprit.b2.esprit1718b2hrboardbis.services.ContratServicesRemote");
		contracts=contratServicesRemote.findAll();
		Contract contract1= new Contract();
		for (Contract contract : contracts) {
			if(contract.getEmployee().getId()==employee.getId())
			{
				contract1=contract;
			}
			DateOfEnding.setText(String.valueOf(contract1.getDateOfEnding()));
		}
    }
}
