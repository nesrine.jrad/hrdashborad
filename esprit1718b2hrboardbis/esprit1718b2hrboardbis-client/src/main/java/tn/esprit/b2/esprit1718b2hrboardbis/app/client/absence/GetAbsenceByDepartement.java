package tn.esprit.b2.esprit1718b2hrboardbis.app.client.absence;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;

public class GetAbsenceByDepartement {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
        AbsenceServiceRemote absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");

		DepartmentServicesRemote departmentServicesRemote = (DepartmentServicesRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
//        List<Department> departments = departmentServicesRemote.getEmployeeByManagerDepartementId(1);
//        for (Department department : departments) {
//			System.out.println(department.getEmployees().toString());
//		}
		
		List<Absence> absenceList= new ArrayList<>();
		List<Department> departments = departmentServicesRemote.findAll();
		for (Department department : departments) {
			
			   for (Employee employee : department.getEmployees()) {
				   
				   if(employee.getId()==2)
					   System.out.println(department.getName());
				   absenceList.addAll(absenceServiceRemote.getAbsenceByUserId(employee.getId()));
			}
			   
		}
		for (Absence absence : absenceList) {
			System.out.println(absence.toString());
		}
		
				} 
	
	
	

}
