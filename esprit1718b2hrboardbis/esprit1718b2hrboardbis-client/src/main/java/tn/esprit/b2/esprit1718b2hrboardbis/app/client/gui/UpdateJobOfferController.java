/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TypeContract;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class UpdateJobOfferController implements Initializable {

    @FXML
    private JFXTextField JobOfferPost;
    @FXML
    private JFXTextField JobOfferSalary;
    @FXML
    private JFXComboBox<String> JobOfferDepartment;
    @FXML
    private JFXComboBox<String> JobOfferContract;
    @FXML
    private JFXComboBox<String> JobOfferHours;
    @FXML
    private JFXTextArea JobOfferDescription;
    @FXML
    private JFXButton AddJobOffer;
    @FXML
    private ImageView AjoutImg;
    @FXML
    private ImageView ImageJnab;
    @FXML
    private JFXButton BackInterfaceRecrutmentHr;
    @FXML
    private ImageView BAckImg;
    @FXML
    private JFXTextField idJobOffer;
    private JobOffer jobOffer;
    ObservableList<String> listeOfContracts=FXCollections.observableArrayList();
    ObservableList<String> listeOfJobHours=FXCollections.observableArrayList("Full time","part time","patiam timr");
    private Context context;
    ObservableList<String> ListOfDepartments=FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    public ObservableList<String> returnNameDepartment()
    {
        Context context;
  		try {
  			context = new InitialContext();
  			//Integer idDepartment=0;
  			DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
  	 		List<Department> DepartmentList= new ArrayList<Department>();
  	 		DepartmentList=departmentServicesRemote.findAll();
  	 		//ObservableList<Integer> IdOfDepartments=FXCollections.observableArrayList();
  	 		List<Integer> idDepartment= new ArrayList<Integer>();
  	 		 for (Department department : DepartmentList) {
  	 			System.out.println(department.getName());
  	 			ListOfDepartments.add(department.getName());
  	 			idDepartment.add(department.getId()); 
  	 		}
  	  		idDepartment.forEach(System.out::println);

  		} catch (NamingException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		return ListOfDepartments;
    }
    
    public List<Integer> returnListIdDepartment()
    {
        Context context;
	 		List<Integer> idDepartment= new ArrayList<Integer>();

      		try {
      			context = new InitialContext();
      			//Integer idDepartment=0;
      			DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
      	 		List<Department> DepartmentList= new ArrayList<Department>();
      	 		DepartmentList=departmentServicesRemote.findAll();
      	 		//ObservableList<Integer> IdOfDepartments=FXCollections.observableArrayList();
      	 		 for (Department department : DepartmentList) {
      	 			System.out.println(department.getName());
      	 			ListOfDepartments.add(department.getName());
      	 			idDepartment.add(department.getId()); 
      	      		return idDepartment;

      	 		}
      	  		idDepartment.forEach(System.out::println);

      		} catch (NamingException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
      		return(idDepartment);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	JobOfferHours.setItems(listeOfJobHours);
        idJobOffer.setVisible(false);
	 		 ObservableList<String> ListOfDepartments=returnNameDepartment();
	 		Image img = new Image("assets/socity.jpg");
	    	ImageJnab.setImage(img);
		JobOfferDepartment.setItems(ListOfDepartments);
       
		TypeContract[] contractList;
		contractList=TypeContract.values();
		for (TypeContract typeContract : contractList) {
			System.out.println(typeContract);
			String contact=String.valueOf(typeContract);
			listeOfContracts.add(contact);
			System.out.println(contact);
			
		}
		listeOfContracts.forEach(System.out::println);

		JobOfferContract.setItems(listeOfContracts);	 
    }    
    private static void GenererAlerte(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(msg);
        alert.showAndWait(); 
    }
    @FXML
    private void SaveJobOfferJobOffer(ActionEvent event) throws NamingException {
    	String msg="";
    	if (JobOfferDepartment.getValue()==null) {
    		msg="Field Department is required !";
    	    GenererAlerte(msg);
    	}

    	else if (JobOfferSalary.getText().equals(null)) {
    		msg="You have to affect Salary to this Employee !";
    	    GenererAlerte(msg);
    	}
    	else if (JobOfferContract.getValue()==null) {
			msg="Field Contract is invalid !";
		    GenererAlerte(msg);
    	}
    	else if (JobOfferDescription.getText()==null) {
			msg="Field Description is invalid !";
		    GenererAlerte(msg);
    	}
    	else if (JobOfferHours.getValue()==null) {
			msg="Field Hours is invalid !";
		    GenererAlerte(msg);
    	}
    	else if (JobOfferPost.getText()==null) {
			msg="Field Post is invalid !";
		    GenererAlerte(msg);
    	}
    	else
    	{
    	JobOffer jobOffer= new JobOffer();
		jobOffer.setId(Integer.valueOf(idJobOffer.getText()));
		jobOffer.setDescription(JobOfferDescription.getText());
		jobOffer.setName(JobOfferPost.getText());
		jobOffer.setSalary(Float.valueOf(JobOfferSalary.getText()));
		System.out.println(jobOffer.getSalary());
		String contract= JobOfferContract.getValue();
		TypeContract type= TypeContract.valueOf(contract);
		jobOffer.setTypeContrat(type);
		 System.out.println(jobOffer.getTypeContrat());
		jobOffer.setTimeOfJob(JobOfferHours.getValue());	
		Integer rowDepartmentSelected=0;
		jobOffer.setDateOfPublishing(java.sql.Date.valueOf(java.time.LocalDate.now()));		   
		List<String> ListDepartment1= new ArrayList<String>();
		String department = JobOfferDepartment.getValue();
		ListDepartment1=JobOfferDepartment.getItems();
		int i=0;
		for (String string : ListDepartment1) {
		
			if(string!=department)
			{
				i++;
			}
			System.out.println(i);
		}
		List<Integer> idDepartment= new ArrayList<Integer>();
		idDepartment=returnListIdDepartment();
        Context context = null;
	 		
        Department department2=new Department();
      		try {
      			System.out.println(i);
      			context = new InitialContext();
      			//Integer idDepartment=0;
      			
      			DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
      	 		department2=departmentServicesRemote.find(i);
      	 		jobOffer.setDepartment(department2);
      	 			} catch (NamingException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
      		//department2.toString();
      		 System.out.println("ajout effectué");
      		
    		JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");
    		DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
    		 jobofferservicesremote.update(jobOffer);
    		 Stage stage = (Stage) AddJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
		
		Alert alert = new Alert(AlertType.WARNING);
  	  alert.setTitle("information");
        alert.setHeaderText("Success Operation");
        
        
        ButtonType buttonTypeCancel = new ButtonType("Ok", ButtonData.CANCEL_CLOSE);
        
        alert.getButtonTypes().setAll(buttonTypeCancel);
        
        Optional<ButtonType> result = alert.showAndWait();  
    }
    }
    @FXML
    private void BackRecrutmentsInterface(ActionEvent event) {
    	 Stage stage = (Stage) AddJobOffer.getScene().getWindow();
         // do what you have to do
         stage.close();
    }

    public JobOffer getJobOffer() {
		return jobOffer;
	}

	public void setJobOffer(JobOffer jobOffer) {
		this.jobOffer = jobOffer;
	}
    public void setTexts (JobOffer jobOffer) {
    	JobOfferDescription.setText(jobOffer.getDescription());
    	JobOfferPost.setText(jobOffer.getName());
        JobOfferSalary.setText(String.valueOf(jobOffer.getSalary()));
        JobOfferHours.setValue(String.valueOf(jobOffer.getTypeContrat()));
        JobOfferHours.setValue(jobOffer.getTimeOfJob());
        JobOfferDepartment.setPromptText(jobOffer.getDepartment().getName());
        JobOfferContract.setPromptText(String.valueOf(jobOffer.getTypeContrat()));
    	idJobOffer.setText(String.valueOf(jobOffer.getId()));
		}
	
    
}
