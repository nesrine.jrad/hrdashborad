package tn.esprit.b2.esprit1718b2hrboardbis.app.test.trainer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote;

public class TestUpdateTrainer {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");
		Trainer trainer=trainerServiceRemote.find(3);
		trainer.setName("KIKI");
		trainerServiceRemote.update(trainer);
	}
}
