package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.skill;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote;;

public class TestReadAllSkills {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		SkillServiceRemote skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");

		List<Skill> skills= new ArrayList<Skill>();
		skills=skillServiceRemote.findAll();
		
		skills.forEach(System.out::println);

	}
}
