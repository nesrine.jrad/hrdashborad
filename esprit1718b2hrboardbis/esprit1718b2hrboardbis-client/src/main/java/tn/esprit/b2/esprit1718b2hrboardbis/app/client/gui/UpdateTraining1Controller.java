package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class UpdateTraining1Controller implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXTextField title;
    @FXML
    private DatePicker dateOfSession;
    @FXML
    private JFXTextArea description;
    @FXML
    private ImageView logo_delete;
    @FXML
    private  ImageView logo_add;
    
    private int id;
    private String title2;
    private String description2;
    private Date date2;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
          img = new Image("assets/newspape.png");
          logo_delete.setImage(new Image("assets/logo_delete.png"));
          logo_add.setImage(new Image("assets/logo_add.png"));
          
    }

    public void setText(
          String title5,
          String description5,
          LocalDate dateOfSession5,
          int id){
    		title.setText(title5);
    		description.setText(description5);
    		dateOfSession.setValue(dateOfSession5);
    		this.id=id;
    }
    
  @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }        


    @FXML
    private void cancel(ActionEvent event) {
    	title.clear();
        description.clear();
        dateOfSession.setValue(LocalDate.now());
    }
    
    @FXML
    private void update1Next(ActionEvent event) throws NamingException, IOException {
    	java.sql.Date gettedDatePickerDate = java.sql.Date.valueOf(dateOfSession.getValue());
    	java.sql.Date datenow = java.sql.Date.valueOf(LocalDate.now());
    	Context context = new InitialContext();
		
		TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");
		
		
		
		Training training=trainingServiceRemote.find(id);
		String msg="";
		if (title.getText().equals("")) {
			msg="Field Title is required !";
		    GenererAlerte(msg);
		}
		else if (description.getText().equals("")) {
			msg="Field Description Name is required !";
		    GenererAlerte(msg);
		}
		else if (gettedDatePickerDate==null || gettedDatePickerDate.before(datenow)) {
			msg="Field Date is invalid !";
		    GenererAlerte(msg);
		}
		else{
		training.setDate(gettedDatePickerDate);
		training.setTitle(title.getText());
		training.setDescription(description.getText());
		trainingServiceRemote.update(training);
		
		Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
	  	  
		FXMLLoader loader=new FXMLLoader(getClass().getResource("TrainingManagement.fxml"));
	    Parent root=(Parent) loader.load();
		Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();}
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}
	
	private static void GenererAlerte(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(msg);
        alert.showAndWait(); 
    }
    
}
