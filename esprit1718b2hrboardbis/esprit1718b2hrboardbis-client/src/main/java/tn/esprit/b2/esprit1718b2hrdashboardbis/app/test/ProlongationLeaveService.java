package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote;

public class ProlongationLeaveService {

	public static void main(String[] args) throws NamingException, ParseException {
		Context context = new InitialContext();
		LeaveServiceRemote leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		
		Leave leave= leaveServiceRemote.find(1);
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		c.setTime(sdf.parse(leave.getToDate().toString()));
		c.add(Calendar.DAY_OF_MONTH, 7);  
		Date dateOfBirth= Date.valueOf("1994-08-08");
		String newDate = sdf.format(c.getTime());
		leave.setToDate(Date.valueOf(newDate));;
		leaveServiceRemote.update(leave);
	}

}
