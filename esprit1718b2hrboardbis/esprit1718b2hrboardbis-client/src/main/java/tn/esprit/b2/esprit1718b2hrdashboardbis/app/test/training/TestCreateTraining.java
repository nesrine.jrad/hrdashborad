package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.training;

import java.sql.Date;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;

public class TestCreateTraining {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		
		TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");
		
		
		String title="Formation Rasberry PI 3";
		String description="Formation certifiée de +/- 8 séances";
		Date date=Date.valueOf("2018-02-02");
		
		Training training=new Training(title, description, date);
		trainingServiceRemote.save(training);
	}
}
