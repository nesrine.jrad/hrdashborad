package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.recrutement;

import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServices;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote;

public class TestDisplayAllJobOffer {
	public static void main (String[] argh) throws NamingException {
		Context context = new InitialContext();
		JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");
		JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
		List<JobRequest> jobRequests= new ArrayList<JobRequest>();
		List<JobOffer> jobOffers=new ArrayList<JobOffer>();
		jobOffers=jobofferservicesremote.findAll();
		jobOffers.forEach(System.out::println);

		
	}

}
