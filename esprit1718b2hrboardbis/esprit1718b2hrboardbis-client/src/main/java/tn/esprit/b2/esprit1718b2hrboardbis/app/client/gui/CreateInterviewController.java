/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class CreateInterviewController implements Initializable {


    @FXML
    private DatePicker InterviewDate;
    @FXML
    private JFXComboBox<String> InterviewTime;
    @FXML
    private JFXButton AddJobOffer;
    @FXML
    private ImageView AjoutImg;
    @FXML
    private ImageView ImageJnab;
    @FXML
    private JFXButton back;
    
    @FXML
    private ImageView backImg;
    private JobRequest jobrequest;
    ObservableList<String> listeOfHours=FXCollections.observableArrayList("9h00","10h00","11h00 ","12h00","14h0","15h00");


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/socity.jpg");
    	ImageJnab.setImage(img);
        InterviewTime.setItems(listeOfHours);
        InterviewDate.setValue(LocalDate.now());

    }  
    private static void GenererAlerte(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(msg);
        alert.showAndWait(); 
    }

    @FXML
    private void SaveJobOfferJobOffer(ActionEvent event) throws NamingException {
    	java.sql.Date gettedDatePickerDate = java.sql.Date.valueOf(InterviewDate.getValue());
    	java.sql.Date datenow = java.sql.Date.valueOf(LocalDate.now());
    	String msg="";
    	if (InterviewTime.getValue()==null) {
    		msg="Field Date is required !";
    	    GenererAlerte(msg);
    	}

    	else if ( gettedDatePickerDate.equals(null) || gettedDatePickerDate.before(java.sql.Date.valueOf(java.time.LocalDate.now()))) {
    		msg="Field Time is required  !";
    	    GenererAlerte(msg);
    	}
    	else 
    	{
    	System.out.println(jobrequest.getEmailOfCandidate());
    	Interview interview= new Interview();
    	Department department= jobrequest.getJobOffer().getDepartment();
    	
		Context context= new InitialContext();
		DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
		List<Department> DepartmentList= new ArrayList<>();
		department=departmentServicesRemote.find(department.getId());
		List<Employee> employees = new ArrayList<Employee>();
		Employee employee2= new Employee();
		employees=department.getEmployees();
		for (Employee employee : employees) {
			if(employee.getRoleEmployee()==RoleEmployee.MANAGER)
			{System.out.println(employee.getId());
			employee2=employee;}
		}
			
    	System.out.println(employee2.getId());
    	interview.setEmployee(employee2);
    	interview.setJobRequest(jobrequest);
    	interview.setDateOfInterview(Date.valueOf(InterviewDate.getValue()));
    	interview.setTimeOfInterview(String.valueOf(InterviewTime.getValue()));
    	System.out.println(interview.toString());
    	context= new InitialContext();
		InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
		jobrequest.setState("treated");
		JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
		jobRequestServicesRemote.update(jobrequest);
		interviewServicesRemote.save(interview);
		  Stage stage = (Stage) back.getScene().getWindow();
	         // do what you have to do
	         stage.close();

		Alert alert = new Alert(AlertType.WARNING);
  	  alert.setTitle("information");
        alert.setHeaderText("Success Operation");
        
        
        ButtonType buttonTypeCancel = new ButtonType("Ok", ButtonData.CANCEL_CLOSE);
        
        alert.getButtonTypes().setAll(buttonTypeCancel);
        
        Optional<ButtonType> result = alert.showAndWait();  
    }
    }
    @FXML
    private void back(ActionEvent event) {
    }
	public JobRequest getJobrequest() {
		return jobrequest;
	}

	public void setJobrequest(JobRequest jobrequest) {
		this.jobrequest = jobrequest;
	}
    public void recupererJobRequest(JobRequest jobRequest)
    {
    	  setJobrequest(jobRequest);
    }
}
