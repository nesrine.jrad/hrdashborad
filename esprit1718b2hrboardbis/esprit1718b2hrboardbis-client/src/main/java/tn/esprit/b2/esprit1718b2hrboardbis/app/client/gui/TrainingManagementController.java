/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class TrainingManagementController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private TableView<Training> table;
    @FXML
    private TableColumn<Training, String> title;
    @FXML
    private TableColumn<Training, String> description;
    @FXML
    private TableColumn<Training, String> date;
    @FXML
    private ImageView logo_delete;
    @FXML
    private  ImageView logo_add;
    @FXML
    private ImageView update;
    
    private ObservableList<Training> datax;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
          img = new Image("assets/newspape.png");
          logo_delete.setImage(new Image("assets/logo_delete.png"));
          logo_add.setImage(new Image("assets/logo_add.png"));
         update.setImage(new Image("assets/update.png"));
         
///////////////////////////////////////////
try {
Context context = new InitialContext();
TrainingServiceRemote trainingServiceRemote;
trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");


List<Training> trainings= new ArrayList<Training>();
trainings=trainingServiceRemote.findAll();

///////////////////////////////////////////
///////////////////////////////////////////
table.setEditable(false);
datax = FXCollections.observableArrayList();
for ( int i = 0; i<table.getItems().size(); i++) 
{
table.getItems().clear();
}
title.setCellValueFactory(new PropertyValueFactory<>("title"));
description.setCellValueFactory(new PropertyValueFactory<>("description"));
date.setCellValueFactory(new PropertyValueFactory<>("date"));

for(Training t:trainings){
t.setTitle(t.getTitle());
t.setDescription(t.getDescription());
 t.setDate(t.getDate());
datax.add(t);
t=null;
}
table.setItems(datax); 
} catch (NamingException e) {
e.printStackTrace();
}     
       
    }  
       @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }    

    @FXML
    private void add(ActionEvent event) throws IOException {
    	Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("addTraining.fxml"));
        Parent root = (Parent) loader.load();
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }

    @FXML
    private void delete(ActionEvent event) throws NamingException {
    		Context context = new InitialContext();
		TrainingServiceRemote trainingServiceRemote= (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");
		SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");
    	seanceServiceRemote.deleteWhenTrainingDeleted(trainingServiceRemote.find(getIdTrainingFromTable()));
		trainingServiceRemote.delete(trainingServiceRemote.find(getIdTrainingFromTable()));
		refreshTable();
    }

    @FXML
    private void update(ActionEvent event) throws IOException, NamingException {
    	Context context = new InitialContext();
    	TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");
		
		
		Training training=trainingServiceRemote.find(getIdTrainingFromTable());
		
 		
 		Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
 	  	  stage.close();
 		
 	  	FXMLLoader loader = new FXMLLoader(getClass().getResource("updateTraining1.fxml"));
        Parent root = (Parent) loader.load();
        UpdateTraining1Controller ctrl=loader.<UpdateTraining1Controller>getController();
 		ctrl.setId(getIdTrainingFromTable());
 		LocalDate dateL = training.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
 		ctrl.setText(training.getTitle(), training.getDescription(), dateL,getIdTrainingFromTable());
 		Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }
    
    private int getIdTrainingFromTable() {
        table.setOnMousePressed(new  EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                int id=table.getSelectionModel().getSelectedItem().getId();
            }
            });
      return table.getSelectionModel().getSelectedItem().getId();
    }
    
    public void refreshTable() throws NamingException{
    	Context context = new InitialContext();
    	TrainingServiceRemote trainingServiceRemote;
    	trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
    	+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");

    	List<Training> trainings= new ArrayList<Training>();
    	trainings=trainingServiceRemote.findAll();

    	table.setEditable(false);
    	datax = FXCollections.observableArrayList();
    	for ( int i = 0; i<table.getItems().size(); i++) 
    	{
    	table.getItems().clear();
    	}
    	title.setCellValueFactory(new PropertyValueFactory<>("title"));
    	description.setCellValueFactory(new PropertyValueFactory<>("description"));
    	date.setCellValueFactory(new PropertyValueFactory<>("date"));
    	for(Training t:trainings){
    	t.setTitle(t.getTitle());
    	t.setDescription(t.getDescription());
    	t.setDate(t.getDate());
    	datax.add(t);
    	t=null;
    	}
    	table.setItems(datax); 
	}
}
