/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class ProfileController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
  

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
          img = new Image("assets/newspape.png");
        
       
    } 
      @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }    
    
    
}
