package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.skill;

import java.util.ArrayList;

import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote;;

public class TestReadSkill {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");

		SkillServiceRemote skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");

		Skill searchedSkill;
		 List<Skill> skills= new ArrayList<Skill>();
		 skills=skillServiceRemote.findAll();
		 List<Skill> skillsFinal= new ArrayList<Skill>();
		 for (Skill skill : skills) {
			 List<JobOffer> jobOffers= new ArrayList<JobOffer>();
			 jobOffers.addAll(skill.getLiJobOffers());
			 for (JobOffer jobOffer : jobOffers) {
				if(jobOffer.getId()==17)
				{
					skillsFinal.add(skill);
				}
			}
		}
		 skillsFinal.forEach(System.out::println);
		/*searchedSkill=skillServiceRemote.find(2);
		List<JobOffer> jobOffers= new ArrayList<JobOffer>();
		jobOffers.addAll(searchedSkill.getLiJobOffers());
		for (JobOffer jobOffer : jobOffers) {
			System.out.println(jobOffer.toString());
		}
		/*JobOffer jobOffer=jobofferservicesremote.find(17);
    List<Skill> skills= new ArrayList<Skill>();
    skills=skillServiceRemote.findJobOfferSkill(jobOffer);
    System.out.println(skills.size());*/
	}
}
