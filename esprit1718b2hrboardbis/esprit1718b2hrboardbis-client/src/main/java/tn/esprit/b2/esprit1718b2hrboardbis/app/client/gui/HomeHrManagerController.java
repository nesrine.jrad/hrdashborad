/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class HomeHrManagerController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView imgNews;
    @FXML
    private ImageView imgVacationRest;
    @FXML
    private ImageView imgProjectNumber;
    @FXML
    private ImageView imgTrainingSoon;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    private Employee employee;
    static Employee employeeConnected;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
         
         
       
    }   @FXML 
    public void projectInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("recrutmentFinal.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }
     @FXML 
    public void absencesInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("absencesHr.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }
    
     @FXML 
    public void VacancesInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("VacancesHr.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }
    
    
     @FXML 
    public void formationInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("formationHr.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }
    @FXML 
    public void profileInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("ProjetHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }
    
      @FXML 
    public void salaireInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("profile.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }
          @FXML 
    public void projectHrInterface() throws IOException{ 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("ProjetHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }
		public Employee getEmployee() {
			return employee;
		}
		public void setEmployee(Employee employee) {
			this.employee = employee;
		}
          public void returnId(Employee employee)
          {
        	  setEmployee(employee);
        	  employeeConnected=employee;
        	  System.out.println(employee.getName());
          }
          
          @FXML 
          public void logout() throws IOException{ 
        	  this.employeeConnected=null;
        	  Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
        	  stage.close();
           FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
               Parent root = (Parent) loader.load();
               
               Scene newScene = new Scene(root);
               Stage newStage = new Stage();
               newStage.setScene(newScene);
               newStage.show();
          
          }
}
