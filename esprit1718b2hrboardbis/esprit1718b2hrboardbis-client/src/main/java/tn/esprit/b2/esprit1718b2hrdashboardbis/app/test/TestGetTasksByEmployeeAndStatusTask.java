package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;

public class TestGetTasksByEmployeeAndStatusTask {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		List<Task> tasks= projectServiceRemote.getListTasksOfProjectByStatus(1, StatusTask.ToDO);
		for(Task task : tasks){
			System.out.println(task.getTaskPk().getName());
		}

	}


}
