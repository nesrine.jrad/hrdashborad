/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TypeContract;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AssignEmployeesToProjectRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class AssignSkillsToJobOffer implements Initializable {

    @FXML
    private TableView<Skill> SkillListes;
    @FXML
    private TableColumn<Skill, Integer> idColumnEmployeeTab1;
    @FXML
    private TableColumn<Skill, String> firstNameColumnEmployeeTab1;
    @FXML
    private TableColumn<Skill, String> lastNameColumnEmployeeTab1;
    @FXML
    private TableView<Skill> SkillsJobOffer;
    @FXML
    private TableColumn<Skill, Integer> idColumnEmployeeTab2;
    @FXML
    private TableColumn<Skill, String> firstNameColumnEmployeeTab2;
    @FXML
    private TableColumn<Skill, String> lastNameColumnEmployeeTab2;
    @FXML
    private ImageView removeImg;
    @FXML
    private Button addSkillsBtn;
    @FXML
    private Button removeBtn;
    @FXML
    private Button confirmButton;
    
    private Skill skillToAdd;
    private Skill skillToRemove;
    @FXML
    private JFXTextField post;
    @FXML
    private JFXTextField department;
    @FXML
    private JFXTextField typeContract;
    @FXML
    private JFXTextField hours;
    @FXML
    private JFXTextField salary;
    @FXML
    private JFXTextField description;
    private List<Skill> skillsToAdd=new ArrayList<Skill>();
    private List<Skill> skillsToRemove=new ArrayList<Skill>();
    private ObservableList<Skill> data;
    private ObservableList<Skill> data1;
    private SkillServiceRemote skillServiceRemote;
    private JobOfferServicesRemote jobOfferServiceRemote;
    private Context context;
    private JobOffer jobOffer;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	try {
			context= new InitialContext();
 skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");
		
			 jobOfferServiceRemote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");

		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
 SkillListes.setItems(null);
 SkillsJobOffer.setItems(null);
 
 skillsToAdd=skillServiceRemote.findAll();
 idColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("id"));
 firstNameColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("name"));
 lastNameColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("description"));
 idColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("id"));
 firstNameColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("name"));
 lastNameColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("description"));
	data = FXCollections.observableList(skillsToAdd);
	SkillListes.setItems(data);

	

        
    }    

    @FXML
    private void getSelectedIdDisplayedEmployee(MouseEvent event) {
    }

    @FXML
    private void getSelectedIdEmployeeToAssign(MouseEvent event) {
    }

    @FXML
    private void removeEmployeeToAssign(MouseEvent event) {
    }

    @FXML
    private void addSkillsForJobOffer(ActionEvent event) {
    	skillToAdd=SkillListes.getSelectionModel().getSelectedItem();
    	
    	         skillsToRemove.add(skillToAdd);
    	    	skillsToAdd.remove(skillToAdd);
    	    	data1= FXCollections.observableList(skillsToAdd);
    	    	data= FXCollections.observableList(skillsToRemove);
    	    	SkillListes.setItems(data1);
    	    	
    	    	SkillsJobOffer.setItems(data);
    	    	skillToAdd=null;
    	    	
    	    	
    }

    @FXML
    private void removeSkill(ActionEvent event) {
    skillToRemove=SkillsJobOffer.getSelectionModel().getSelectedItem();
       skillsToAdd.add(skillToRemove);
      	skillsToRemove.forEach(System.out::println);
    	skillsToRemove.forEach(System.out::println);
   	System.out.println(skillsToRemove.indexOf(skillToRemove));
   	skillsToRemove.remove(skillsToRemove.indexOf(skillToRemove));
   	skillsToRemove.forEach(System.out::println);
   	data1= FXCollections.observableList(skillsToAdd);
   	data= FXCollections.observableList(skillsToRemove);
   	
   	SkillListes.setItems(data1);
   	SkillsJobOffer.setItems(data);
   	skillToRemove=null;
    }

    @FXML
    private void confirmAssignSkillForJobOfer(ActionEvent event) throws NamingException {
    	JobOffer jobOffer= new JobOffer();
    	jobOffer.setDateOfPublishing(java.sql.Date.valueOf(java.time.LocalDate.now()));
    	jobOffer.setDescription(description.getText());
    	jobOffer.setName(post.getText());
    	jobOffer.setSalary(Float.valueOf(salary.getText()));
    	jobOffer.setTimeOfJob(hours.getText());
    	jobOffer.setTypeContrat(TypeContract.valueOf(typeContract.getText()));
    	Context context= new InitialContext();
		DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
		Department department1= new Department();
		department1=departmentServicesRemote.find(Integer.valueOf(department.getText()));
		jobOffer.setDepartment(department1);
    	data=SkillsJobOffer.getItems();
    	skillsToAdd.addAll(data);
    	System.out.println("list"+skillsToAdd.size());
    	System.out.println("**avant");
    	jobOffer.setSkills(skillsToAdd);
    	System.out.println(jobOffer.getSkills().size());
    	System.out.println("job offer skill"+jobOffer.getSkills().toString());
    	context= new InitialContext();
		 jobOfferServiceRemote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");

    	jobOfferServiceRemote.save(jobOffer);
    	 Stage stage = (Stage)  addSkillsBtn.getScene().getWindow();
         // do what you have to do
         stage.close();
    }

	public JobOffer getJobOffer() {
		return jobOffer;
	}

	public void setJobOffer(JobOffer jobOffer) {
		jobOffer = jobOffer;
	}
	public void init()
	{
		try {
			context= new InitialContext();
 skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");
		
			 jobOfferServiceRemote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");

		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
 SkillListes.setItems(null);
 SkillsJobOffer.setItems(null);
 
 skillsToAdd=skillServiceRemote.findAll();
 idColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("id"));
 firstNameColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("name"));
 lastNameColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("description"));
 idColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("id"));
 firstNameColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("name"));
 lastNameColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("description"));
	data = FXCollections.observableList(skillsToAdd);
	SkillListes.setItems(data);

	}
	public void recupjoboffre(String post,String department,String contract,String hour, String Salary, String description)
	{ this.post.setVisible(false);
	this.post.setVisible(false);
	this.department.setVisible(false);
	this.typeContract.setVisible(false);
	this.salary.setVisible(false);
	this.description.setVisible(false);
		this.post.setText(post);
		this.department.setText(department);
		typeContract.setText(contract);
		hours.setText(hour);
		salary.setText(Salary);
		this.description.setText(description);
	}
    
}
