package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TypeContract;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

public class TestAddEmployee {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		
		String name = "Nesrine";
		String familyName = "Jrad";
		Date dateOfBirth = Date.valueOf("1994-08-08");
		Date dateOfHiring = Date.valueOf("2018-01-01");
		String email="nesrine.jrad@esprit.tn";
		String phoneNumber="0";
		String address="";
		float salary=1500f;
		RoleEmployee roleEmployee= RoleEmployee.EMPLOYEE;
		TypeContract typeContract = TypeContract.CDD;
		DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
		Department department=departmentServicesRemote.find(1);
		List<Department> departments= new ArrayList<Department>();
		departments.add(department);
		String login = "nesrine";
		String password = "123123";
		Employee employee = new Employee(name, familyName, dateOfBirth, dateOfHiring, email, phoneNumber, address, salary, roleEmployee,typeContract, login, password,departments);
		
		
		String name1 = "Amine";
		String familyName1 = "Naija";
		Date dateOfBirth1 = Date.valueOf("1994-08-01");
		Date dateOfHiring1 = Date.valueOf("2018-01-01");
		String email1="mohamedamine.naija@esprit.tn";
		String phoneNumber1="0";
		String address1="";
		float salary1=1500f;
		RoleEmployee roleEmployee1= RoleEmployee.EMPLOYEE;
		TypeContract typeContract1 = TypeContract.CDI;
		
		String login1 = "naija";
		String password1 = "123123";
		
	
		
		
		
		Employee employee1 = new Employee(name1, familyName1, dateOfBirth1, dateOfHiring1, email1, phoneNumber1, address1, salary1, roleEmployee1, typeContract1, login1, password1,departments);
		employeeServiceRemote.save(employee1);
		employeeServiceRemote.save(employee);
	}

}
