package tn.esprit.b2.esprit1718b2hrboardbis.app.client.absence;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

public class GetAbsencesByUserID {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		AbsenceServiceRemote absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");
		List<Absence> absences = absenceServiceRemote.getAbsenceByUserId(2);
		for (Absence absence : absences) {
			System.out.print(absence.getId());
		}
	}
}
