package tn.esprit.b2.esprit1718b2hrboardbis.app.client.absence;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Justification;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

public class AddAbsence {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
        AbsenceServiceRemote absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");
		String csvFile = "//home/kadhem/Desktop/Esprit2/PDEV/test.csv";
        String line = "";
        String cvsSplitBy = ",";
        
        List< Absence> absences = new ArrayList<Absence>();
        Employee emp;
        Absence absence;
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] absenceDetail = line.split(cvsSplitBy);
                emp= employeeServiceRemote.find(Integer.parseInt(absenceDetail[0]));
                
                 absence= new Absence(Date.valueOf("2018-08-08"),Date.valueOf("2018-08-08"),Justification.NOT_JUSTIFIED,emp);
                //System.out.println("Absence [matricule="+ absenceDetail[0]+",absfrom= " + absenceDetail[1] + " , absto=" + absenceDetail[2] + ",justified="+absenceDetail[3]+"]");
                absences.add(absence);
                absenceServiceRemote.save(absence);
            }
            System.out.println(absences.toString());
            //  absenceServiceRemote.insertAbsence(absences);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
