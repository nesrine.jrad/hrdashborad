/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;

import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;



import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class RecrutementsController implements Initializable {

    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXButton HomeManager;
    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView imgBtHome1;
    @FXML
    private ImageView imgBtHome2;
    @FXML
    private ImageView imgBtHome3;
    @FXML
    private ImageView imgBtHome4;
    @FXML
    private AnchorPane JobOfferManage;
    @FXML
    private JFXButton AddJobOffer;
    @FXML
    private JFXButton FindJobOffer;
    @FXML
    private ImageView AddJobOfferImg1;
    @FXML
    private JFXButton FindAllJobOffer;
    @FXML
    private ImageView FinfJobOfferImg1;
    @FXML
    private ImageView FindAllJobOfferImg1;
    @FXML
    private AnchorPane InterviewManage;
    @FXML
    private JFXButton AddInterview;
    @FXML
    private JFXButton FindJobOffer12;
    @FXML
    private ImageView AddInterviewImg;
    @FXML
    private JFXButton FindAllInterviews;
    @FXML
    private ImageView FinfInterviewImg;
    @FXML
    private AnchorPane JobRequestManage1;
    @FXML
    private JFXButton FindJobRequest;
    @FXML
    private JFXButton FindAllJobRequests;
    @FXML
    private ImageView FinfJobRequestImg;
    @FXML
    private ImageView FindAllJobRequestImg;
    @FXML
    private AnchorPane EmployeeManage;
    @FXML
    private JFXButton FindAllContracts;
    @FXML
    private JFXButton FindEmployee;
    @FXML
    private JFXButton UpgradeEmployee;
    @FXML
    private ImageView FindAlInterviewImg;
    @FXML
    private ImageView FindAllContractsImg;
    @FXML
    private ImageView FindEmployeeImg;
    @FXML
    private ImageView UpgradeEmployeeImg;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/save.png");
         AddJobOfferImg1.setImage(img);
         img = new Image("assets/FindAll.png");
         FindAllJobOfferImg1.setImage(img);
         img = new Image("assets/FindAll.png");
         FindAllJobRequestImg.setImage(img);
         img = new Image("assets/FindAll.png");
         FindAllJobOfferImg1.setImage(img);
         img = new Image("assets/FindAll.png");
         FindAlInterviewImg.setImage(img);
         img = new Image("assets/FindAll.png");
         FindAllContractsImg.setImage(img);
         img = new Image("assets/Find.png");
         FindEmployeeImg.setImage(img);
         img = new Image("assets/Find.png");
         FinfJobOfferImg1.setImage(img);
         img = new Image("assets/Find.png");
         FinfJobRequestImg.setImage(img);
         img = new Image("assets/FindAll.png");
         FindAllJobOfferImg1.setImage(img);
         img = new Image("assets/Find.png");
         FinfInterviewImg.setImage(img);
         img = new Image("assets/JobOffer.png");
         imgBtHome1.setImage(img);
         img = new Image("assets/jobRequest.png");
         imgBtHome2.setImage(img);
          img = new Image("assets/interview.png");
         imgBtHome3.setImage(img);
         img = new Image("assets/GoodManagement.png");
         imgBtHome4.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
         EmployeeManage.setVisible(false);
         InterviewManage.setVisible(false);
         JobOfferManage.setVisible(false);
         JobRequestManage1.setVisible(false);
    }    

    @FXML
    private void HomeManagerInterface(ActionEvent event) throws IOException {
        
          FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    }

    @FXML
    private void projectInterface(ActionEvent event) {
    }

    @FXML
    private void JobOfferManageInterface(MouseEvent event) {
    	  EmployeeManage.setVisible(false);
          InterviewManage.setVisible(false);
          JobOfferManage.setVisible(true);
          JobRequestManage1.setVisible(false);
    }

    @FXML
    private void JobRequestManageInterface(MouseEvent event) {
    	  EmployeeManage.setVisible(false);
          InterviewManage.setVisible(false);
          JobOfferManage.setVisible(false);
          JobRequestManage1.setVisible(true);
    }

    @FXML
    private void InterviewsManageInterface(MouseEvent event) {
    	  EmployeeManage.setVisible(false);
          InterviewManage.setVisible(true);
          JobOfferManage.setVisible(false);
          JobRequestManage1.setVisible(false);
    }

    @FXML
    private void EmployeesManagementInterface(MouseEvent event) {
    	  EmployeeManage.setVisible(true);
          InterviewManage.setVisible(false);
          JobOfferManage.setVisible(false);
          JobRequestManage1.setVisible(false);
    }

    @FXML
    private void AddJobOfferInterface(ActionEvent event) throws IOException {
  	   FXMLLoader loader = new FXMLLoader(getClass().getResource("AddJobOffer.fxml"));
       Parent root = (Parent) loader.load();
       
       Scene newScene = new Scene(root);
       Stage newStage = new Stage();
       newStage.setScene(newScene);
       newStage.show();
    }

    @FXML
    private void FindJobOfferInterface(ActionEvent event) {
    }

    @FXML
    private void FindAllJobOfferInterface(ActionEvent event) throws IOException {
  	   FXMLLoader loader = new FXMLLoader(getClass().getResource("FindAllJobOffers.fxml"));
       Parent root = (Parent) loader.load();
       
       Scene newScene = new Scene(root);
       Stage newStage = new Stage();
       newStage.setScene(newScene);
       newStage.show();
    }

    @FXML
    private void FindInterviewInterface(ActionEvent event) {
    }

    @FXML
    private void FindAllInterviewsInterface(ActionEvent event) throws IOException {
   	   FXMLLoader loader = new FXMLLoader(getClass().getResource("FindAllInterview.fxml"));
       Parent root = (Parent) loader.load();
       
       Scene newScene = new Scene(root);
       Stage newStage = new Stage();
       newStage.setScene(newScene);
       newStage.show();
    	
    }

    @FXML
    private void FindJobRequestInterface(ActionEvent event) {
    }

    @FXML
    private void FindAllJobRequestsInterface(ActionEvent event) throws IOException { 	
  	   FXMLLoader loader = new FXMLLoader(getClass().getResource("FindAllJobRequests.fxml"));
       Parent root = (Parent) loader.load();
       
       Scene newScene = new Scene(root);
       Stage newStage = new Stage();
       newStage.setScene(newScene);
       newStage.show();
    }

    @FXML
    private void FindllContractsInterface(ActionEvent event) {
    }

    @FXML
    private void FindEmployee(ActionEvent event) {
    }

    @FXML
    private void UpgradeEmployeeInterface(ActionEvent event) {
    }
    
}
