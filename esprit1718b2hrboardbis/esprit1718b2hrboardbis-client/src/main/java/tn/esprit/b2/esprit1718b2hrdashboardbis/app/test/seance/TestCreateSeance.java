package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.seance;

import java.sql.Date;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;

public class TestCreateSeance{
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		
		SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");
		
		int idEmployee=1;
		int idTrainer=4;
		int idTraining=7;
		Date dateSeance=Date.valueOf("2018-02-12");
		String salle="C11";
		Seance seance=new Seance(idEmployee, idTrainer, idTraining, dateSeance, salle);
		seanceServiceRemote.save(seance);
	}
}
