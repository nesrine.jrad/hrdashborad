/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;

import com.jfoenix.controls.JFXToggleButton;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class FindAllInterviewController implements Initializable {

    @FXML
    private AnchorPane TextFindJobOffer;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXButton HomeManager;
    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView JObOfferImg;
    @FXML
    private JFXButton BackRecrutments;
    @FXML
    private ImageView BAckImg;
    @FXML
    private TableView<Interview> ListOfJobOffer;
    @FXML
    private TableColumn<Interview, Date> DateOfInterview;
    @FXML
    private TableColumn<Interview, String> TimeOfInterview;
    @FXML
    private TableColumn<Interview, Integer> InterviewEvaluation;
    @FXML
    private TableColumn<Interview, Integer> idOfInterview;
    @FXML
    private JFXButton recrutEmployee;
    @FXML
    private ImageView recrutImg;
    
    @FXML
    private ImageView FindJobOfferBt;
    @FXML
    private JFXButton DeleteJobOfferBT;
    @FXML
    private ImageView DeleteImg;
    @FXML
    private TableView<JobOffer> jobOfferList;
    @FXML
    private TableColumn<JobOffer, String> JobOfferName;
    private Context context;
    private ObservableList<Interview> data;
    private ObservableList<JobOffer> data2;
    @FXML
    private JFXToggleButton filterButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	  Image img = new Image("assets/home.png");
          imgBtHome.setImage(img);
          img = new Image("assets/projectmenu.png");
          imgbtProjects.setImage(img);
          img = new Image("assets/hrhead.jpg");
          headimg.setImage(img);
          img = new Image("assets/M2A.png");
          logoImg.setImage(img);
          img = new Image("assets/lecture.png");
          imgBtTrainig.setImage(img);
          img = new Image("assets/luggage.png");
          imgBtVac.setImage(img);
          img = new Image("assets/calendar.png");
          imgBtAbsences.setImage(img);
          img = new Image("assets/users.png");
          imgBtProfile.setImage(img);
          img = new Image("assets/chatting.png");
         imgBtCommunication.setImage(img);
          img = new Image("assets/salary.png");
          imgBtSalaire.setImage(img);
           img = new Image("assets/logout.png");
          imgBtlogout.setImage(img);
           img = new Image("assets/hi.png");
          imgHi.setImage(img);
          img = new Image("assets/delete.png");
          DeleteImg.setImage(img);
          img = new Image("assets/Find.png");
          FindJobOfferBt.setImage(img);
          img = new Image("assets/back.png");
          BAckImg.setImage(img);
          img = new Image("assets/interview.png");
          JObOfferImg.setImage(img);
          ListOfJobOffer.setItems(null);
          ListOfJobOffer.setItems(null);
          jobOfferList.setStyle("-fx-selection-bar:#D4D4D4");
          recrutImg.setVisible(false);
          recrutEmployee.setVisible(false);
        
          ListOfJobOffer.setStyle("-fx-selection-bar:#D4D4D4");
          try {
			context=new InitialContext();
			List<Interview> interviews= new ArrayList<Interview>();
			InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
			interviews=interviewServicesRemote.findAll();
			data= FXCollections.observableList(interviews);
			List<JobOffer> jobOffers= new ArrayList<JobOffer>();
			for (Interview interview : interviews) {
				System.out.println(interview.getJobRequest().getJobOffer().getName());
				jobOffers.add(interview.getJobRequest().getJobOffer());
				
			}
			data2=FXCollections.observableList(jobOffers);
			//interviews.forEach(System.outprintln);
			InterviewEvaluation.setCellValueFactory(new PropertyValueFactory<>("evaluation"));
			 idOfInterview.setVisible(false);
			 TimeOfInterview.setCellValueFactory(new PropertyValueFactory<>("timeOfInterview"));
	  		 idOfInterview.setCellValueFactory(new PropertyValueFactory<>("id"));
				DateOfInterview.setCellValueFactory(new PropertyValueFactory<>("dateOfInterview"));
				ListOfJobOffer.setItems(data);
				JobOfferName.setCellValueFactory(new PropertyValueFactory<>("name"));
				jobOfferList.setItems(data2);
		} catch (NamingException e) {
		}
    }    

    @FXML
    private void HomeManagerInterface(ActionEvent event) throws IOException {
    	  FXMLLoader loader = new FXMLLoader(getClass().getResource("FindAllInterview.fxml"));
		       Parent root = (Parent) loader.load();

		         Stage stage = (Stage) DeleteImg.getScene().getWindow();
		         // do what you have to do
		         stage.close();
      	 
		       Scene newScene = new Scene(root);
		       Stage newStage = new Stage();
		       newStage.setScene(newScene);
		       newStage.show();
    	
    }

    @FXML
    private void projectInterface(ActionEvent event) {
    }

    @FXML
    private void JobOfferManageInterface(MouseEvent event) {
    }

    @FXML
    private void BackrecrutmentsInterface(ActionEvent event) {
    }

    @FXML
    private void FindJobOffer(MouseEvent event) {
    }

    @FXML
    private void DeleteJobOffer(ActionEvent event) {
    }
    @FXML
    private void recrutEmployee(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AddEmployee.fxml"));
        Parent root = (Parent) loader.load();
        AddEmployeeController addEmployeeController= new AddEmployeeController();
        addEmployeeController=loader.getController();
        Interview interview = ListOfJobOffer.getSelectionModel().getSelectedItem();
        addEmployeeController.recupererInterview(interview);
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }
    @FXML
    private void ItemSelected(MouseEvent event) {
    	
    	Integer index=ListOfJobOffer.getSelectionModel().getSelectedIndex();
    	jobOfferList.getSelectionModel().select(index);
    	
   
    }

    @FXML
    private void ChangeInterview(ActionEvent event) throws IOException {
    	recrutImg.setVisible(true);
    	recrutEmployee.setVisible(true);
    if(	filterButton.isSelected())
    	{ ObservableList<Interview> data3;
    	   ObservableList<JobOffer> data4;
       	data=ListOfJobOffer.getItems();
       	List<Interview> interviews= new ArrayList<Interview>();
       	List<Interview> interviews1= new ArrayList<Interview>();
       	interviews.addAll(data);
       	for (Interview interview : interviews) {
       		if((interview.getDateOfInterview().before(java.sql.Date.valueOf(java.time.LocalDate.now()))))
   			{
   				interviews1.add(interview);
   			}
       		
   		}
       	
   			
   			 data3= FXCollections.observableList(interviews1);		
   			ListOfJobOffer.getItems().clear();
   			ListOfJobOffer.setItems(data3);
   		    List<JobOffer> jobOffers= new ArrayList<JobOffer>();
   		    jobOfferList.getItems().clear();
   			for (Interview interview : interviews1) {
   				System.out.println(interview.getJobRequest().getJobOffer().getName());
   				jobOffers.add(interview.getJobRequest().getJobOffer());}
   				 data4= FXCollections.observableList(jobOffers);	
   				 jobOfferList.setItems(data4);
   			}
   			else
   			{
   		   	   FXMLLoader loader = new FXMLLoader(getClass().getResource("FindAllInterview.fxml"));
   		       Parent root = (Parent) loader.load();
   		       
   		       Scene newScene = new Scene(root);
   		       Stage newStage = new Stage();
   		       newStage.setScene(newScene);
   		       newStage.show();
   		    	
   			}
   			
   		}
}
  
    

