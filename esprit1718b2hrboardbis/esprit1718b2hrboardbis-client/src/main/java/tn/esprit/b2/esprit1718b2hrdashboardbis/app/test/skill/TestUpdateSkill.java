package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.skill;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote;;

public class TestUpdateSkill {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		SkillServiceRemote skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");

		Skill skillToUpdate=skillServiceRemote.find(2);
		skillToUpdate.setDescription("Symfony is a set of reusable PHP components and a PHP framework to build web applications, APIs, microservices and web services.");
		skillServiceRemote.update(skillToUpdate);

	}
}
