/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class FindAllJobRequestsController implements Initializable {

    @FXML
    private AnchorPane TextFindJobOffer;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXButton HomeManager;
    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView JObOfferImg;
    @FXML
    private JFXButton BackRecrutments;
    @FXML
    private ImageView BAckImg;
    @FXML
    private TableColumn<?, ?> idJobOffer12;
    @FXML
    private ImageView FindJobOfferBt;
    @FXML
    private JFXButton DeleteJobOfferBT;
    @FXML
    private ImageView DeleteImg;
    @FXML
    private JFXButton UpdateJobOfferBT;
    @FXML
    private ImageView UpdateImg;
    @FXML
    private TableView<JobRequest> ListJobRequest;
    @FXML
    private TableColumn<JobRequest, String> nameJobRequest;
    @FXML
    private TableColumn<JobRequest, String> LastNameJobRequest;
    @FXML
    private TableColumn<JobRequest, Date> dateOfBirthJobRequest;
    @FXML
    private TableColumn<JobRequest,Date> dateJobRequest;
    @FXML
    private TableColumn<JobRequest, String> mailJobRequest;
    @FXML
    private TableColumn<JobRequest, String> phoneJobRequest;
    @FXML
    private TableColumn<JobRequest, String> stateJobRequest;
    @FXML
    private TableView<JobOffer> jobOfferJobRequest;
    @FXML
    private TableColumn<JobOffer, String> ListJobOffer;
    private ObservableList<JobRequest> data;
    private ObservableList<JobOffer> data2;
    private Context context;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image img = new Image("assets/home.png");
        imgBtHome.setImage(img);
        img = new Image("assets/projectmenu.png");
        imgbtProjects.setImage(img);
        img = new Image("assets/hrhead.jpg");
        headimg.setImage(img);
        img = new Image("assets/M2A.png");
        logoImg.setImage(img);
        img = new Image("assets/lecture.png");
        imgBtTrainig.setImage(img);
        img = new Image("assets/luggage.png");
        imgBtVac.setImage(img);
        img = new Image("assets/calendar.png");
        imgBtAbsences.setImage(img);
        img = new Image("assets/users.png");
        imgBtProfile.setImage(img);
        img = new Image("assets/chatting.png");
       imgBtCommunication.setImage(img);
        img = new Image("assets/salary.png");
        imgBtSalaire.setImage(img);
         img = new Image("assets/logout.png");
        imgBtlogout.setImage(img);
         img = new Image("assets/hi.png");
        imgHi.setImage(img);
        img = new Image("assets/JobOffer.png");
        JObOfferImg.setImage(img);
        img = new Image("assets/Modify.jpg");
        UpdateImg.setImage(img);
        img = new Image("assets/delete.png");
        DeleteImg.setImage(img);
        img = new Image("assets/Find.png");
        FindJobOfferBt.setImage(img);
        img = new Image("assets/back.png");
        BAckImg.setImage(img);
       // Context context;
        
		try {
			context = new InitialContext();
			JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
			JobRequest jobRequest=new JobRequest();
			List<JobRequest> jobRequests= new ArrayList<JobRequest>();
			jobRequests=jobRequestServicesRemote.findAll();
			System.out.println("a");
			jobRequests.forEach(System.out::println);
			data= FXCollections.observableList(jobRequests);
			 ListJobRequest.setItems(null);
		        idJobOffer12.setCellValueFactory(new PropertyValueFactory<>("id"));
				 idJobOffer12.setVisible(false);
		           LastNameJobRequest.setCellValueFactory(new PropertyValueFactory<>("lastNameCandidate"));
		           mailJobRequest.setCellValueFactory(new PropertyValueFactory<>("emailOfCandidate"));
					nameJobRequest.setCellValueFactory(new PropertyValueFactory<>("nameCandidate"));
					phoneJobRequest.setCellValueFactory(new PropertyValueFactory<>("phoneOfCandidate"));
					stateJobRequest.setCellValueFactory(new PropertyValueFactory<>("state"));
					dateJobRequest.setCellValueFactory(new PropertyValueFactory<>("dateOfRequest"));
					dateOfBirthJobRequest.setCellValueFactory(new PropertyValueFactory<>("dateOfBirthCandidate"));
					ListJobRequest.setItems(data);
					jobOfferJobRequest.setItems(null);
					ListJobOffer.setCellValueFactory(new PropertyValueFactory<>("name"));
				//	departmentName.setText("department");
					List<JobOffer> jobOffers= new ArrayList<JobOffer>();
					for(JobRequest jobrequest:jobRequests)
					{	JobOffer joboffer= new JobOffer();
						joboffer=jobrequest.getJobOffer();
						JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");
						jobOffers.add(jobofferservicesremote.find(joboffer.getId()));
					}	
					data2= FXCollections.observableList(jobOffers);
					jobOfferJobRequest.setItems(data2);
		} catch (NamingException e) {
;
		}

       
 
    }    

    @FXML
    private void HomeManagerInterface(ActionEvent event) {
    }

    @FXML
    private void projectInterface(ActionEvent event) {
    }

    @FXML
    private void JobOfferManageInterface(MouseEvent event) {
    }

    @FXML
    private void BackrecrutmentsInterface(ActionEvent event) {
    }

    @FXML
    private void FindJobOffer(MouseEvent event) {
    }

    @FXML
    private void DeleteJobOffer(ActionEvent event) throws NamingException, IOException {
    	JobRequest jobRequest= new JobRequest();
    	jobRequest=ListJobRequest.getSelectionModel().getSelectedItem();
		 context= new InitialContext();
			JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
			jobRequestServicesRemote.delete(jobRequest);
		

    }

    @FXML
    private void UpdateJobOffer(ActionEvent event) throws IOException {
    	JobRequest jobRequest= new JobRequest();
    	jobRequest=ListJobRequest.getSelectionModel().getSelectedItem();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("CreateInterview.fxml"));
	       Parent root = (Parent) loader.load();
		CreateInterviewController createInterviewController= new CreateInterviewController();
		createInterviewController=loader.getController();
		createInterviewController.recupererJobRequest(jobRequest);
		//createInterviewController.setJobrequest(jobRequest);
		System.out.println(jobRequest.toString());
		Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();

}
    
}
