package tn.esprit.b2.esprit1718b2hrboardbis.app.test.trainer;

import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote;

public class TestReadAllTrainers {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");

		List<Trainer> trainers= new ArrayList<Trainer>();
		trainers=trainerServiceRemote.findAll();
		trainers.forEach(System.out::println);
	}
}
