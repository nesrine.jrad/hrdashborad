package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.sql.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskPK;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote;

public class TestUpdateTask {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		Employee employee = employeeServiceRemote.find(2);
		TaskPK taskId = new TaskPK(1, 1, "Hello world");
		Task task = taskServiceRemote.find(taskId);
		taskServiceRemote.updateEmployee(task, employee);

	}

}
