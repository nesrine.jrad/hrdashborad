package tn.esprit.b2.esprit1718b2hrboardbis.app.test.trainer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote;

public class TestCreateTrainer {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		
		TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");
		
		String name="Sameh";
		String familyName="Kebiri";
		String email="kebiri.sameh@gmail.com";
		String phoneNumber="88888888";
		String domain="JAVA EE";
		
		Trainer trainer=new Trainer(name, familyName, email, phoneNumber, domain);
		trainerServiceRemote.save(trainer);
	}
}
