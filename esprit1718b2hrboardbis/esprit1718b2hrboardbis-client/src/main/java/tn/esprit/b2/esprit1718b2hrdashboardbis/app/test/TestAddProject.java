package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.sql.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;

public class TestAddProject {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		String name = "PDev HR Dashboard";
		Date dateOfBeginning = Date.valueOf("2018-03-12");
		Project project = new Project(name, dateOfBeginning, null);
		
		projectServiceRemote.save(project);

	}

}
