/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.jfoenix.controls.JFXTextArea;

import javafx.fxml.FXML;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Alert;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskListView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskPK;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class MyTasksController implements Initializable {

    @FXML
    private ImageView toDoImg;
    @FXML
    private ImageView doingImg;
    @FXML
    private ImageView doneImg;
    @FXML
    private ImageView makeItDoneimg;
    @FXML
    private TableColumn<TaskListView, String> nameProjectColumn;
    @FXML
    private TableColumn<TaskListView, String> nameEmployeeColumn;
    @FXML
    private TableColumn<TaskListView, String> nameTaskColumn;
    @FXML
    private TableColumn<TaskListView, Date> startDateColumn;
    @FXML
    private TableColumn<TaskListView, Date> endDateColumn;
    @FXML
    private TableView<TaskListView> myTasksTable;
    //List of tasks to display
    private ObservableList<TaskListView> tasksToDisplay;
    @FXML
    private JFXButton makeItDoing;
    private int idProject;
    private TaskPK taskPK;
    @FXML
    private JFXTextArea descriptionTextArea;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	myTasksTable.setItems(null);
    	Image img = new Image("assets/ToDo.png");
    	toDoImg.setImage(img);
    	img = new Image("assets/Doing.png");
    	doingImg.setImage(img);
    	img = new Image("assets/Done.png");
    	doneImg.setImage(img);
    	img = new Image("assets/MakeItDone.png");
    	makeItDoneimg.setImage(img);
    	makeItDoneimg.setVisible(false);
    			
    }    

    @FXML
    private void displayToDoTaks(MouseEvent event) throws NamingException {
    	descriptionTextArea.setText("");
    	makeItDoneimg.setVisible(false);
    	makeItDoing.setVisible(true);
    	myTasksTable.setItems(null);
    	Context context = new InitialContext();
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		List<Task> tasksToDo = taskServiceRemote.getTasksByEmployeeAndStatusTask(HomeController.employeeConnected.getId(), StatusTask.ToDO,idProject);
		tasksToDisplay = this.convertListIntoObservableList(tasksToDo);
		myTasksTable.setItems(tasksToDisplay);
    }

    @FXML
    private void displayDoingTasks(MouseEvent event) throws NamingException {
    	descriptionTextArea.setText("");
    	makeItDoneimg.setVisible(true);
    	makeItDoing.setVisible(false);
    	myTasksTable.setItems(null);
    	Context context = new InitialContext();
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		List<Task> tasksDoing = taskServiceRemote.getTasksByEmployeeAndStatusTask(HomeController.employeeConnected.getId(), StatusTask.DOING,idProject);
		tasksToDisplay = this.convertListIntoObservableList(tasksDoing);
		myTasksTable.setItems(tasksToDisplay);
    }

    @FXML
    private void displayDoneTasks(MouseEvent event) throws NamingException {
    	descriptionTextArea.setText("");
    	makeItDoneimg.setVisible(false);
    	makeItDoing.setVisible(false);
    	myTasksTable.setItems(null);
    	Context context = new InitialContext();
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		List<Task> tasksDone = taskServiceRemote.getTasksByEmployeeAndStatusTask(HomeController.employeeConnected.getId(), StatusTask.DONE,idProject);
		tasksToDisplay = this.convertListIntoObservableList(tasksDone);
		myTasksTable.setItems(tasksToDisplay);
    }

    @FXML
    private void makeTaskDone(MouseEvent event) throws NamingException {
    	if(taskPK!=null){
    		Context context = new InitialContext();
    		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
    				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
    		Task task = taskServiceRemote.find(taskPK);
    		task.setStatusTask(StatusTask.DONE);
    		task.setDateEnd(Date.valueOf(LocalDate.now()));
    		taskServiceRemote.save(task);
    		Alert alert = new Alert(Alert.AlertType.INFORMATION,"You accomplished this task with success");
    		alert.showAndWait();
    	}
    	else{
    		Alert alert = new Alert(Alert.AlertType.ERROR,"Please choose a task");
    		alert.showAndWait();
    	}
    
    }
    
    //Convert a List<Task> into Observable<TaskListView>
    public ObservableList<TaskListView> convertListIntoObservableList (List<Task> tasks) throws NamingException{
    	List<TaskListView> tasksListView = new ArrayList<>();
    	for(Task task : tasks){
    		TaskListView taskListView = new TaskListView(task);
    		tasksListView.add(taskListView);   		
    	}
    	return FXCollections.observableList(tasksListView);

    }
    
    public void getTheProjectFromPreviousScreen(int idProject){
    	this.idProject = idProject;
    	Context context;
		try {
			context = new InitialContext();
			TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
			List<Task> tasksToDo = taskServiceRemote.getTasksByEmployeeAndStatusTask(HomeController.employeeConnected.getId(), StatusTask.ToDO, idProject);
			nameProjectColumn.setCellValueFactory(new PropertyValueFactory<>("nameProject"));
			nameEmployeeColumn.setCellValueFactory(new PropertyValueFactory<>("nameEmployee"));
			nameTaskColumn.setCellValueFactory(new PropertyValueFactory<>("nameTask"));
			startDateColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfbeginning"));
			endDateColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfEnd"));
			tasksToDisplay = this.convertListIntoObservableList(tasksToDo);
			myTasksTable.setItems(tasksToDisplay);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    @FXML
    void makeItDoing(ActionEvent event) throws NamingException {
    	if(taskPK!=null){
    		Context context = new InitialContext();
    		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
    				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
    		Task task = taskServiceRemote.find(taskPK);
    		task.setStatusTask(StatusTask.DOING);
    		taskServiceRemote.save(task);
    		Alert alert = new Alert(Alert.AlertType.INFORMATION,"Let the work BEGIN");
    		alert.showAndWait();
    	}
    	else{
    		Alert alert = new Alert(Alert.AlertType.ERROR,"Please choose a task");
    		alert.showAndWait();
    	}
    }
    
    @FXML
    private void getSelectedIdTask(MouseEvent event) {
    	TaskListView taskListView = myTasksTable.getSelectionModel().getSelectedItem();
    	taskPK = new TaskPK(taskListView.getIdProject(), taskListView.getIdEmployee(), taskListView.getNameTask());;
    	descriptionTextArea.setText(taskListView.getDescripiton());
    }
    
   
    
}
