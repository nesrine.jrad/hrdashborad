package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.training;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;;

public class TestReadTraining {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		TrainingServiceRemote trainingServiceRemote= (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");
		System.out.println(trainingServiceRemote.find(1));
	}
}
