/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


import org.hibernate.transform.AliasedTupleSubsetResultTransformer;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class AddProjectController implements Initializable {

    @FXML
    private DatePicker dateBeginningDatePicker;
    @FXML
    private DatePicker expectedEndDatePicker;
    @FXML
    private JFXButton addProjectBtn;
    @FXML
    private JFXButton exitBtn;
    @FXML
    private JFXTextField nameTextField;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LocalDate today= LocalDate.now();
		dateBeginningDatePicker.setValue(today);
		expectedEndDatePicker.setValue(today.plusDays(1));
		
    }    

    @FXML
	private void addProjectAction(ActionEvent event) throws NamingException, IOException {

		String name = nameTextField.getText();
		Date dateOfBeginning = Date.valueOf(dateBeginningDatePicker.getValue());
		Date expectedDateEnd = Date.valueOf(expectedEndDatePicker.getValue());
		if (name.equals("")) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Please verify the name");
			alert.showAndWait();
		}

		else if (expectedDateEnd.before(dateOfBeginning) || (expectedDateEnd == null) || (dateOfBeginning == null)) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Please verify the dates");
			alert.showAndWait();
		} else {
			Context context = new InitialContext();
			ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context
					.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
							+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");

			Project project = new Project(name, dateOfBeginning, expectedDateEnd);
			projectServiceRemote.save(project);
			FXMLLoader loader = new FXMLLoader(getClass().getResource("AssignEmployeesToProjectFXML.fxml"));
	        Parent root = (Parent) loader.load();
	        AssignEmployeesToProjectFXMLController assignEmployeesToProjectFXMLController = new AssignEmployeesToProjectFXMLController();
	        assignEmployeesToProjectFXMLController = loader.getController();
	        int idProject = projectServiceRemote.findIdOfLastRecord();
	        assignEmployeesToProjectFXMLController.getTheIdProjectFromThePreviousScreen(idProject);
	        Scene newScene = new Scene(root);
	        Stage newStage = new Stage();
	        newStage.setScene(newScene);
	        newStage.show();
		}
	}

    @FXML
    private void exitWitndowAction(ActionEvent event) {
    	Stage stage = (Stage)exitBtn.getScene().getWindow();
    	stage.close();
    	
    }
    
}
