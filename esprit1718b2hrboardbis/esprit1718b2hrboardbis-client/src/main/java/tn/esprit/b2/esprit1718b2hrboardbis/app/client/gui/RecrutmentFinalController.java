/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.criteria.CriteriaBuilder.In;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.EmployeeView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.InterviewView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOfferView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequestView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TypeContract;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class RecrutmentFinalController implements Initializable {

    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXButton HomeManager;
    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg1;
    @FXML
    private ImageView logoImg1;
    @FXML
    private JFXButton HomeManager1;
    @FXML
    private ImageView imgBtHome5;
    @FXML
    private ImageView imgbtProjects1;
    @FXML
    private ImageView imgBtTrainig1;
    @FXML
    private ImageView imgBtVac1;
    @FXML
    private ImageView imgBtAbsences1;
    @FXML
    private ImageView imgBtProfile1;
    @FXML
    private ImageView imgBtCommunication1;
    @FXML
    private ImageView imgBtSalaire1;
    @FXML
    private ImageView imgBtlogout1;
    @FXML
    private ImageView imgHi1;
    @FXML
    private ImageView imgBtHome1;
    @FXML
    private ImageView imgBtHome2;
    @FXML
    private ImageView imgBtHome3;
    @FXML
    private ImageView imgBtHome4;
    @FXML
    private TableView<JobOfferView> ListOfJobOffer;
    @FXML
    private TableColumn<?, String> PostJobOffer;
    @FXML
    private TableColumn<?, Date> DateJobOffer;
    @FXML
    private TableColumn<?, String> DescriptionJObOffer;
    @FXML
    private TableColumn<?, String> TimeOfJobOffer;
    @FXML
    private TableColumn<?, Float> SalaryOfJobOffer;
    @FXML
    private TableColumn<?, String> departmentJobOffer;
    @FXML
    private TableColumn<?, TypeContract> ContractJobOffer;
    @FXML
    private TableColumn<?, Integer> idJobOffer12;
    @FXML
    private ImageView addJobOffer;
    @FXML
    private ImageView deleteJobOffer;
    @FXML
    private ImageView CreateInterview;
    @FXML
    private ImageView deleteJobRequest;
    @FXML
    private ImageView recrut;
    @FXML
    private ImageView deleteInterview;
    @FXML
    private JFXToggleButton filterInterviewB;
    @FXML
    private Label NonDoneInterviewLabel;
    @FXML
    private ImageView FindEmployeeContract;
    @FXML
    private TableView<JobRequestView> ListJobRequest;
    @FXML
    private TableColumn<JobRequestView, String> nameJobRequest;
    @FXML
    private TableColumn<JobRequestView, String> LastNameJobRequest;
    @FXML
    private TableColumn<JobRequestView, Date> dateOfBirthJobRequest;
    @FXML
    private TableColumn<JobRequestView, Date> dateJobRequest;
    @FXML
    private TableColumn<JobRequestView, String> mailJobRequest;
    @FXML
    private TableColumn<JobRequestView, String> phoneJobRequest;
    @FXML
    private TableColumn<JobRequestView, String> stateJobRequest;
    @FXML
    private TableColumn<JobRequestView, Integer> idJobOffer121;
    @FXML
    private TableColumn<JobRequestView, String> postJobRequest;
    @FXML
    private TableView<InterviewView> ListInterview;
    @FXML
    private TableColumn<?, Date> DateOfInterview;
    @FXML
    private TableColumn<?, String> TimeOfInterview;
    @FXML
    private TableColumn<?, Integer> InterviewEvaluation;
    @FXML
    private TableColumn<?, Integer> idOfInterview;
    @FXML
    private TableColumn<?, Integer> InterviewEvaluation1;
    @FXML
    private TableView<EmployeeView> EmployeesList;
    @FXML
    private TableColumn<?, String> NameEmployee;
    @FXML
    private TableColumn<?, String> LAstNameEmployee;
    @FXML
    private TableColumn<?, Integer> idEmployee;
    @FXML
    private TableColumn<?, RoleEmployee> RoleEmployee;
    @FXML
    private TableColumn<?, String> DepartmentEmployee;
    @FXML
    private TableColumn<?, String> MailEmployee;
    @FXML
    private TableColumn<?, String> phoneEmployee;
    @FXML
    private TableColumn<?, Float> SalalryEmployee;
    @FXML
    private ImageView promoteEmployee;
    private ObservableList<JobRequestView> data;
    private ObservableList<JobOfferView> data2;
    private ObservableList<InterviewView> data3;
    private ObservableList<EmployeeView> data4;
    private Context context;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      	Image img = new Image("assets/home.png");
        imgBtHome.setImage(img);
        img = new Image("assets/projectmenu.png");
        imgbtProjects.setImage(img);
        img = new Image("assets/save.png");
        addJobOffer.setImage(img);
        CreateInterview.setImage(img);
        recrut.setImage(img);
        promoteEmployee.setImage(img);
        img = new Image("assets/contract.png");
        FindEmployeeContract.setImage(img);
        img = new Image("assets/delete.jpg");
        deleteInterview.setImage(img);
        deleteJobOffer.setImage(img);
        deleteJobRequest.setImage(img);
        img = new Image("assets/JobOffer.png");
        imgBtHome1.setImage(img);
        img = new Image("assets/JobRequest.png");
        imgBtHome2.setImage(img);
         img = new Image("assets/interview.png");
        imgBtHome3.setImage(img);
        img = new Image("assets/GoodManagement.png");
        imgBtHome4.setImage(img);
        img = new Image("assets/hrhead.jpg");
        headimg.setImage(img);
        img = new Image("assets/M2A.png");
        logoImg.setImage(img);
        img = new Image("assets/lecture.png");
        imgBtTrainig.setImage(img);
        img = new Image("assets/luggage.png");
        imgBtVac.setImage(img);
        img = new Image("assets/calendar.png");
        imgBtAbsences.setImage(img);
        img = new Image("assets/users.png");
        imgBtProfile.setImage(img);
        img = new Image("assets/chatting.png");
       imgBtCommunication.setImage(img);
        img = new Image("assets/salary.png");
        imgBtSalaire.setImage(img);
         img = new Image("assets/logout.png");
        imgBtlogout.setImage(img);
         img = new Image("assets/hi.png");
        imgHi.setImage(img);
        ListInterview.setVisible(false);
        ListJobRequest.setVisible(false);
        EmployeesList.setVisible(false);
        ListOfJobOffer.setVisible(false);
        filterInterviewB.setVisible(false);
        NonDoneInterviewLabel.setVisible(false);
        deleteInterview.setVisible(false);
        deleteJobOffer.setVisible(false);
        deleteJobRequest.setVisible(false);
        addJobOffer.setVisible(false);
        CreateInterview.setVisible(false);
        recrut.setVisible(false);
        promoteEmployee.setVisible(false);
        FindEmployeeContract.setVisible(false);
        
    }    

    @FXML
    private void HomeManagerInterface(ActionEvent event) throws IOException {
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
	       Parent root = (Parent) loader.load();

	         Stage stage = (Stage) addJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
	 
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }
    

    @FXML
    private void projectInterface(ActionEvent event) throws IOException {
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("recrutmentFinal.fxml"));
	       Parent root = (Parent) loader.load();

	         Stage stage = (Stage) addJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
	 
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }
    @FXML
    private void TrainingInterface(ActionEvent event) throws IOException {
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("recrutmentFinal.fxml"));
	       Parent root = (Parent) loader.load();

	         Stage stage = (Stage) addJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
	 
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }
    @FXML
    private void LeaveInterface(ActionEvent event) throws IOException {
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("recrutmentFinal.fxml"));
	       Parent root = (Parent) loader.load();

	         Stage stage = (Stage) addJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
	 
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }
    @FXML
    private void ProfilInterface(ActionEvent event) throws IOException {
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("recrutmentFinal.fxml"));
	       Parent root = (Parent) loader.load();

	         Stage stage = (Stage) addJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
	 
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }
    @FXML
    private void AbsenceInterface(ActionEvent event) throws IOException {
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("recrutmentFinal.fxml"));
	       Parent root = (Parent) loader.load();

	         Stage stage = (Stage) addJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
	 
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }

    @FXML
    private void JobOfferManageInterface(MouseEvent event) {
    	  ListInterview.setVisible(false);
          ListJobRequest.setVisible(false);
          EmployeesList.setVisible(false);
          ListOfJobOffer.setVisible(true);
          filterInterviewB.setVisible(false);
          NonDoneInterviewLabel.setVisible(false);
          deleteInterview.setVisible(false);
          deleteJobOffer.setVisible(true);
          deleteJobRequest.setVisible(false);
          addJobOffer.setVisible(true);
          CreateInterview.setVisible(false);
          recrut.setVisible(false);
          promoteEmployee.setVisible(false);
          FindEmployeeContract.setVisible(false);
          try {
  			context = new InitialContext();
  			JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");
  			List<JobOffer> jobOffers=new ArrayList<JobOffer>();
  			jobOffers=jobofferservicesremote.findAll();
  			List<JobOfferView> jobOfferViews= new ArrayList<JobOfferView>();
  			
  			for (JobOffer jobOffer : jobOffers) {
				JobOfferView jobOfferView= new JobOfferView(jobOffer.getId(), jobOffer.getDateOfPublishing(), jobOffer.getName(), jobOffer.getDescription(), jobOffer.getSalary(), jobOffer.getTimeOfJob(), jobOffer.getTypeContrat(), jobOffer.getDepartment().getName());
				jobOfferViews.add(jobOfferView);
  			}
  			
  			
  			idJobOffer12.setCellValueFactory(new PropertyValueFactory<>("id"));
  	         PostJobOffer.setCellValueFactory(new PropertyValueFactory<>("name"));
  	         DateJobOffer.setCellValueFactory(new PropertyValueFactory<>("dateOfPublishing"));
  	         TimeOfJobOffer.setCellValueFactory(new PropertyValueFactory<>("TimeOfJob"));
  			 ContractJobOffer.setCellValueFactory(new PropertyValueFactory<>("typeContrat"));
  			 SalaryOfJobOffer.setCellValueFactory(new PropertyValueFactory<>("Salary"));
  			 DescriptionJObOffer.setCellValueFactory(new PropertyValueFactory<>("description"));
  			 departmentJobOffer.setCellValueFactory(new PropertyValueFactory<>("department"));
  				
  				data2=FXCollections.observableList(jobOfferViews);
  				ListOfJobOffer.setItems(data2);

  		} catch (NamingException e) {
  		
  		}
    }

    @FXML
    private void JobRequestManageInterface(MouseEvent event) {
    	ListInterview.setVisible(false);
        ListJobRequest.setVisible(true);
        EmployeesList.setVisible(false);
        ListOfJobOffer.setVisible(false);
        filterInterviewB.setVisible(false);
        NonDoneInterviewLabel.setVisible(false);
        deleteInterview.setVisible(false);
        deleteJobOffer.setVisible(false);
        deleteJobRequest.setVisible(true);
        addJobOffer.setVisible(false);
        CreateInterview.setVisible(true);
        recrut.setVisible(false);
        promoteEmployee.setVisible(false);
        FindEmployeeContract.setVisible(false);
        try {
			context = new InitialContext();
			JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
			JobRequest jobRequest=new JobRequest();
			List<JobRequest> jobRequests= new ArrayList<JobRequest>();
			List<JobRequestView> jobRequestViews= new ArrayList<JobRequestView>();
			jobRequests=jobRequestServicesRemote.findAll();
			for (JobRequest jobRequest2 : jobRequests) {
				JobRequestView jobRequestView=  new JobRequestView(jobRequest2.getId(), jobRequest2.getDateOfRequest(), jobRequest2.getNameCandidate(), jobRequest2.getLastNameCandidate(), jobRequest2.getDateOfBirthCandidate(), jobRequest2.getPhoneOfCandidate(), jobRequest2.getEmailOfCandidate(), jobRequest2.getJobOffer().getName(), jobRequest2.getState());
				jobRequestViews.add(jobRequestView);
			}
			for (JobRequestView jobRequestView : jobRequestViews) {
				System.out.println(jobRequestView.getPost());
			}
			data= FXCollections.observableList(jobRequestViews);
			ListJobRequest.setItems(null);
			idJobOffer121.setCellValueFactory(new PropertyValueFactory<>("id"));
			 idJobOffer12.setVisible(false);
	           LastNameJobRequest.setCellValueFactory(new PropertyValueFactory<>("lastNameCandidate"));
	           mailJobRequest.setCellValueFactory(new PropertyValueFactory<>("emailOfCandidate"));
				nameJobRequest.setCellValueFactory(new PropertyValueFactory<>("nameCandidate"));
				phoneJobRequest.setCellValueFactory(new PropertyValueFactory<>("phoneOfCandidate"));
				stateJobRequest.setCellValueFactory(new PropertyValueFactory<>("state"));
				dateJobRequest.setCellValueFactory(new PropertyValueFactory<>("dateOfRequest"));
				dateOfBirthJobRequest.setCellValueFactory(new PropertyValueFactory<>("dateOfBirthCandidate"));
				PostJobOffer.setCellValueFactory(new PropertyValueFactory<>("post"));
				idJobOffer121.setCellValueFactory(new PropertyValueFactory<>("id"));
				
				ListJobRequest.setItems(data);
				
	
			 //System.out.println(data.get(0));
			ListJobRequest.setItems(data);
		
			
		} catch (NamingException e) {

		}
    }

    @FXML
    private void InterviewsManageInterface(MouseEvent event) {
    	 ListInterview.setVisible(true);
         ListJobRequest.setVisible(false);
         EmployeesList.setVisible(false);
         ListOfJobOffer.setVisible(false);
         filterInterviewB.setVisible(true);
         NonDoneInterviewLabel.setVisible(true);
         deleteInterview.setVisible(true);
         deleteJobOffer.setVisible(false);
         deleteJobRequest.setVisible(false);
         addJobOffer.setVisible(false);
         CreateInterview.setVisible(false);
         recrut.setVisible(false);
         promoteEmployee.setVisible(false);
         FindEmployeeContract.setVisible(false);
         ListInterview.setItems(null);
         try {
 			context=new InitialContext();
 			List<Interview> interviews= new ArrayList<Interview>();
 			InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
 			interviews=interviewServicesRemote.findAll();
 			//interviews.forEach(System.out::println);
 			//System.out.println(interviews.size());
 			List<InterviewView> interviewViews= new ArrayList<InterviewView>();
 			
 			for (Interview interview : interviews) {
 	 			//System.out.println("*********");

 				InterviewView interviewView= new InterviewView(interview.getId(), interview.getJobRequest().getEmailOfCandidate(), interview.getDateOfInterview(), interview.getTimeOfInterview(), interview.getEvalution(), interview.getEmployee().getId(), interview.getJobRequest().getJobOffer().getName());
 				interviewViews.add(interviewView);
 			}
 			data3=FXCollections.observableList(interviewViews);
 			//interviews.forEach(System.outprintln);
 			    InterviewEvaluation.setCellValueFactory(new PropertyValueFactory<>("evalution"));
 			    TimeOfInterview.setCellValueFactory(new PropertyValueFactory<>("timeOfInterview"));
 	  		    idOfInterview.setCellValueFactory(new PropertyValueFactory<>("id"));
 				DateOfInterview.setCellValueFactory(new PropertyValueFactory<>("dateOfInterview"));
 				InterviewEvaluation1.setCellValueFactory(new PropertyValueFactory<>("jobOffer"));
 				
 				ListInterview.setItems(data3);
 		} catch (NamingException e) {
 		}
    }

    @FXML
    private void EmployeesManagementInterface(MouseEvent event) {
    	 ListInterview.setVisible(false);
         ListJobRequest.setVisible(false);
         EmployeesList.setVisible(true);
         ListOfJobOffer.setVisible(false);
         filterInterviewB.setVisible(false);
         NonDoneInterviewLabel.setVisible(false);
         deleteInterview.setVisible(false);
         deleteJobOffer.setVisible(false);
         deleteJobRequest.setVisible(false);
         addJobOffer.setVisible(false);
         CreateInterview.setVisible(false);
         recrut.setVisible(false);
         promoteEmployee.setVisible(true);
         FindEmployeeContract.setVisible(true);
         EmployeesList.setItems(null);;
         try {
  			context=new InitialContext();
  			List<Employee> employees= new ArrayList<Employee>();
  			EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
  			employees =employeeServiceRemote.findAll();
  			employees.forEach(System.out::println);
  			System.out.println(employees.size());
  			List<EmployeeView> employeeViews= new ArrayList<EmployeeView>();
  			for(Employee employee: employees)
  			{
  				System.out.println("****"+employee.getName());
			 EmployeeView employeeView= new EmployeeView(employee.getId(),employee.getName(), employee.getFamilyName(), employee.getDateOfBirth(), employee.getDateOfHiring(), employee.getEmail(), employee.getPhoneNumber(), employee.getAddress(), employee.getSalary(), employee.getRoleEmployee(), employee.getTypeContract());
			 employeeViews.add(employeeView);

  				
  			}
  			
  		
  			data4=FXCollections.observableList(employeeViews);
  			
  			    NameEmployee.setCellValueFactory(new PropertyValueFactory<>("name"));
  			    LAstNameEmployee.setCellValueFactory(new PropertyValueFactory<>("familyName"));
  	  		    MailEmployee.setCellValueFactory(new PropertyValueFactory<>("email"));
  				//DepartmentEmployee.setCellValueFactory(new PropertyValueFactory<>("departments"));
  				SalalryEmployee.setCellValueFactory(new PropertyValueFactory<>("Salary"));
  				idEmployee.setCellValueFactory(new PropertyValueFactory<>("id"));
  				RoleEmployee.setCellValueFactory(new PropertyValueFactory<>("roleEmployee"));
  				phoneEmployee.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));

  				EmployeesList.setItems(data4);
  		} catch (NamingException e) {
  		}
         
         
    }

    @FXML
    private void AddJobOffer(MouseEvent event) throws IOException {
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("AddJobOffer.fxml"));
	       Parent root = (Parent) loader.load();

	        
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }

    @FXML
    private void DeleteJobOffer(MouseEvent event) {
    	//System.out.println("***********");
    	JobOffer jobOffer= new JobOffer();
    	JobOfferView jobOfferView= ListOfJobOffer.getSelectionModel().getSelectedItem();
    	try {
		 context= new InitialContext();
			JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");
			jobOffer=jobofferservicesremote.find(jobOfferView.getId());
			System.out.println(jobOffer.getId());
			List<JobRequest> jobRequests= new ArrayList<JobRequest>();
			jobRequests=jobOffer.getJobRequest();
			for (JobRequest jobRequest : jobRequests) {
				List<Interview> interviews= new ArrayList<Interview>();
					for (Interview interview : interviews) {
						if(interview.getJobRequest().getId()==jobRequest.getId()){
							context=new InitialContext();
							InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
							interviewServicesRemote.deleteInterview(interview);
					}
					
				}
				
			}
			
			jobofferservicesremote.delete(jobOffer);
		} catch (NamingException e) {
		}
		}
    
    
    @FXML
    private void DeleteJobRequest(MouseEvent event) throws NamingException {
       	JobRequest jobRequest= new JobRequest();
       	JobRequestView jobRequestView=ListJobRequest.getSelectionModel().getSelectedItem();
    
		 context= new InitialContext();
			JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
			jobRequest=jobRequestServicesRemote.find(jobRequestView.getId());
			jobRequestServicesRemote.delete(jobRequest);
    }

    @FXML
    private void ChangeInterview(ActionEvent event) throws IOException {

  		
     if(	filterInterviewB.isSelected())
     	{          recrut.setVisible(true);

    			ObservableList<InterviewView> data5;
    	    	   ObservableList<JobOffer> data4;
    	       	data5=ListInterview.getItems();
    	       
    	     
    	       	List<InterviewView> interviews= new ArrayList<InterviewView>();
    	       	List<InterviewView> interviews1= new ArrayList<InterviewView>();
    	       	interviews.addAll(data5);
    	       	System.out.println(data5.size());
    	       	System.out.println(interviews.size());
    	       	for (InterviewView interview : interviews) {
    	       		if((interview.getDateOfInterview().before(java.sql.Date.valueOf(java.time.LocalDate.now()))))
    	   			{   System.out.println(interview.getDateOfInterview());
    	   				interviews1.add(interview);
    	   			}
    	   		}
	       		System.out.println(interviews1.size());
     			data3=FXCollections.observableList(interviews1);
     			 InterviewEvaluation.setCellValueFactory(new PropertyValueFactory<>("evalution"));
	 			    TimeOfInterview.setCellValueFactory(new PropertyValueFactory<>("timeOfInterview"));
	 	  		    idOfInterview.setCellValueFactory(new PropertyValueFactory<>("id"));
	 				DateOfInterview.setCellValueFactory(new PropertyValueFactory<>("dateOfInterview"));
	 				InterviewEvaluation1.setCellValueFactory(new PropertyValueFactory<>("jobOffer"));
	 				 ListInterview.getItems().clear();
	 				ListInterview.setItems(data3);
        		
    		
        }
    			else
    			{
    				try {
    		 			context=new InitialContext();
    		 			List<Interview> interviews= new ArrayList<Interview>();
    		 			InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
    		 			interviews=interviewServicesRemote.findAll();
    		 			interviews.forEach(System.out::println);
    		 			List<InterviewView> interviewViews= new ArrayList<InterviewView>();
    		 			for (Interview interview : interviews) {
    		 				InterviewView interviewView= new InterviewView(interview.getId(), interview.getJobRequest().getEmailOfCandidate(), interview.getDateOfInterview(), interview.getTimeOfInterview(), interview.getEvalution(), interview.getEmployee().getId(), interview.getJobRequest().getJobOffer().getName());
    		 				interviewViews.add(interviewView);
    		 			}
    		 			data3=FXCollections.observableList(interviewViews);
    		 			//interviews.forEach(System.outprintln);
    		 			    InterviewEvaluation.setCellValueFactory(new PropertyValueFactory<>("evalution"));
    		 			    TimeOfInterview.setCellValueFactory(new PropertyValueFactory<>("timeOfInterview"));
    		 	  		    idOfInterview.setCellValueFactory(new PropertyValueFactory<>("id"));
    		 				DateOfInterview.setCellValueFactory(new PropertyValueFactory<>("dateOfInterview"));
    		 				InterviewEvaluation1.setCellValueFactory(new PropertyValueFactory<>("jobOffer"));
    		 				ListInterview.setItems(data3);
    		 		} catch (NamingException e) {
    		 		}
    				}
    }
    @FXML
    private void FindEmployeeContract(MouseEvent event) {
    	//EmployeeView employeeView
    }

    @FXML
    private void ItemSelected(MouseEvent event) throws IOException, NamingException {
    	context= new InitialContext();
     	 FXMLLoader loader = new FXMLLoader(getClass().getResource("UpdateJobOffer.fxml"));
     	 	JobOfferView jobOfferView= ListOfJobOffer.getSelectionModel().getSelectedItem();
  			JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");
     	 	JobOffer jobOffer=jobofferservicesremote.find(jobOfferView.getId()) ;
	       Parent root = (Parent) loader.load();
	       UpdateJobOfferController updateJobOfferController= new UpdateJobOfferController();
	       updateJobOfferController=loader.getController();
	       updateJobOfferController.setTexts(jobOffer);
	        
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    	
   
    }

    @FXML
    private void DeleteInterview(MouseEvent event) throws NamingException {
    	System.out.println("***************");
        InterviewView interviewView=ListInterview.getSelectionModel().getSelectedItem();
        context=new InitialContext();
		List<Interview> interviews= new ArrayList<Interview>();
		InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
      
		System.out.println("********///////*******"+interviewView.getId());
		Interview interview= interviewServicesRemote.find(interviewView.getId());
		System.out.println("newwwww :"+interview.getId());
		interviewServicesRemote.deleteInterview(interview);
    	
    }
    @FXML
    private void recrutEmployee(MouseEvent event) throws NamingException, IOException {
       	FXMLLoader loader = new FXMLLoader(getClass().getResource("AddEmployee.fxml"));
        Parent root = (Parent) loader.load();
        AddEmployeeController addEmployeeController= new AddEmployeeController();
        addEmployeeController=loader.getController();
        InterviewView interviewView = ListInterview.getSelectionModel().getSelectedItem();
        context= new InitialContext();
	    InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
	    Interview interview= new Interview();
	    interview=interviewServicesRemote.find(interviewView.getId());
        addEmployeeController.recupererInterview(interview);
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    	
    }
    
    @FXML
    private void CreateInterview(MouseEvent event) throws IOException, NamingException {
    	JobRequestView jobRequestView= ListJobRequest.getSelectionModel().getSelectedItem();
    	context= new InitialContext();
		JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
		JobRequest jobRequest= jobRequestServicesRemote.find(jobRequestView.getId());
    	
    	System.out.println("************");
   	 FXMLLoader loader = new FXMLLoader(getClass().getResource("CreateInterview.fxml"));
     Parent root = (Parent) loader.load();
		CreateInterviewController createInterviewController= new CreateInterviewController();
		createInterviewController=loader.getController();
		createInterviewController.recupererJobRequest(jobRequest);
		//createInterviewController.setJobrequest(jobRequest);
		System.out.println(jobRequest.toString());
		Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }
    @FXML
    private void promoteEmployee(MouseEvent event) {
    }
    @FXML
    private void ItemSelectedEmployee(MouseEvent event) throws NamingException, IOException  {
       	context= new InitialContext();
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("InformationEmployees.fxml"));
    	 	EmployeeView employeeView= EmployeesList.getSelectionModel().getSelectedItem();
    	 	//List<Employee> employees= new ArrayList<Employee>();
  			EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
    	 	Employee employee=employeeServiceRemote.find(employeeView.getId()) ;
	       Parent root = (Parent) loader.load();
	       InformationEmployeesController informationEmployeesController= new InformationEmployeesController();
	       informationEmployeesController=loader.getController();
	       informationEmployeesController.setEmployees(employee);
	       Scene newScene = new Scene(root);
	       Stage newStage = new Stage();
	       newStage.setScene(newScene);
	       newStage.show();
    }
    
    
}
