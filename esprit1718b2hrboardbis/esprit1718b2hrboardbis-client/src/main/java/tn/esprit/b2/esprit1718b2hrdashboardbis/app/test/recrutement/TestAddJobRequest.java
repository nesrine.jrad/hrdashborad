package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.recrutement;

import java.lang.invoke.MethodHandles.Lookup;
import java.sql.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote;

public class TestAddJobRequest {
	public static void main(String[] args) throws NamingException
	{
		Context context = new InitialContext();
		JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
		JobOfferServicesRemote jobofferservicesremote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");

		Date dateOfRequest= Date.valueOf("2018-05-03");
	JobRequest jobRequest = new JobRequest();
	JobOffer jobOffer= new JobOffer();
	Interview interview= new Interview();
	jobOffer=jobofferservicesremote.find(19);
	jobRequest.setDateOfBirthCandidate(dateOfRequest);
	jobRequest.setLastNameCandidate("last name");
	jobRequest.setDateOfRequest(dateOfRequest);
	jobRequest.setEmailOfCandidate("essai2");
	jobRequest.setNameCandidate("name");
	jobRequest.setJobOffer(jobOffer);
	jobRequest.setState("no treat");
	jobRequestServicesRemote.save(jobRequest);
	}

}
