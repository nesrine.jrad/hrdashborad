/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import javafx.scene.control.Alert;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Status;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class ProjetController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    private ImageView imgNews;
    private ImageView imgVacationRest;
    private ImageView imgProjectNumber;
    private ImageView imgTrainingSoon;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private TableColumn<Project, Integer> idColumn;
    @FXML
    private TableColumn<Project, String> nameColumn;
    @FXML
    private TableColumn<Project, Date> startDateColumn;
    @FXML
    private TableColumn<Project, Date> expectedEndDateColumn;
    @FXML
    private TableColumn<Project, Date> endDateColumn;
    @FXML
    private TableView<Project> myProjects;

    private ObservableList<Project> data;
    private List<Project> projects;
    private int idSelectedProject;
    /**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		idSelectedProject = 0;
		Image img = new Image("assets/home.png");
		imgBtHome.setImage(img);
		img = new Image("assets/projectmenu.png");
		imgbtProjects.setImage(img);
		img = new Image("assets/lecture.png");
		imgBtTrainig.setImage(img);
		img = new Image("assets/luggage.png");
		imgBtVac.setImage(img);
		img = new Image("assets/calendar.png");
		imgBtAbsences.setImage(img);
		img = new Image("assets/users.png");
		imgBtProfile.setImage(img);
		img = new Image("assets/chatting.png");
		imgBtCommunication.setImage(img);
		img = new Image("assets/salary.png");
		imgBtSalaire.setImage(img);
		img = new Image("assets/logout.png");
		imgBtlogout.setImage(img);
		img = new Image("assets/hi.png");
		imgHi.setImage(img);
		img = new Image("assets/hrhead.jpg");
		headimg.setImage(img);
		img = new Image("assets/M2A.png");
		logoImg.setImage(img);
		Context context;
		myProjects.setItems(null);
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		startDateColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfBeginning"));
		endDateColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfEnd"));
		expectedEndDateColumn.setCellValueFactory(new PropertyValueFactory<>("expectedDateEnd"));
		try {

			context = new InitialContext();
			ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context
					.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
							+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
			projects = projectServiceRemote.getProjectsByEmployee(HomeController.employeeConnected.getId());
			if (projects != null) {
				data = FXCollections.observableList(projects);
				myProjects.setItems(data);
			}
		} catch (NamingException e) {

		}
	}

	public void hometInterface() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
		Parent root = (Parent) loader.load();
		HomeController homeController = new HomeController();
		homeController = loader.getController();
		homeController.returnId(HomeController.employeeConnected);
		Scene newScene = new Scene(root);
		Stage newStage = new Stage();
		newStage.setScene(newScene);
		newStage.show();
		Stage stage = (Stage) myProjects.getScene().getWindow();
		// do what you have to do
		stage.close();

	}
    
    @FXML
    void displayAllProjects(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		projects = projectServiceRemote.getProjectsByEmployee(HomeController.employeeConnected.getId());
		data= FXCollections.observableList(projects);
		myProjects.setItems(data);
    }

    

    @FXML
    void displayUnifinishedProjects(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		projects = projectServiceRemote.getProjectsByEmployee(HomeController.employeeConnected.getId(), Status.IN_PROGRESS);
		data= FXCollections.observableList(projects);
		myProjects.setItems(data);
    }

    @FXML
    void displayFinishedProjects(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		projects = projectServiceRemote.getProjectsByEmployee(HomeController.employeeConnected.getId(),Status.DONE);
		data= FXCollections.observableList(projects);
		myProjects.setItems(data);

    }

    @FXML
    private void getIdSelectedProject(MouseEvent event) {
    	idSelectedProject = myProjects.getSelectionModel().getSelectedItem().getId();
    }
    
    
    @FXML
    void seeMyTasks(ActionEvent event) throws NamingException, IOException {
    	if(idSelectedProject==0){
    		Alert alert = new Alert(Alert.AlertType.ERROR,"Please choose a project");
    		alert.showAndWait();
    	} else{
    		Context context = new InitialContext();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("MyTasks.fxml"));
			Parent root = (Parent) loader.load();
			MyTasksController myTasksController = new MyTasksController();
			myTasksController = loader.getController();
			myTasksController.getTheProjectFromPreviousScreen(idSelectedProject);
			Scene newScene = new Scene(root);
			Stage newStage = new Stage();
			newStage.setScene(newScene);
			newStage.show();
    	}
    }
    
    
}
