/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import javafx.stage.Stage;
import javafx.util.Callback;
import tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.SendMail;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Justification;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class AbsencesHrController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    
    
    private ObservableList<Absence> data=FXCollections.observableArrayList();
    @FXML
    private JFXListView<Absence> absenceHrList;
    @FXML
    private Text csvFilePath;
    @FXML
    private JFXButton btnUpload;
    @FXML
    private JFXButton pdfGenerator;
    @FXML
    private ImageView imgPDF;
	Context context;
	AbsenceServiceRemote absenceServiceRemote;
	EmployeeServiceRemote employeeServiceRemote;
	

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	System.out.print("************"+HomeHrManagerController.employeeConnected.getName());
    	List<Absence> absences=new ArrayList<>();
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
           img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
          img = new Image("assets/hi.png");
           imgPDF.setImage(new Image("assets/pdf.png"));
         imgHi.setImage(img);
        HBox hbox = new HBox();
       data.clear();
       try {
    	  
		context = new InitialContext();
		absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");
 		absences= absenceServiceRemote.findAll();
 		Collections.reverse(absences);
          for (Absence absence : absences) {
			data.add(absence);
		}
	} catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     		
              
               absenceHrList.setCellFactory(new Callback<ListView<Absence>, ListCell<Absence>>(){
           @Override
           public ListCell<Absence> call(ListView<Absence> args0) {
               //Image img= new Image("assets/hi.png");
               ImageView imageview=new ImageView();
               Label label=new Label("");
               JFXButton buttAnnuler=new JFXButton();
               //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
              // buttAnnuler.setStyle("-fx-background-image: url('/assets/hi.png');-fx-font-size: 1em; ");
               buttAnnuler.setMinHeight(30);
               buttAnnuler.setMinWidth(30);
               JFXButton buttProfil=new JFXButton();
              // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
              buttProfil.setMinHeight(30);
               buttProfil.setMinWidth(32);
               JFXButton buttjustify=new JFXButton();
               buttjustify.setMinHeight(30);
               buttjustify.setMinWidth(30);
               buttjustify.setStyle("-fx-background-image: url('/assets/confirm.png');");
               buttjustify.setAlignment(Pos.CENTER_RIGHT);
               HBox hbox = new HBox();
               hbox.getChildren().addAll(imageview, label,buttAnnuler,buttProfil,buttjustify);
                 
               
               
               ListCell<Absence> cell;
               int i = 0 ;
               cell = new ListCell<Absence>(){
                   
                   @Override
                   protected void updateItem(Absence r ,boolean b){
                       
                       super.updateItem(r,b);
                         
                         if(r != null){
                        	  
                             	 if(r.getJustified()==Justification.NOT_JUSTIFIED){
                             		  hbox.setStyle("-fx-background-color:#ff9999;");
                           	          
                             	 }else{
                             		buttjustify.setDisable(true);
                           		    hbox.setStyle("-fx-background-color:#bef67a;");
                           	   }
                        	 label.setText(r.getEmployee().getName() +" "+r.getEmployee().getFamilyName() +" From:"+r.getFromDate() + " To:"+r.getToDate() );
                           setGraphic(hbox);
                           
                           buttjustify.setOnAction((event) -> {
                               
                               
                               System.out.print("confirm");
                  Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
  alert.setTitle("justify Absence");
  alert.setHeaderText("you will justify  this  demand , one email will be send to the employee ");
  alert.setContentText("Are you ok with this?");

  Optional<ButtonType> result = alert.showAndWait();

  AbsenceServiceRemote absenceServiceRemote1;

  if (result.get() == ButtonType.OK){
  
//  Context context1;
//
//  context1 = new InitialContext();
//  absenceServiceRemote1= (AbsenceServiceRemote)context1.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");
  Absence absence =absenceServiceRemote.find(r.getId());
  System.out.println("id Absence = *****"+ absence.getId() +"********");
  absence.setJustified(Justification.JUSTIFIED);
  absenceServiceRemote.mergeAbsence(absence);
  SendMail mail =new SendMail();
  mail.sendmail("m2adashbored@gmail.com","kadhem94@gmail.com", "hrbis2018", "your absence of"+ r.getFromDate() +" to "+r.getToDate() +" is justified "+"\n \nM2A HR dashbored ");
 
  
  
  } else {
  // ... user chose CANCEL or closed the dialog
  }
                      
  initialize(url,rb);
             });  
                         
                         
                         
                         }
                         
                         
                         
                      
                             
                    
                           
                           
                       
                    
                       }
                       
                     
                
               };
               
               
            return cell;    
           }
            
          
       
                });
        absenceHrList.setItems(data);
        
       
        
        
     
           
      
    } 
    
     @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }  
    @FXML
    public void csvUpload() throws IOException, NamingException{ 
    	//Upload CSV in application
          Stage stage=new Stage();
          FileChooser fileChooser = new FileChooser();
     File file = fileChooser.showOpenDialog(stage);
                System.out.println(file+"");
                String str =file+"";
                 csvFilePath.setText(file.toString());
                 //CSV to persisit
                 //String csvFile = "//home/kadhem/Desktop/Esprit2/PDEV/test.csv";
                 String line = "";
                 String cvsSplitBy = ",";
                 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
         				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
                 List< Absence> absences = new ArrayList<Absence>();
                 Employee emp;
                 Absence absence;
                 try (BufferedReader br = new BufferedReader(new FileReader(file.toString()))) {

                     while ((line = br.readLine()) != null) {

                         // use comma as separator
                         String[] absenceDetail = line.split(cvsSplitBy);
                         System.out.print("absence emp **************** "+absenceDetail[0]);
                         emp= employeeServiceRemote.find(Integer.parseInt(absenceDetail[0]));
                         
                          absence= new Absence(Date.valueOf(absenceDetail[1]),Date.valueOf(absenceDetail[2]),Justification.NOT_JUSTIFIED,emp);
                         //System.out.println("Absence [matricule="+ absenceDetail[0]+",absfrom= " + absenceDetail[1] + " , absto=" + absenceDetail[2] + ",justified="+absenceDetail[3]+"]");
                         absences.add(absence);
                         absenceServiceRemote.save(absence);
                     }
                     System.out.println(absences.toString());
                     //  absenceServiceRemote.insertAbsence(absences);
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
                 imgBtHome.getScene().getWindow().hide();
                 FXMLLoader loader = new FXMLLoader(getClass().getResource("absencesHr.fxml"));
                 Parent root = (Parent) loader.load();
                 
                 Scene newScene = new Scene(root);
                 Stage newStage = new Stage();
                 newStage.setScene(newScene);
                 newStage.show();

             }
    

    
      @FXML 
    public void pdfGenerator() throws IOException, NamingException{ 
    	   employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
    				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
    	  List<Absence>absences;
    	   absences= absenceServiceRemote.findAll();
    		Collections.reverse(absences);
             
    	  Document document = new Document();
          try{
              PdfWriter.getInstance(document, new FileOutputStream("/home/kadhem/absenceList"+java.sql.Date.valueOf(java.time.LocalDate.now())+".pdf"));
              document.open();
              Font code = new Font(FontFamily.COURIER, 12, Font.NORMAL, BaseColor.GREEN);
              Paragraph p1 = new Paragraph("Absence before "+java.sql.Date.valueOf(java.time.LocalDate.now()),code);
              
              document.addHeader("Absence","Before"+java.sql.Date.valueOf(java.time.LocalDate.now()));
              document.addTitle("Absence of All Employees");
              com.itextpdf.text.Image img = com.itextpdf.text.Image.getInstance("/home/kadhem/git/esprit1718b2hrboardbis/esprit1718b2hrboardbis/esprit1718b2hrboardbis-client/src/main/java/assets/M2A.png");
              img.setAbsolutePosition(40f, 10f);
                 document.add(img);
             document.add(p1);
           
              for (Absence absence : absences) {
            	  Paragraph p2 = new Paragraph("Name :"+absence.getEmployee().getName()+" FamilyName: "+absence.getEmployee().getFamilyName()+" From:"+ absence.getFromDate()+ " To: "+absence.getToDate() +" Jstified: "+absence.getJustified());	
            	  document.add(p2);
              }
              
  				
            
          }
          catch(Exception e){
              System.out.println(e);
          }
          document.close();
      }
    
     
      @FXML
      public void leavestInterface() throws IOException{
     	 
     	 
     	 
     	 FXMLLoader loader = new FXMLLoader(getClass().getResource("VacancesHr.fxml"));
          Parent root = (Parent) loader.load();
          
          Scene newScene = new Scene(root);
          Stage newStage = new Stage();
          newStage.setScene(newScene);
          newStage.show();
          Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
          // do what you have to do
          stage.close();
     
      }
    
    
}
