/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import java.awt.ImageCapabilities;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TypeContract;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class AddJobOfferController implements Initializable {

    @FXML
    private ImageView headimg;
    @FXML
    private ImageView ImageJnab;
    @FXML
    private ImageView logoImg;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private JFXTextField JobOfferPost;
    @FXML
    private JFXTextField JobOfferSalary;
    @FXML
    private JFXComboBox<String> JobOfferDepartment;
    @FXML
    private JFXComboBox<String> JobOfferContract;
    @FXML
    private JFXComboBox<String> JobOfferHours;
    @FXML
    private JFXTextArea JobOfferDescription;
    @FXML
    private JFXButton AddJobOffer;
    @FXML
    private ImageView AjoutImg;
    @FXML
    private JFXButton BackInterfaceRecrutmentHr;
    @FXML
    private ImageView BAckImg;
    ObservableList<String> listeOfContracts=FXCollections.observableArrayList();
    ObservableList<String> listeOfJobHours=FXCollections.observableArrayList("Full time","part time","patiam timr");

    ObservableList<String> ListOfDepartments=FXCollections.observableArrayList();
    @FXML
    private JFXButton AddSkillsbuttom;

    /**
     * Initializes the controller class.
     */
    public ObservableList<String> returnNameDepartment()
    {
        Context context;
  		try {
  			context = new InitialContext();
  			//Integer idDepartment=0;
  			DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
  	 		List<Department> DepartmentList= new ArrayList<Department>();
  	 		DepartmentList=departmentServicesRemote.findAll();
  	 		//ObservableList<Integer> IdOfDepartments=FXCollections.observableArrayList();
  	 		List<Integer> idDepartment= new ArrayList<Integer>();
  	 		 for (Department department : DepartmentList) {
  	 			System.out.println(department.getName());
  	 			ListOfDepartments.add(department.getName());
  	 			idDepartment.add(department.getId()); 
  	 		}
  	  		idDepartment.forEach(System.out::println);

  		} catch (NamingException e) {
  		
  		}
  		return ListOfDepartments;
    }
    
    public List<Integer> returnListIdDepartment()
    {
        Context context;
	 		List<Integer> idDepartment= new ArrayList<Integer>();

      		try {
      			context = new InitialContext();
      			//Integer idDepartment=0;
      			DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
      	 		List<Department> DepartmentList= new ArrayList<Department>();
      	 		DepartmentList=departmentServicesRemote.findAll();
      	 		//ObservableList<Integer> IdOfDepartments=FXCollections.observableArrayList();
      	 		 for (Department department : DepartmentList) {
      	 			System.out.println(department.getName());
      	 			ListOfDepartments.add(department.getName());
      	 			idDepartment.add(department.getId()); 
      	      		return idDepartment;

      	 		}
      	  		idDepartment.forEach(System.out::println);

      		} catch (NamingException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
      		return(idDepartment);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Image img = new Image("assets/socity.jpg");
    	ImageJnab.setImage(img);
    	img= new Image("assets/save.png");
    	AjoutImg.setImage(img);
    	img= new Image("assets/back.png");
    	BAckImg.setImage(img);
    	 JobOfferHours.setItems(listeOfJobHours);
    	   
 		 ObservableList<String> ListOfDepartments=returnNameDepartment();
 		 for (String string : ListOfDepartments) {
			System.out.println(string)	;	}

	JobOfferDepartment.setItems(ListOfDepartments);
    
		TypeContract[] contractList;
		contractList=TypeContract.values();
		for (TypeContract typeContract : contractList) {
			System.out.println(typeContract);
			String contact=String.valueOf(typeContract);
			listeOfContracts.add(contact);
			System.out.println(contact);
			
		}
		listeOfContracts.forEach(System.out::println);

	JobOfferContract.setItems(listeOfContracts);	
       
    }    
    private static void GenererAlerte(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(msg);
        alert.showAndWait(); 
    }

    @FXML
    private void SaveJobOfferJobOffer(ActionEvent event) throws NamingException, IOException {
    	
    	//java.sql.Date gettedDatePickerDate = java.sql.Date.valueOf(.getValue());
    	//java.sql.Date datenow = java.sql.Date.valueOf(LocalDate.now());
  
    	String msg="";
    	if (JobOfferDepartment.getValue()==null) {
    		msg="Field Department is required !";
    	    GenererAlerte(msg);
    	}

    	else if (JobOfferSalary.getText().equals(null)) {
    		msg="You have to affect Salary to this Employee !";
    	    GenererAlerte(msg);
    	}
    	else if (JobOfferContract.getValue()==null) {
			msg="Field Contract is invalid !";
		    GenererAlerte(msg);
    	}
    	else if (JobOfferDescription.getText()==null) {
			msg="Field Description is invalid !";
		    GenererAlerte(msg);
    	}
    	else if (JobOfferHours.getValue()==null) {
			msg="Field Hours is invalid !";
		    GenererAlerte(msg);
    	}
    	else if (JobOfferPost.getText()==null) {
			msg="Field Post is invalid !";
		    GenererAlerte(msg);
    	}
    
    	
    	else
    	{
    	JobOffer jobOffer= new JobOffer();
		jobOffer.setDescription(JobOfferDescription.getText());
		jobOffer.setName(JobOfferPost.getText());
		jobOffer.setSalary(Float.parseFloat(JobOfferSalary.getText()));
		System.out.println(jobOffer.getSalary());
		TypeContract type= TypeContract.valueOf((JobOfferContract.getValue()));

		jobOffer.setTypeContrat(type);
		 System.out.println(jobOffer.getTypeContrat());
		jobOffer.setTimeOfJob(JobOfferHours.getValue());	
		Integer rowDepartmentSelected=0;
		jobOffer.setDateOfPublishing(java.sql.Date.valueOf(java.time.LocalDate.now()));		   
		List<String> ListDepartment1= new ArrayList<String>();
		String department = JobOfferDepartment.getValue();
		ListDepartment1=JobOfferDepartment.getItems();
		int i=1;
		for (String string : ListDepartment1) {
		
			if(string!=department)
			{
				i++;
			}
			System.out.println(i);
		}
		List<Integer> idDepartment= new ArrayList<Integer>();
		idDepartment=returnListIdDepartment();
        Context context = null;
	 		
        Department department2=new Department();
      	
      			System.out.println(i);
      			context = new InitialContext();
      			//Integer idDepartment=0;
      			
      			DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
      	 		department2=departmentServicesRemote.find(i);
      	 		jobOffer.setDepartment(department2);
      	
      	 		System.out.println("*************"+jobOffer.getDescription());
    		
 	    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AssignSkillsToJobOffer.fxml"));
 	       Parent root = (Parent) loader.load();
 	      AssignSkillsToJobOffer assignSkillsToJobOffer= new AssignSkillsToJobOffer();
 		 assignSkillsToJobOffer=loader.getController();
 		 assignSkillsToJobOffer.init();
 		assignSkillsToJobOffer.recupjoboffre(jobOffer.getName(),String.valueOf(jobOffer.getDepartment().getId()),String.valueOf(jobOffer.getTypeContrat()),jobOffer.getTimeOfJob(),String.valueOf(jobOffer.getSalary()),jobOffer.getDescription());
 		System.out.println("*************"+jobOffer.getDescription());
 		 System.out.println(assignSkillsToJobOffer.getJobOffer());
 		 Stage stage = (Stage) AddJobOffer.getScene().getWindow();
         // do what you have to do
         stage.close();
 	     Scene newScene = new Scene(root);
 	     Stage newStage = new Stage();
 	     newStage.setScene(newScene);
 	   
 	     newStage.show();
 	     
 	     
 		/*Alert alert = new Alert(AlertType.WARNING);
   	  alert.setTitle("information");
         alert.setHeaderText("Success Operation");*/
         
    }
    }
    @FXML
    private void BackRecrutmentsInterface(ActionEvent event) {
    	  Stage stage = (Stage) AddJobOffer.getScene().getWindow();
	         // do what you have to do
	         stage.close();
    }
    @FXML
    private void AddSkill(ActionEvent event) throws IOException { 
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AssignSkillsToJobOffer.fxml"));
    Parent root = (Parent) loader.load();
  Scene newScene = new Scene(root);
  Stage newStage = new Stage();
  newStage.setScene(newScene);
  newStage.show();
    }
    
}
