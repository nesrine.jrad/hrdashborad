package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.seance;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;

public class TestDeleteSeance {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		
		SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");
		seanceServiceRemote.delete(seanceServiceRemote.find(1));
		////////////////////////////////////////////////to check
	}
}
