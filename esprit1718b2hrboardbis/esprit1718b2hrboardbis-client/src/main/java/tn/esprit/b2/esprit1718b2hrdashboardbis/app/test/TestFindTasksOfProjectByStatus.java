package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote;

public class TestFindTasksOfProjectByStatus {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		List<Task> tasksToDo = taskServiceRemote.getTasksByEmployeeAndStatusTask(2, StatusTask.ToDO);
		System.out.println(tasksToDo.size());
		

	}

}
