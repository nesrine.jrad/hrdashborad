/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class SendJustificationController implements Initializable {

    @FXML
    private JFXButton explorerButton;
    @FXML
    private Text path;
    @FXML
    private ImageView imgJustification;
    @FXML
    private ImageView emailsend;
    @FXML
    private ImageView imgm2a;
    String datefrom;
    String dateto;

    public String getDatefrom() {
		return datefrom;
	}

	public void setDatefrom(String datefrom) {
		this.datefrom = datefrom;
	}

	public String getDateto() {
		return dateto;
	}

	public void setDateto(String dateto) {
		this.dateto = dateto;
	}

	/**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        emailsend.setImage(new Image("assets/email.png"));
        imgm2a.setImage(new Image("assets/M2A.png"));
    } 
    
    @FXML
    public void chooserInterface() throws IOException { 
          Stage stage=new Stage();
          FileChooser fileChooser = new FileChooser();
              //  FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
               // fileChooser.getExtensionFilters().add(extFilter);
                
                File file = fileChooser.showOpenDialog(stage);
                System.out.println(file+"");
                String str =file+"";
                 path.setText(file.toString());
                 Image img = new Image(file.toURI().toURL().toString());
                 imgJustification.setImage(img);
               
                
}
    
     @FXML
    public void sendemail() throws IOException { 
        // Create the custom dialog.
Dialog<Pair<String, String>> dialog = new Dialog<>();
dialog.setTitle("Login Dialog");
dialog.setHeaderText("Look, a Custom Login Dialog");

// Set the icon (must be included in the project).
dialog.setGraphic(new ImageView(new Image("assets/M2A.png")));

// Set the button types.
ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

// Create the username and password labels and fields.
GridPane grid = new GridPane();
grid.setHgap(10);
grid.setVgap(10);
//grid.setPadding(new Insets(20, 150, 10, 10));

TextField username = new TextField();
username.setPromptText("Username");
PasswordField passwordtxt = new PasswordField();
passwordtxt.setPromptText("Password");

grid.add(new Label("Username:"), 0, 0);
grid.add(username, 1, 0);
grid.add(new Label("Password:"), 0, 1);
grid.add(passwordtxt, 1, 1);

// Enable/Disable login button depending on whether a username was entered.
Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
loginButton.setDisable(true);

// Do some validation (using the Java 8 lambda syntax).
username.textProperty().addListener((observable, oldValue, newValue) -> {
    loginButton.setDisable(newValue.trim().isEmpty());
});

dialog.getDialogPane().setContent(grid);

// Request focus on the username field by default.
Platform.runLater(() -> username.requestFocus());

// Convert the result to a username-password-pair when the login button is clicked.
dialog.setResultConverter(dialogButton -> {
    if (dialogButton == loginButtonType) {
        return new Pair<>(username.getText(), passwordtxt.getText());
    }
    return null;
});

Optional<Pair<String, String>> result = dialog.showAndWait();

result.ifPresent(usernamePassword -> {
    System.out.println("Username=" + usernamePassword.getKey() + ", Password=" + usernamePassword.getValue());


         String host = "smtp.gmail.com";
        String port = "587";
        String mailFrom = usernamePassword.getKey();
        String password = usernamePassword.getValue();

        // message info
        String mailTo = "m2adashbored@gmail.com";
        String subject = "Justify ABsence of " +datefrom +" to "+ dateto;
        StringBuffer body
            = new StringBuffer("<html>Justify Absence of "+datefrom +" to "+ dateto + ".<br>");
        body.append("Image:<br>");
        body.append("<img src=\"cid:image1\" width=\"30%\" height=\"30%\" /><br>");
        body.append("<br>");
        body.append("<img src=\"cid:image2\" width=\"15%\" height=\"15%\" /><br>");
        body.append("End of message.");
        body.append("</html>");
 
        // inline images
        Map<String, String> inlineImages = new HashMap<String, String>();
         String photo = path.getText();
        inlineImages.put("image1", photo);
       
 
        try {
        	tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.EmbeddedImageEmailUtil.send(host, port, mailFrom, password, mailTo,
                subject, body.toString(), inlineImages);
            System.out.println("Email sent.");
            Alert alert = new Alert(AlertType.INFORMATION);
alert.setTitle("Email sent");
alert.setHeaderText(null);
alert.setContentText("your email was successfully sent!");

alert.showAndWait();
        } catch (Exception ex) {
            System.out.println("Could not send email.");
            Alert alert = new Alert(AlertType.INFORMATION);
alert.setTitle("Problems Sending Email  ");
alert.setHeaderText(null);
alert.setContentText("Verify your login and password !");

            ex.printStackTrace();
        } });

 
    }
    
         
               
                
}
    
    
    
    

