/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Status;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class ProjetManagerController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    private ImageView imgNews;
    private ImageView imgVacationRest;
    private ImageView imgProjectNumber;
    private ImageView imgTrainingSoon;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private TableColumn<Project, Integer> idColumn;
    @FXML
    private TableColumn<Project, String> nameColumn;
    @FXML
    private TableColumn<Project, Date> startDateColumn;
    @FXML
    private TableColumn<Project, Date> expectedEndDateColumn;
    @FXML
    private TableColumn<Project, Date> endDateColumn;
    @FXML
    private ImageView AddProjectImg;
    @FXML
    private TableView<Project> projetcsTable;
    
    private ObservableList<Project> data;
    @FXML
    private ImageView deleteImg;
    @FXML
    private ImageView updateImg;
    @FXML
    private JFXButton addTasksBtn;
    
    private int idSelectedProject;

    @FXML
    private JFXButton seeProgressBtn;
    @FXML
    private JFXRadioButton allRadioButton;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	idSelectedProject = 0;
    	allRadioButton.setSelected(true);
		Image img = new Image("assets/home.png");
		imgBtHome.setImage(img);
		img = new Image("assets/projectmenu.png");
		imgbtProjects.setImage(img);
		img = new Image("assets/lecture.png");
		imgBtTrainig.setImage(img);
		img = new Image("assets/luggage.png");
		imgBtVac.setImage(img);
		img = new Image("assets/calendar.png");
		imgBtAbsences.setImage(img);
		img = new Image("assets/users.png");
		imgBtProfile.setImage(img);
		img = new Image("assets/chatting.png");
		imgBtCommunication.setImage(img);
		img = new Image("assets/salary.png");
		imgBtSalaire.setImage(img);
		img = new Image("assets/logout.png");
		imgBtlogout.setImage(img);
		img = new Image("assets/hi.png");
		imgHi.setImage(img);
		img = new Image("assets/hrhead.jpg");
		headimg.setImage(img);
		img = new Image("assets/M2A.png");
		logoImg.setImage(img);
		img = new Image("assets/Add.png");
		AddProjectImg.setImage(img);
		img = new Image("assets/trash.png");
		deleteImg.setImage(img);
		Context context;
		projetcsTable.setItems(null);
try {
			
			context = new InitialContext();
			ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
	 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
			List<Project> projects = projectServiceRemote.findAll();
			data = FXCollections.observableList(projects);
            idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
            nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
            startDateColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfBeginning"));
			endDateColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfEnd"));
			expectedEndDateColumn.setCellValueFactory(new PropertyValueFactory<>("expectedDateEnd"));
			projetcsTable.setItems(data);
			
	} catch (NamingException e) {
			
		}
         
       
    }    
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }

    @FXML
    private void addProject(MouseEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("AddProject.fxml"));
        Parent root = (Parent) loader.load();
        
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
   
    }

    @FXML
    private void getIdSelectedProject(MouseEvent event) {
    	idSelectedProject = projetcsTable.getSelectionModel().getSelectedItem().getId();
    	System.out.println(idSelectedProject);
    }
    
    @FXML
    private void addTasks(ActionEvent event) throws IOException, NamingException{
		if (idSelectedProject == 0) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Please choose a project to add the tasks");
			alert.showAndWait();
		} else {
			Context context = new InitialContext();
			ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context
					.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
							+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("AddTasksToProjects.fxml"));
			Parent root = (Parent) loader.load();
			AddTasksToProjectsController addTasksToProjectsController = new AddTasksToProjectsController();
			addTasksToProjectsController = loader.getController();
			addTasksToProjectsController.getTheProjectFromPreviousScreen(projectServiceRemote.find(idSelectedProject));
			Scene newScene = new Scene(root);
			Stage newStage = new Stage();
			newStage.setScene(newScene);
			newStage.show();
		}
    	
    }
    @FXML
    private void deleteProject(MouseEvent event) throws IOException, NamingException{
		Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context
				.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
						+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		if (idSelectedProject == 0) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Please select a project to delete");
			alert.setTitle("Error in selection");
			alert.showAndWait();
		} else {
			Project entity = projectServiceRemote.find(idSelectedProject);
			List<Task> tasksDoing = projectServiceRemote.getListTasksOfProjectByStatus(idSelectedProject,
					StatusTask.DOING);
			List<Task> tasksToDo = projectServiceRemote.getListTasksOfProjectByStatus(idSelectedProject,
					StatusTask.ToDO);
			if (!(tasksDoing.isEmpty()) || !(tasksToDo.isEmpty())) {
				Alert alert = new Alert(Alert.AlertType.ERROR, "You still have some remaining unfinished tasks!");
				alert.showAndWait();
			} else {
				projectServiceRemote.delete(entity);

				List<Project> projects = projectServiceRemote.findAll();
				data = FXCollections.observableList(projects);
				projetcsTable.setItems(data);
			}
		}
    	
    }
    
    @FXML
    void displayAllProjects(ActionEvent event) throws NamingException {
    	idSelectedProject = 0;
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		List<Project> projects = projectServiceRemote.findAll();
		data= FXCollections.observableList(projects);
		projetcsTable.setItems(data);
    }

    

    @FXML
    void displayUnifinishedProjects(ActionEvent event) throws NamingException {
    	idSelectedProject = 0;
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		List<Project> projects = projectServiceRemote.getProjectsByStatus(Status.IN_PROGRESS);
		data= FXCollections.observableList(projects);
		projetcsTable.setItems(data);
    }

    @FXML
    void displayFinishedProjects(ActionEvent event) throws NamingException {
    	idSelectedProject = 0;
    	Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		List<Project> projects = projectServiceRemote.getProjectsByStatus(Status.DONE);
		data= FXCollections.observableList(projects);
		projetcsTable.setItems(data);

    }
    
    @FXML
    void seeProgressProject(ActionEvent event) throws IOException {
		if (idSelectedProject != 0) {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SeeProgressProjectFXML.fxml"));
			Parent root = (Parent) loader.load();
			SeeProgressProjectFXMLController seeProgressProjectFXMLController = new SeeProgressProjectFXMLController();
			seeProgressProjectFXMLController = loader.getController();
			seeProgressProjectFXMLController.setIdProjectFromLastScreen(idSelectedProject);
			Scene newScene = new Scene(root);
			Stage newStage = new Stage();
			newStage.setScene(newScene);
			newStage.show();
		} else {
			Alert alert = new Alert(Alert.AlertType.ERROR,"Please choose a project to see its progress");
			alert.showAndWait();
		}

    }
    
    @FXML
    void seeMyTasks(ActionEvent event) throws NamingException {
    	if(idSelectedProject == 0) {
    		Alert alert = new Alert(Alert.AlertType.ERROR,"Please choose a project to see your tasks");
    		alert.showAndWait();
    	}
    	else{
    		
    	}
    }
    
}
