package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.SendMail;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class AddSessionController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private TableView<Employee> table1;
    @FXML
    private TableColumn<Employee, Integer> id;
    @FXML
    private TableColumn<Employee, String> name;
    @FXML
    private TableColumn<Employee, String> familyName;
    @FXML
    private TableView<Employee> table2;
    @FXML
    private TableColumn<Employee, Integer> id2;
    @FXML
    private TableColumn<Employee, String> name2;
    @FXML
    private TableColumn<Employee, String> familyName2;
    
    private ObservableList<Employee> data;
    
    private ObservableList<Employee> data2;
    @FXML
    private DatePicker date;
    @FXML
    private ComboBox<Training> comboTraining;
    @FXML
    private ComboBox<Trainer> comboTrainer;
    @FXML
    private ImageView logo_delete;
    @FXML
    private  ImageView logo_add;
    
    public List<Employee>employeesSelected=new ArrayList<Employee>();
    @FXML
    private ComboBox comboSalle;
	ObservableList <String> type_comboSalle=FXCollections.observableArrayList("Salle S1","Salle S2","Salle des réunions");

    
 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
    	comboSalle.setValue("Salle 1");
    	comboSalle.setItems(type_comboSalle);
        
    	Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
          img = new Image("assets/newspape.png");
          logo_delete.setImage(new Image("assets/logo_delete.png"));
          logo_add.setImage(new Image("assets/logo_add.png"));
///////////////////////////////////////////
try {
Context context = new InitialContext();
EmployeeServiceRemote employeeServiceRemote= (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");

List<Employee> employees= new ArrayList<Employee>();
employees=employeeServiceRemote.findAll();
System.out.println(employees.size());
///////////////////////////////////////////
///////////////////////////////////////////
table1.setEditable(false);
table1.setItems(null);
data = FXCollections.observableArrayList(employees);
id.setCellValueFactory(new PropertyValueFactory<>("id"));
name.setCellValueFactory(new PropertyValueFactory<>("name"));
familyName.setCellValueFactory(new PropertyValueFactory<>("familyName"));
table1.setItems(data);
//for(Employee e:employees){
//e.setId(e.getId());
//e.setName(e.getName());
//e.setFamilyName(e.getFamilyName());
//data.add(e);
//e=null;
//}
//table1.setItems(data); 

comboTrainer.getItems().addAll(populatecomboTrainer());
comboTraining.getItems().addAll(populatecomboTraining());
} catch (NamingException e) {
e.printStackTrace();
}
date.setValue(LocalDate.now());
/////////////////////////////////////////// 
}   


  @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
	  	  
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }        



    @FXML
    private void update1Next(ActionEvent event) throws NamingException, IOException {
    	java.sql.Date datenow = java.sql.Date.valueOf(LocalDate.now());
    	Context context = new InitialContext();
    	SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");
    	String msg="";
    	if (comboTrainer.getValue()==null) {
    		msg="Field Trainer is required !";
    	    GenererAlerte(msg);
    	}
    	else if (comboTraining.getValue()==null) {
    		msg="Field Training is required !";
    	    GenererAlerte(msg);
    	}
    	else if (comboSalle.getValue()==null) {
    		msg="Field Place is required !";
    	    GenererAlerte(msg);
    	}
    	else if (employeesSelected.size()==0) {
    		msg="You have to affect employees to this training session !";
    	    GenererAlerte(msg);
    	}
    	else if (Date.valueOf(date.getValue())==null || Date.valueOf(date.getValue()).before(datenow)) {
			msg="Field Date is invalid !";
		    GenererAlerte(msg);
    	}
    	else {
    		String objectMail;
		for (Employee c1 : employeesSelected) {
			SendMail mail=new SendMail();
			objectMail="Vous avez été affecté à la formation "+
			comboTraining.getValue().getTitle()+
			", qui sera animé par notre formateur "+
			comboTrainer.getValue().getName()+" "+comboTrainer.getValue().getFamilyName()+"."
			+"Date de formation : "+Date.valueOf(date.getValue())+"."
			+"Salle : "+comboSalle.getValue().toString()+".";
			
			mail.sendmail("houssem.hamila@gmail.com", c1.getEmail(), "ris2k01forlife", objectMail);
			Seance seance=new Seance(c1.getId(),
					comboTrainer.getValue().getId(),
					comboTraining.getValue().getId(),
					Date.valueOf(date.getValue()),
					comboSalle.getValue().toString());
			seanceServiceRemote.save(seance);
			
		}
		
		Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
		
		FXMLLoader loader=new FXMLLoader(getClass().getResource("SessionsManagement.fxml"));
	    Parent root=(Parent) loader.load();
		Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();}
    }

    @FXML
    private void ajouter(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
    	EmployeeServiceRemote employeeServiceRemote= (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		
		Employee employee;
		
		employee=employeeServiceRemote.find(getIdTrainerFromTable1());
		employeesSelected.add(employee);
		refreshTable2();
    }

    @FXML
    private void supprimer(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
    	EmployeeServiceRemote employeeServiceRemote= (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		
		Employee employee;
		
		employee=employeeServiceRemote.find(getIdEmployeeFromTable2());
		//employeesSelected.remove(employee);
		employeesSelected.clear();
		refreshTable2();
    }

    @FXML
    private void cancel(ActionEvent event) {
    	employeesSelected.clear();
    	refreshTable2();
    	comboSalle.setValue("Salle 1");
        date.setValue(LocalDate.now());
        comboTrainer.setValue(null);
        comboTraining.setValue(null);
    }
    
    private int getIdTrainerFromTable1() {
        table1.setOnMousePressed(new  EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                int id=table1.getSelectionModel().getSelectedItem().getId();
            }
            });
      return table1.getSelectionModel().getSelectedItem().getId();
}
	
	 private int getIdEmployeeFromTable2() {
	        table2.setOnMousePressed(new  EventHandler<MouseEvent>(){
	            @Override
	            public void handle(MouseEvent event) {
	                int id=table2.getSelectionModel().getSelectedItem().getId();
	            }
	            });
	      return table2.getSelectionModel().getSelectedItem().getId();
	}
	 
	 public void refreshTable2(){
		table2.setEditable(false);
		data2 = FXCollections.observableArrayList();
		for ( int i = 0; i<table2.getItems().size(); i++) 
		{
		table2.getItems().clear();
		}
		id2.setCellValueFactory(new PropertyValueFactory<>("id2"));
		name2.setCellValueFactory(new PropertyValueFactory<>("name2"));
		familyName2.setCellValueFactory(new PropertyValueFactory<>("familyName2"));
		
		for(Employee e:employeesSelected){
		e.setId2(e.getId2());
		e.setName2(e.getName2());
		e.setFamilyName2(e.getFamilyName2());
		data2.add(e);
		e=null;
		}
		table2.setItems(data2);
	}
	 
	 public static ObservableList<Training> populatecomboTraining() throws NamingException {
			Context context = new InitialContext();
			TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");

			List<Training> c = trainingServiceRemote.findAll();
			ObservableList<Training> items = FXCollections.observableArrayList();
			for (Training c1 : c) {
				items.add(c1);
			}
			return items;
		}
	 
	 public static ObservableList<Trainer> populatecomboTrainer() throws NamingException {
			Context context = new InitialContext();
			TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");

			List<Trainer> c = trainerServiceRemote.findAll();
			ObservableList<Trainer> items = FXCollections.observableArrayList();
			for (Trainer c1 : c) {
				items.add(c1);
			}
			return items;
		}
	 
	 private static void GenererAlerte(String msg){
	        Alert alert = new Alert(Alert.AlertType.ERROR);
	        alert.setTitle("Error");
	        alert.setContentText(msg);
	        alert.showAndWait(); 
	    }
	    
    
}
