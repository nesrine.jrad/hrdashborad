/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.jfoenix.controls.JFXButton;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Alert;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AssignEmployeesToProjectRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class AssignEmployeesToProjectFXMLController implements Initializable {

	//First Table View Where to display all the employees
	// The manager will choose from this list who will get assigned
	// To the actual project
    @FXML
    private TableView<Employee> DisplayEmloyees;
    //After choosing an emloyee. The chosen one will
    //Be displayed in the second table view
    //And when the manager confirms the chosen ones
    //They get affected to the project
    @FXML
    private TableView<Employee> EmployeesToAssignToThisProject;
    @FXML
    private ImageView removeImg;
    @FXML
    private TableColumn<Employee, Integer> idColumnEmployeeTab1;

    @FXML
    private TableColumn<Employee, Integer> idColumnEmployeeTab2;

    @FXML
    private TableColumn<Employee, String> lastNameColumnEmployeeTab2;

    @FXML
    private TableColumn<Employee, String> firstNameColumnEmployeeTab1;

    @FXML
    private TableColumn<Employee, String> lastNameColumnEmployeeTab1;

    @FXML
    private TableColumn<Employee, String> firstNameColumnEmployeeTab2;
    //Selected Employee from the first table
    private Employee employeeToAssign;
    //Selected Employee from  the second Table
    private Employee employeeToRemove;
    
    private List<Employee> employeesToDisplay;
    private List<Employee> employeesToAssign;
    //Data to display in the first Table
    private ObservableList<Employee> data1;
    //Data to display in the second table
    private ObservableList<Employee> data2;
    
    private EmployeeServiceRemote employeeServiceRemote;
    private ProjectServiceRemote projectServiceRemote;
    @FXML
    private Button addEmployeeBtn;
    @FXML
    private Button removeBtn;
    @FXML
    private Button confirmButton;
    
    //Id of last project To Assign EmployeesTo
    private int idProject;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	Context context;
    	
    	try {
    		context = new InitialContext();
			projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
					+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		
    	} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
		this.employeeToAssign = null;
		this.employeeToRemove = null;
    	DisplayEmloyees.setItems(null);
    	EmployeesToAssignToThisProject.setItems(null);
    	employeesToDisplay=new ArrayList<>();
    	employeesToAssign=new ArrayList<>();
    	
    	idColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("id"));
        firstNameColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("name"));
        lastNameColumnEmployeeTab1.setCellValueFactory(new PropertyValueFactory<>("familyName"));
        idColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("id"));
        firstNameColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("name"));
        lastNameColumnEmployeeTab2.setCellValueFactory(new PropertyValueFactory<>("familyName"));
		try {
			context = new InitialContext();
			employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");	
			employeesToDisplay = employeeServiceRemote.findAll();
			//Employees To Display in The first Table View
			data1 = FXCollections.observableList(employeesToDisplay);
			DisplayEmloyees.setItems(data1);
			if (employeesToDisplay.size()!=0) 
				employeeToAssign = employeesToDisplay.get(0);
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }    

    @FXML
    private void getSelectedIdDisplayedEmployee(MouseEvent event) {    	
    	this.employeeToAssign = DisplayEmloyees.getSelectionModel().getSelectedItem();
    	
    }

    @FXML
    private void getSelectedIdEmployeeToAssign(MouseEvent event) {
    	this.employeeToRemove = EmployeesToAssignToThisProject.getSelectionModel().getSelectedItem();
    }

    @FXML
    private void addEmployeeToAssign(ActionEvent event) {
    	employeesToAssign.add(employeeToAssign);
    	employeesToDisplay.remove(employeeToAssign);
    	data1= FXCollections.observableList(employeesToDisplay);
    	data2= FXCollections.observableList(employeesToAssign);
    	DisplayEmloyees.setItems(data1);
    	EmployeesToAssignToThisProject.setItems(data2);
    	employeeToAssign=null;
    	employeeToRemove=null;
    }

    @FXML
    private void removeEmployeeToAssign(ActionEvent event) {
    	employeesToDisplay.add(employeeToRemove);
    	employeesToAssign.remove(employeeToRemove);
    	data1= FXCollections.observableList(employeesToDisplay);
    	data2= FXCollections.observableList(employeesToAssign);
    	DisplayEmloyees.setItems(data1);
    	EmployeesToAssignToThisProject.setItems(data2);
    }


    @FXML
    private void confirmAssignEmployeesToProject(ActionEvent event) throws NamingException {
		Context context = new InitialContext();
		AssignEmployeesToProjectRemote assignEmployeesToProjectRemote = (AssignEmployeesToProjectRemote) context
				.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AssignEmployeesToProject!"
						+ "tn.esprit.b2.esprit1718b2hrboardbis.services.AssignEmployeesToProjectRemote");
		if (employeesToAssign.isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Please select the employees to assign to this project");
			alert.setTitle("Error");
			alert.showAndWait();
		} else {
			for (Employee employee : employeesToAssign) {
				assignEmployeesToProjectRemote.addEmployee(employee.getId());
			}
			assignEmployeesToProjectRemote.validateEmployees(idProject);
			Stage stage = (Stage) confirmButton.getScene().getWindow();
			stage.close();
		}
    }

	public int getIdProject() {
		return idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
    
    public void getTheIdProjectFromThePreviousScreen(int idProject){
    	this.idProject = idProject;
    }

   
    
}
