package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.sql.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote;

//
public class testAddleave {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		LeaveServiceRemote leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		Leave leave = new Leave("leave request ",Date.valueOf("2018-10-08"),Date.valueOf("2018-11-10"),"conges de maladie",false,employeeServiceRemote.find(1));
		leaveServiceRemote.save(leave);
	}

}
