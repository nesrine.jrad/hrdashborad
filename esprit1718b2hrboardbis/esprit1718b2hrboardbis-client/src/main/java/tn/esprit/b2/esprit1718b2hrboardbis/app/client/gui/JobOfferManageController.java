/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class JobOfferManageController implements Initializable {

    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXButton HomeManager;
    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView JObOfferImg;
    @FXML
    private JFXButton AddJobOffer;
    @FXML
    private JFXButton FindJobOffer;
    @FXML
    private JFXButton DeleteJobOffer;
    @FXML
    private JFXButton UpdateJobOffer;
    @FXML
    private JFXButton FindAllJobOffer;
    @FXML
    private JFXButton BackRecrutments;
    @FXML
    private ImageView AddJobOfferImg;
    @FXML
    private ImageView FindJobOfferImg;
    @FXML
    private ImageView DeleteJobOfferImg;
    @FXML
    private ImageView UpdateJobOfferImg;
    @FXML
    private ImageView FindAllJobOfferImg;
    @FXML
    private ImageView BAckImg;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	 Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/JobOffer.png");
         JObOfferImg.setImage(img);
         img = new Image("assets/save.png");
         AddJobOfferImg.setImage(img);
         img = new Image("assets/delete.jpg");
         DeleteJobOfferImg.setImage(img);
         img = new Image("assets/Find.png");
         FindJobOfferImg.setImage(img);
         img = new Image("assets/FindAll.png");
         FindAllJobOfferImg.setImage(img);
         img = new Image("assets/back.png");
         BAckImg.setImage(img);
         img = new Image("assets/Modify.jpg");
         UpdateJobOfferImg.setImage(img);
         
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
    }    

    @FXML
    private void HomeManagerInterface(ActionEvent event) throws IOException {

    }

    @FXML
    private void projectInterface(ActionEvent event) {
    }

    @FXML
    private void JobOfferManageInterface(MouseEvent event) {
    }

    @FXML
    private void AddJobOfferInterface(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddJobOffer.fxml"));
        Parent root = (Parent) loader.load();
        
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }

    @FXML
    private void FindJobOfferInterface(ActionEvent event) {
    }

    @FXML
    private void DeleteJobOfferInterface(ActionEvent event) {
    }

    @FXML
    private void UpdateJobOfferInterface(ActionEvent event) {
    }

    @FXML
    private void FindAllJobOfferInterface(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("FindAllJobOffers.fxml"));
        Parent root = (Parent) loader.load();
        
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }

    @FXML
    private void BackrecrutmentsInterface(ActionEvent event) throws IOException {
    	   FXMLLoader loader = new FXMLLoader(getClass().getResource("Recrutments.fxml"));
           Parent root = (Parent) loader.load();
           
           Scene newScene = new Scene(root);
           Stage newStage = new Stage();
           newStage.setScene(newScene);
           newStage.show();
    }
    
}
