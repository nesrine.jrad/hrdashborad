package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.recrutement;

import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

public class TestFindEmployeeByName {

	public static void main(String[] args) throws NamingException {
	Context context = new InitialContext();
	EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
	List<Employee> employees= new ArrayList<Employee>();
	employees=employeeServiceRemote.findEmployeeByName("Amine", "Alouini");
	employees.forEach(System.out::println);
	
	
	}
}
