package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.SeanceListView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.SeancePk;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;
import javafx.scene.control.ComboBox;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class SessionsManagementController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private TableView<SeanceListView> table;
    @FXML
    private TableColumn<SeanceListView,Date> date;
    @FXML
    private TableColumn<SeanceListView, String> training;
    @FXML
    private TableColumn<SeanceListView, String> trainer;
    @FXML
    private TableColumn<SeanceListView, String> employee;
    @FXML
    private TableColumn<SeanceListView, String> salle;
    @FXML
    private ComboBox<Training> trainingCombo;
    @FXML
    private ImageView logo_delete;
    @FXML
    private  ImageView logo_add;
    @FXML
    private  ImageView search_logo;
    
    private ObservableList<SeanceListView> data;
  

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
          img = new Image("assets/newspape.png");
          logo_delete.setImage(new Image("assets/logo_delete.png"));
          logo_add.setImage(new Image("assets/logo_add.png"));
          search_logo.setImage(new Image("assets/search_logo.png"));
          
///////////////////////////////////////////
try {
Context context = new InitialContext();
SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
		+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");

TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");

TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
		+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");

EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
		+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");


List<Seance> seances= new ArrayList<Seance>();
seances=seanceServiceRemote.findAll();
List<SeanceListView> seancesListView= new ArrayList<SeanceListView>();
///////////////////////////////////////////
///////////////////////////////////////////
table.setEditable(false);
data = FXCollections.observableArrayList();
for ( int i = 0; i<table.getItems().size(); i++) 
{
table.getItems().clear();
}
training.setCellValueFactory(new PropertyValueFactory<>("training"));
trainer.setCellValueFactory(new PropertyValueFactory<>("trainer"));
employee.setCellValueFactory(new PropertyValueFactory<>("employee"));
date.setCellValueFactory(new PropertyValueFactory<>("date"));
salle.setCellValueFactory(new PropertyValueFactory<>("salle"));

for(Seance s:seances){
	seancesListView.add(new SeanceListView(s));
}
data = FXCollections.observableList(seancesListView);
table.setItems(data); 
trainingCombo.getItems().addAll(populatecomboTraining());
} catch (NamingException e) {
e.printStackTrace();
}     

} 
       @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }    

    @FXML
    private void add(ActionEvent event) throws IOException {
    	Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
	  	  
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("addSession.fxml"));
        Parent root = (Parent) loader.load();
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }

    @FXML
    private void delete(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
    	SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
		+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");
    	seanceServiceRemote.deleteMyWay(getSeancePkFromTable());
		refreshTable();
    }

    private SeancePk getSeancePkFromTable() {
    	table.setOnMousePressed(new  EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                SeancePk id=table.getSelectionModel().getSelectedItem().getSeancePk();
            }
            });
      return table.getSelectionModel().getSelectedItem().getSeancePk();
    }
    
    public void refreshTable() throws NamingException{
    	Context context = new InitialContext();
    	SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");

    	TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
    	+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");

    	TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");

    	EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");


    	List<Seance> seances= new ArrayList<Seance>();
    	seances=seanceServiceRemote.findAll();
    	List<SeanceListView> seancesListView= new ArrayList<SeanceListView>();
    	///////////////////////////////////////////
    	///////////////////////////////////////////
    	table.setEditable(false);
    	data = FXCollections.observableArrayList();
    	for ( int i = 0; i<table.getItems().size(); i++) 
    	{
    	table.getItems().clear();
    	}
    	training.setCellValueFactory(new PropertyValueFactory<>("training"));
    	trainer.setCellValueFactory(new PropertyValueFactory<>("trainer"));
    	employee.setCellValueFactory(new PropertyValueFactory<>("employee"));
    	date.setCellValueFactory(new PropertyValueFactory<>("date"));
    	salle.setCellValueFactory(new PropertyValueFactory<>("salle"));

    	for(Seance s:seances){
    		seancesListView.add(new SeanceListView(s));
    	}
    	data = FXCollections.observableList(seancesListView);
    	table.setItems(data); 
	}
    
    @FXML
    private void search(ActionEvent event) throws NamingException {
    	refreshTableSearch();
    }
    
    public static ObservableList<Training> populatecomboTraining() throws NamingException {
		Context context = new InitialContext();
		TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");

		List<Training> c = trainingServiceRemote.findAll();
		ObservableList<Training> items = FXCollections.observableArrayList();
		for (Training c1 : c) {
			items.add(c1);
		}
		return items;
	}
    
    public void refreshTableSearch() throws NamingException{
    	Context context = new InitialContext();
    	SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");

    	TrainingServiceRemote trainingServiceRemote = (TrainingServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainingService!"
    	+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote");

    	TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");

    	EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");


    	List<Seance> seances= new ArrayList<Seance>();
    	seances=seanceServiceRemote.findByTraining(trainingCombo.getValue());

    	///////////////////////////////////////////
    	///////////////////////////////////////////
    	List<SeanceListView> seancesListView= new ArrayList<SeanceListView>();
///////////////////////////////////////////
///////////////////////////////////////////
table.setEditable(false);
data = FXCollections.observableArrayList();
for ( int i = 0; i<table.getItems().size(); i++) 
{
table.getItems().clear();
}
training.setCellValueFactory(new PropertyValueFactory<>("training"));
trainer.setCellValueFactory(new PropertyValueFactory<>("trainer"));
employee.setCellValueFactory(new PropertyValueFactory<>("employee"));
date.setCellValueFactory(new PropertyValueFactory<>("date"));
salle.setCellValueFactory(new PropertyValueFactory<>("salle"));

for(Seance s:seances){
seancesListView.add(new SeanceListView(s));
}
data = FXCollections.observableList(seancesListView);
table.setItems(data); 
    }
}
