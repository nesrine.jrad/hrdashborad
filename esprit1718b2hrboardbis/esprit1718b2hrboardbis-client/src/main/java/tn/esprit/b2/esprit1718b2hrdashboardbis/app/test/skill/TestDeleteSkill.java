package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.skill;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote;;

public class TestDeleteSkill {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		SkillServiceRemote skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");

		skillServiceRemote.delete(skillServiceRemote.find(2));
		System.out.println("Skill Deleted");
	}
}
