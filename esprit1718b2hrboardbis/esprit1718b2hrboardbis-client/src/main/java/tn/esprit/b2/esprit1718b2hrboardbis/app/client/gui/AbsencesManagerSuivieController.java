/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXToggleButton;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import javafx.stage.Stage;
import javafx.util.Callback;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Justification;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class AbsencesManagerSuivieController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXListView<Absence> absenceListt;
    
    private ObservableList<Absence> data=FXCollections.observableArrayList();
    @FXML
    private JFXRadioButton radioJustification;
    @FXML
    private ToggleGroup justification;
    @FXML
    private ImageView rechJustification;
    @FXML
    private ImageView imgPDF;
    @FXML
    private JFXRadioButton radioNonJustification;
    @FXML
    private DatePicker datePicker;
    @FXML
    private JFXToggleButton modeSuivie;
    int connectID;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	System.out.print("************"+HomeManagerController.employeeConnected.getName());
    	connectID=HomeManagerController.employeeConnected.getId();
    	
    	Context context;
    	List<Absence> absences=new ArrayList<>();
    	AbsenceServiceRemote absenceServiceRemote;
		
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
           img = new Image("assets/search.png");
           rechJustification.setImage(img);
             img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
        
        // imgPDF.setImage(new Image("assets/pdf.png"));
           img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         modeSuivie.setSelected(true);
         HBox hbox = new HBox();
       data.clear();
       try {
    	   context = new InitialContext();
		absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");
		absences= absenceServiceRemote.getAbsenceByUserId(connectID);
       } catch (NamingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
       Collections.reverse(absences);
        for (Absence absence : absences) {
			 data.add(absence);		
			 
        
        }
              
               absenceListt.setCellFactory(new Callback<ListView<Absence>, ListCell<Absence>>(){
           @Override
           public ListCell<Absence> call(ListView<Absence> args0) {
               Image img= new Image("assets/interview(1).png");
               ImageView imageview=new ImageView(img);
               Label label=new Label("");
               JFXButton buttAnnuler=new JFXButton();
               //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
              // buttAnnuler.setStyle("-fx-background-image: url('/assets/hi.png');-fx-font-size: 1em; ");
               buttAnnuler.setMinHeight(30);
               buttAnnuler.setMinWidth(30);
               JFXButton buttProfil=new JFXButton();
              // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
              buttProfil.setMinHeight(30);
               buttProfil.setMinWidth(32);
               JFXButton buttsend=new JFXButton();
               buttsend.setMinHeight(30);
               buttsend.setMinWidth(30);
               buttsend.setStyle("-fx-background-image: url('/assets/send.png');");
               buttsend.setAlignment(Pos.CENTER_RIGHT);
               HBox hbox = new HBox();
               hbox.getChildren().addAll(imageview, label,buttAnnuler,buttProfil,buttsend);
               
               buttsend.setOnAction((event) -> {
                               
                              
                                   
                   try {
                       System.out.println("kadhem");
                       FXMLLoader loader = new FXMLLoader(getClass().getResource("SendJustification.fxml"));
                       Parent root = (Parent) loader.load();
                       Scene newScene = new Scene(root);
                       Stage newStage = new Stage();
                       newStage.setTitle("envoi d'un justificatif");
                       newStage.setScene(newScene);
                       newStage.show();
                   } catch (IOException ex) {
                       Logger.getLogger(AbsencesManagerSuivieController.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   
                                
                                    
       
                           });
               
               
               ListCell<Absence> cell;
               int i = 0 ;
               cell = new ListCell<Absence>(){
                   
                   @Override
                   protected void updateItem(Absence r ,boolean b){
                       
                       super.updateItem(r,b);
                       
                         if(r != null){
                        	  
                             	 if(r.getJustified()==Justification.NOT_JUSTIFIED){
                             		
                             		 hbox.setStyle("-fx-background-color:#ff9999;");
                           	   }else{
                           		 buttsend.setDisable(true);
                           		 hbox.setStyle("-fx-background-color:#bef67a;");
                           	   }
                             label.setText("FROM:"+r.getFromDate() +" TO:" + r.getToDate());
                           setGraphic(hbox);}
                          
                         
                         
                      
                             
                           
                          
                           
                            
                           
                           
                       
                    
                       }
                       
                     
                
               };
               
               
            return cell;    
           }
            
          
       
                });
        absenceListt.setItems(data);
        
       
        
        
     
           
      
    } 
    
     @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    } 
    
     @FXML
    public void suivieInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("absencesManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }  @FXML
    public void leavestInterface() throws IOException{
   	 
   	 
   	 
   	 FXMLLoader loader = new FXMLLoader(getClass().getResource("VacancesManagerSuivie.fxml"));
        Parent root = (Parent) loader.load();
        
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
        Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
        // do what you have to do
        stage.close();
   
    }
    
    @FXML 
    public void refrech() throws IOException{
   	 
   	 FXMLLoader loader = new FXMLLoader(getClass().getResource("absencesManagerSuivie.fxml"));
        Parent root = (Parent) loader.load();
        
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
        Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
    
}
