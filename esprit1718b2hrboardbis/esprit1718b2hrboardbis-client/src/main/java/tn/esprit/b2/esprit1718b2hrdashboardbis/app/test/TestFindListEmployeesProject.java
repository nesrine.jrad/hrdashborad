package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote;

public class TestFindListEmployeesProject {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		List<Employee> employees = projectServiceRemote.getListEmployees(2);
		System.out.println(employees.size());
	}
}
