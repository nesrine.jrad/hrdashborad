/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.scene.control.Alert;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import com.jfoenix.controls.JFXTextArea;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote;

/**
 * FXML Controller class
 *
 * @author Amine
 */
public class AddTasksToProjectsController implements Initializable {

	@FXML
    private TableView<Employee> employeeTable;
    @FXML
    private DatePicker startDatePicker;
    @FXML
    private JFXButton confirmBtn;
    @FXML
    private JFXButton exitBtn;
    @FXML
    private TableColumn<Employee, Integer> idEmployeeColumn;
    @FXML
    private TableColumn<Employee, String> nameEmployeeColumn;
    @FXML
    private TableColumn<Employee, String> familyNameEmployeeColumn;
    @FXML
    private TableColumn<Employee, Date> hiringDateEmployeeColumn;

    @FXML
    private JFXTextField nameTaskTextField;

    @FXML
    private JFXTextArea descriptionTextArea;
    private Project project;
    private ObservableList<Employee> data;
    //The id of selected employee
    private int employeeId;
    
    
   
	/**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

			startDatePicker.setValue(LocalDate.now());
			employeeTable.setItems(null);
			
          
			
		
		
		

    }    

    @FXML
    private void getIdSelectedEmployee(MouseEvent event) {
    	employeeId = employeeTable.getSelectionModel().getSelectedItem().getId();
    }

    @FXML
    private void addTaskToTheEmployee(ActionEvent event) throws NamingException {
		
    	Context context;
		context = new InitialContext();
		ProjectServiceRemote projectServiceRemote = (ProjectServiceRemote) context
				.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
						+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup(
				"esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		TaskServiceRemote taskServiceRemote = (TaskServiceRemote) context.lookup(
				"esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		Project projectToAssignEmployeesTo = projectServiceRemote.find(this.project.getId());
		Employee employeeToAssignTheTaskTo = employeeServiceRemote.find(employeeId);
		String name = nameTaskTextField.getText();
		Date dateTaskBeginning = Date.valueOf(startDatePicker.getValue());
		String description = descriptionTextArea.getText();
		if(description.equals("")){
			Alert alert = new Alert(Alert.AlertType.ERROR,"Please descripe the task");
			alert.showAndWait();
		} else if (dateTaskBeginning.before(project.getDateOfBeginning())){
			Alert alert = new Alert(Alert.AlertType.ERROR,"You can't start the task before the project ");
			alert.showAndWait();
			
		} else if (name.equals("")){
			Alert alert = new Alert(Alert.AlertType.ERROR,"Give a name to the task ");
			alert.showAndWait();
		} else {
		Task task = new Task(projectToAssignEmployeesTo, employeeToAssignTheTaskTo, name, dateTaskBeginning, description);
		taskServiceRemote.save(task);
		
		}
		
    }

    @FXML
    private void exitWindow(ActionEvent event) {
    	Stage stage = (Stage) exitBtn.getScene().getWindow();
        stage.close();
    	
    }

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
    
	//Get a project from ProjectManagerController
	public void getTheProjectFromPreviousScreen(Project project) throws NamingException{
		setProject(project);
		Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
 				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		//List of Employees in a specific project
		List<Employee> employees = projectServiceRemote.getListEmployees(project.getId());
		if (employees != null) {
			data = FXCollections.observableList(employees);
			idEmployeeColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
			nameEmployeeColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
			familyNameEmployeeColumn.setCellValueFactory(new PropertyValueFactory<>("familyName"));
			hiringDateEmployeeColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfHiring"));
			employeeTable.setItems(data);
		}
	}
}
