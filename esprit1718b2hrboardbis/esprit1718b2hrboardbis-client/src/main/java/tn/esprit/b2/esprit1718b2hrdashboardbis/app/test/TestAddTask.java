package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.sql.Date;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskPK;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote;

public class TestAddTask {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		TaskServiceRemote taskServiceRemote= (TaskServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TaskService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TaskServiceRemote");
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		Employee employee = employeeServiceRemote.find(1);
		Project project = projectServiceRemote.find(1);
		Date dateTask = Date.valueOf("2018-02-21");
		Task task = new Task(project, employee, "Tache1_ Projet2", dateTask, "Descrition" );
		Task task1 = new Task(project, employee, "Tache2 Projet2", dateTask, "Descrition" );
		Task task2 = new Task(project, employee, "Tache3 Projet2", dateTask, "Descrition" );
		Task task3 = new Task(project, employee, "Tache4 Projet2", dateTask, "Descrition" );
		task.setStatusTask(StatusTask.DONE);
		task1.setStatusTask(StatusTask.DONE);
		task2.setStatusTask(StatusTask.DOING);
		taskServiceRemote.save(task);
		taskServiceRemote.save(task1);
		taskServiceRemote.save(task2);
		taskServiceRemote.save(task3);

	}

}
