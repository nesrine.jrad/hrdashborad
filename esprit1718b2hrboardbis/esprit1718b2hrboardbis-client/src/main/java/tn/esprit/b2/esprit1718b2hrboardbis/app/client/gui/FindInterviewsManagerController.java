/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.InterviewView;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class FindInterviewsManagerController implements Initializable {

    @FXML
    private AnchorPane TextFindJobOffer;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXButton HomeManager;
    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView JObOfferImg;
    @FXML
    private JFXButton BackRecrutments;
    @FXML
    private ImageView BAckImg;
    @FXML
    private TableView<InterviewView> ListOfJobOffer;
    @FXML
    private TableColumn<InterviewView, Date> DateOfInterview;
    @FXML
    private TableColumn<InterviewView, String> TimeOfInterview;
    @FXML
    private TableColumn<InterviewView, Integer> InterviewEvaluation;
    @FXML
    private TableColumn<InterviewView, Integer> idOfInterview;
    @FXML
    private ImageView FindJobOfferBt;
    @FXML
    private JFXButton evaluateInterview;
    @FXML
    private ImageView recrutImg;
    @FXML
    private JFXToggleButton filterButton;
    private Employee employee;
    private Context context;
    private ObservableList<Interview> data;
    private ObservableList<Interview> data2;
    private ObservableList<InterviewView> data3;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	 Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
         TimeOfInterview.setCellValueFactory(new PropertyValueFactory<>("timeOfInterview"));
	  		
			DateOfInterview.setCellValueFactory(new PropertyValueFactory<>("dateOfInterview"));
         
    }    

    @FXML
    private void HomeManagerInterface(ActionEvent event) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeManager.fxml"));
        Parent root = (Parent) loader.load();
        
        Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
        
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }

    @FXML
    private void projectInterface(ActionEvent event) {
    }

    @FXML
    private void JobOfferManageInterface(MouseEvent event) {
    }

    @FXML
    private void BackrecrutmentsInterface(ActionEvent event) {
    }

    @FXML
    private void ItemSelected(MouseEvent event) {
    
    }

    @FXML
    private void FindJobOffer(MouseEvent event) {
    }

    @FXML
    private void evaluateInterview(ActionEvent event) {
    }

    @FXML
    private void ChangeInterview(ActionEvent event) {

          if(	filterButton.isSelected())
          {	
          	ObservableList<InterviewView> data5;
         	    	   ObservableList<JobOffer> data4;
         	       	data5=ListOfJobOffer.getItems();
         	     
         	     
         	       	List<InterviewView> interviews= new ArrayList<InterviewView>();
         	       	List<InterviewView> interviews1= new ArrayList<InterviewView>();
         	       	interviews.addAll(data5);
         	       	System.out.println("data"+data5.size());
         	       	System.out.println("interview"+interviews.size());
         	      	for (InterviewView interview : interviews) {
         	       		if((interview.getDateOfInterview().before(java.sql.Date.valueOf(java.time.LocalDate.now()))))
         	   			{   System.out.println(interview.getDateOfInterview());
         	   				interviews1.add(interview);
         	   			}
         	   		}
     	       		System.out.println(interviews1.size());
     	       		
          			data3=FXCollections.observableList(interviews1);

     	 			    TimeOfInterview.setCellValueFactory(new PropertyValueFactory<>("timeOfInterview"));
     	 				DateOfInterview.setCellValueFactory(new PropertyValueFactory<>("dateOfInterview"));
     	 				ListOfJobOffer.setItems(data3);
             		
         		
             }
         			else
         			{
         				try {
         		 			context=new InitialContext();
         		 			List<Interview> interviews= new ArrayList<Interview>();
         		 			InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
         		 			interviews=interviewServicesRemote.findAll();
         		 			interviews.forEach(System.out::println);
         		 			List<InterviewView> interviewViews= new ArrayList<InterviewView>();
         		 			for (Interview interview : interviews) {
         		 				InterviewView interviewView= new InterviewView(interview.getId(), interview.getJobRequest().getEmailOfCandidate(), interview.getDateOfInterview(), interview.getTimeOfInterview(), interview.getEvalution(), interview.getEmployee().getId(), interview.getJobRequest().getJobOffer().getName());
         		 				interviewViews.add(interviewView);
         		 			}
         		 			data3=FXCollections.observableList(interviewViews);
         		 			//interviews.forEach(System.outprintln);
         		 			    TimeOfInterview.setCellValueFactory(new PropertyValueFactory<>("timeOfInterview"));
         		 				DateOfInterview.setCellValueFactory(new PropertyValueFactory<>("dateOfInterview"));
         		 				ListOfJobOffer.setItems(data3);
         		 		} catch (NamingException e) {
         		 		}
         				}
    }

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
    public void returnId(Employee employee) {
		setEmployee(employee);
		try {
 			context=new InitialContext();
 			List<Interview> interviews= new ArrayList<Interview>();
 			List<Interview> interviews1= new ArrayList<Interview>();
 			InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");
 			interviews=interviewServicesRemote.findAll();
 			for (Interview interview : interviews) {
				if((employee.getId())==(interview.getEmployee().getId()))
					interviews1.add(interview);
				
			}
 			List<InterviewView> interviewViews= new ArrayList<InterviewView>();

 			for (Interview interview : interviews1) {
 				InterviewView interviewView= new InterviewView(interview.getId(), interview.getJobRequest().getEmailOfCandidate(), interview.getDateOfInterview(), interview.getTimeOfInterview(), interview.getEvalution(), interview.getEmployee().getId(), interview.getJobRequest().getJobOffer().getName());
 				interviewViews.add(interviewView);
			}
 			
 			
 			data3= FXCollections.observableList(interviewViews);
			 
				ListOfJobOffer.setItems(data3);
				System.out.println(employee.getId());
 		
 				
 		} catch (NamingException e) {
 		}
         
	}
}
