package tn.esprit.b2.esprit1718b2hrboardbis.app.test.Department;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesLocal;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;

public class TestAddDepartment {
	public static void main(String[] argh) throws NamingException
	{
		Context context= new InitialContext();
		DepartmentServicesRemote departmentServicesRemote=(DepartmentServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");
		Department department= new Department();
		department.setName("RH");
		departmentServicesRemote.save(department);
		
	}

}
