package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import java.util.List;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote;

public class TestFindProjectsOfAnEmployee {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		ProjectServiceRemote projectServiceRemote= (ProjectServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/ProjectService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.ProjectServiceRemote");
		List<Project> projects = projectServiceRemote.getProjectsByEmployee(1);
		List<Project> projects1 = projectServiceRemote.getProjectsByEmployee(2);
		
			System.out.println(projects.size()+" 1");
			System.out.println(projects1.size()+" 2");

	}
}
