package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class AddTrainerController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXTextField name;
    @FXML
    private JFXTextField familyName;
    @FXML
    private JFXTextField email;
    @FXML
    private JFXTextField number;
    @FXML
    private JFXTextField domain;
    @FXML
    private ImageView logo_delete;
    @FXML
    private  ImageView logo_add;
 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
          img = new Image("assets/newspape.png");
          logo_delete.setImage(new Image("assets/logo_delete.png"));
          logo_add.setImage(new Image("assets/logo_add.png"));
          
       
    }

  @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
	  	  
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }        

    @FXML
    private void create(ActionEvent event) throws IOException, NamingException {
Context context = new InitialContext();
		
TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
		+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");
String msg="";
if (name.getText().equals("")) {
	msg="Field Name is required !";
    GenererAlerte(msg);
}
else if (familyName.getText().equals("")) {
	msg="Field Family Name is required !";
    GenererAlerte(msg);
}
else if (email.getText().equals("")) {
	msg="Field Email is required !";
    GenererAlerte(msg);
}
else if (number.getText().equals("") || (!(isNumeric(number.getText())))) {
	msg="Field Telephone Number is Invalid";
    GenererAlerte(msg);
}
else if (domain.getText().equals("")) {
	msg="Field Domain is required !";
    GenererAlerte(msg);
}
else {
		Trainer trainer=new Trainer(name.getText(), familyName.getText(), email.getText(), number.getText(), domain.getText());
		trainerServiceRemote.save(trainer);
		
		Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
	  	  
		FXMLLoader loader=new FXMLLoader(getClass().getResource("TrainerManagement.fxml"));
	    Parent root=(Parent) loader.load();
		Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
} }

    @FXML
    private void cancel(ActionEvent event) {
    	name.clear();
        familyName.clear();
        email.clear();
        number.clear();
        domain.clear();
    }
    
    private static void GenererAlerte(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(msg);
        alert.showAndWait(); 
    }
    
    public static boolean isNumeric(String str)
    {
for (char c : str.toCharArray())
{
    if (!Character.isDigit(c)) return false;
}
return true;
    }

    
}
