package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

public class TestUpdateEmployee {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
		Employee employee = employeeServiceRemote.find(1);
		employee.setAddress("Tunis");
		employeeServiceRemote.update(employee);

	}

}
