/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class LeaveDemandController implements Initializable {

    @FXML
    private DatePicker datePickerFrom;
    @FXML
    private DatePicker datePickerTo;
    @FXML
    private JFXTextField Description;
    @FXML
    private ImageView imgM2A;
    @FXML
    private JFXButton btnDemandLeave;
    int connectID;
    Context context;
  
    
    /**leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
        employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
     System.out.print("MAnager"+HomeManagerController.employeeConnected);
     System.out.print("MAnager"+HomeController.employeeConnected);
     if(HomeManagerController.employeeConnected !=null){
    	 connectID=HomeManagerController.employeeConnected.getId();
     }else{
    	 connectID=HomeController.employeeConnected.getId();
    	 
     }
    	
         Image img = new Image("assets/M2A.png");
         imgM2A.setImage(img);
      
         
    }  
    
    
    @FXML
    public void DemandAction() throws IOException {
    	
        LocalDate dateFrom = datePickerFrom.getValue();
         LocalDate dateTo = datePickerTo.getValue();
         System.out.println(java.time.LocalDate.now());
         if((dateFrom == null) || (dateTo == null) || (Description.getText()==null)){
        	 Alert alert = new Alert(AlertType.WARNING);
             alert.setTitle("error in input data");
             alert.setHeaderText("check Empty Fields");
             
             
             ButtonType buttonTypeCancel = new ButtonType("OK", ButtonData.CANCEL_CLOSE);
             
             alert.getButtonTypes().setAll(buttonTypeCancel);
             
             Optional<ButtonType> result = alert.showAndWait();
        	 
         }
         else{
         if((dateFrom.isBefore(java.time.LocalDate.now()))||(dateTo.isBefore(java.time.LocalDate.now()))
                 ||(dateTo.isBefore(dateFrom))){
                   Alert alert = new Alert(AlertType.WARNING);
                               alert.setTitle("error in input data");
                               alert.setHeaderText("check dates");
                               
                               
                               ButtonType buttonTypeCancel = new ButtonType("OK", ButtonData.CANCEL_CLOSE);
                               
                               alert.getButtonTypes().setAll(buttonTypeCancel);
                               
                               Optional<ButtonType> result = alert.showAndWait();
         }
         else{ 
        	 System.out.println(dateFrom.toString() + dateTo.toString());
        	   try {
        		   Context context = new InitialContext();
       			LeaveServiceRemote leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
       			EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
       			Leave leave = new Leave("leave request ",Date.valueOf(dateFrom.toString()),Date.valueOf(dateTo.toString()),Description.getText(),false,employeeServiceRemote.find(connectID));
        		 leaveServiceRemote.save(leave);
        		  Alert alert = new Alert(AlertType.WARNING);
                  alert.setTitle("add with success");
                  alert.setHeaderText("your leave request is send to your HR manager");
                  
                  
                  ButtonType buttonTypeCancel = new ButtonType("ok", ButtonData.CANCEL_CLOSE);
                  
                  alert.getButtonTypes().setAll(buttonTypeCancel);
        		 Stage stage = (Stage) Description.getScene().getWindow();
        		    // do what you have to do
        		    stage.close();
        		        FXMLLoader loader = new FXMLLoader(getClass().getResource("Vacances.fxml"));
                        Parent root;
     				    root = (Parent) loader.load();
     				    Scene newScene = new Scene(root);
     		            Stage newStage = new Stage();
     		            newStage.setTitle("Extend Leave");
     		            newStage.setScene(newScene);
     		            newStage.show();
        	   } catch (NamingException e) {
       			// TODO Auto-generated catch block
       			e.printStackTrace();
       		}
         } }    
    } 
}
