package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.recrutement;

import java.util.List;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote;

public class TestDisplayAllJobRequest {
	public static void main(String[] argh) throws NamingException
	{
		Context context= new InitialContext();
		JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
		JobRequest jobRequest= new JobRequest();
		List<JobRequest> jobRequests= new ArrayList<JobRequest>();
		jobRequests=jobRequestServicesRemote.findAll();
		jobRequests.forEach(System.out::println);
		
	}

}
