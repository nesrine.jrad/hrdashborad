package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.seance;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;

public class TestUpdateSeance {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");
		Seance seance=seanceServiceRemote.find(1);//////////////to ckeck
		seance.setSalle("B44");
		seanceServiceRemote.update(seance);
	}
}
