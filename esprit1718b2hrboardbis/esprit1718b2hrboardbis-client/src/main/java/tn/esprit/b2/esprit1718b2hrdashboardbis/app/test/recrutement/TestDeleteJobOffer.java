package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.recrutement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote;

public class TestDeleteJobOffer {
	public static void main ( String[]argh) throws NamingException
	{
		Context context= new InitialContext();
		JobOfferServicesRemote jobOfferServicesRemote= (JobOfferServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobOfferServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobOfferServicesRemote");
		JobOffer jobOffer= new JobOffer();
		jobOffer=jobOfferServicesRemote.find(1);
		jobOfferServicesRemote.delete(jobOffer);
	}

}
