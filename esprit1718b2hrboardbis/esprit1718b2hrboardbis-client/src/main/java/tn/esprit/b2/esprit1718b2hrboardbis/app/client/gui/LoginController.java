/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class LoginController implements Initializable {

    @FXML
    private ImageView ImageLogo;
    @FXML
    private JFXButton loginbtn;
    @FXML
    private JFXButton pwdbtn;
    @FXML
    private ImageView imgLoginSlide;
    @FXML
    private JFXTextField login;
    @FXML
    private JFXPasswordField password2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
      Image img = new Image("assets/M2A.png");
      ImageLogo.setImage(img);
      img = new Image("assets/socity.jpg");
      imgLoginSlide.setImage(img);
       loginbtn.setStyle("-fx-font: 14 arial; -fx-base: #b6e7c9;-fx-background-color: #74D14C");
       pwdbtn.setStyle("-fx-font: 17arial; -fx-base: #b6e7c9;-fx-background-color: #74D14C ;");
    }

    @FXML
    private void changerMode() throws IOException, NamingException {
    	 Context context= new InitialContext();
		 EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
         List<Employee> employees= employeeServiceRemote.findEmployees(login.getText(),password2.getText());
          if(employees.size() ==0){
        	  Alert alert = new Alert(AlertType.WARNING);
        	  alert.setTitle("error in input data");
              alert.setHeaderText("Login or Password");
              
              
              ButtonType buttonTypeCancel = new ButtonType("non", ButtonData.CANCEL_CLOSE);
              
              alert.getButtonTypes().setAll(buttonTypeCancel);
              
              Optional<ButtonType> result = alert.showAndWait();  
          }
          
 		 Employee employee2= employees.get(0);
 		System.out.println(employee2.getName());
        	 for (Employee employee : employees) {
        		 
			if(employee.getRoleEmployee().equals(RoleEmployee.MANAGER)){
		 FXMLLoader loader=new FXMLLoader(getClass().getResource("HomeManager.fxml"));
         Parent root = (Parent) loader.load();
		 HomeManagerController homeManagerController= new HomeManagerController();
		 homeManagerController=loader.getController();
		 homeManagerController.returnId(employee);
		 System.out.println(employee.getRoleEmployee());
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
       
       
         Stage stage = (Stage) imgLoginSlide.getScene().getWindow();
         // do what you have to do
         stage.close();}
			else if(employee.getRoleEmployee().equals(RoleEmployee.EMPLOYEE)){
				System.out.println(employee.getRoleEmployee());
				FXMLLoader loader=new FXMLLoader(getClass().getResource("Home.fxml"));
		         Parent root = (Parent) loader.load();
				HomeController homeController= new HomeController();
				homeController=loader.getController();
				homeController.returnId(employee);
		         Scene newScene = new Scene(root);
		         Stage newStage = new Stage();
		         newStage.setScene(newScene);
		         newStage.show();
		       
		       
		         Stage stage = (Stage) imgLoginSlide.getScene().getWindow();
		         // do what you have to do
		         stage.close();
        	 
         }else if(employee.getRoleEmployee().equals(RoleEmployee.HRMANAGER)){
        	 System.out.println(employee.getRoleEmployee());
             FXMLLoader loader=new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
	         Parent root = (Parent) loader.load();
	         HomeHrManagerController homeHrManagerController= new HomeHrManagerController();
	         homeHrManagerController= loader.getController();
	         homeHrManagerController.returnId(employee);
	         Scene newScene = new Scene(root);
	         Stage newStage = new Stage();
	         newStage.setScene(newScene);
	         newStage.show();
	       
	       
	         Stage stage = (Stage) imgLoginSlide.getScene().getWindow();
	         // do what you have to do
	         stage.close();
         }
        	 }
    }
    
    
    
}
