package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.recrutement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote;

public class TestDeleteInterview {
	public static void main(String[] argh ) throws NamingException
	{	Context context= new InitialContext();
	//JobRequestServicesRemote jobRequestServicesRemote= (JobRequestServicesRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/JobRequestServices!tn.esprit.b2.esprit1718b2hrboardbis.services.JobRequestServicesRemote");
	InterviewServicesRemote interviewServicesRemote= (InterviewServicesRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/InterviewServices!tn.esprit.b2.esprit1718b2hrboardbis.services.InterviewServicesRemote");		
		Interview interview= new Interview();
		interview=interviewServicesRemote.find(1);
		interviewServicesRemote.deleteInterview(interview);
	}


}
