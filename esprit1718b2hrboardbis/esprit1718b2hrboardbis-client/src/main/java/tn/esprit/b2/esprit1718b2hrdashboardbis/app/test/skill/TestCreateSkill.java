package tn.esprit.b2.esprit1718b2hrdashboardbis.app.test.skill;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote;;

public class TestCreateSkill {
	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		SkillServiceRemote skillServiceRemote= (SkillServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SkillService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SkillServiceRemote");

		String skillName="C++";
		String skillDescription="C++ est un langage de programmation compilé permettant la programmation sous de "
				+ "multiples paradigmes (comme la programmation procédurale, orientée objet ou générique). ";
		
		String skillName2="Assembleur ";
		String skillDescription2="Un langage d'assemblage ou langage assembleur est, en programmation informatique, "
				+ "un langage de bas niveau qui représente le langage machine sous une forme lisible par un humain.";
		
		//Recherche si ça existe
		
		
		
		Skill skill = new Skill(skillName,skillDescription);
		Skill skill2 = new Skill(skillName2,skillDescription2);
		
		
		skillServiceRemote.save(skill);
		skillServiceRemote.save(skill2);

	}
}
