/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXToggleButton;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import tn.esprit.b2.esprit1718b2hrboardbis.app.client.utilities.SendMail;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class VacancesManagerSuivieController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private JFXListView<Leave> vacationList;
    @FXML
    private ToggleGroup vacationFilter;
    @FXML
    private ImageView imgSearch;
    @FXML
    private JFXButton btnFilter;
    @FXML
    private JFXButton btnDemand;
    @FXML
    private ImageView imgDemand;
    
      private ObservableList<Leave> data=FXCollections.observableArrayList();
    @FXML
    private ImageView imgPDF;
    @FXML
    private JFXToggleButton ModeToggle;
    Context context;
    LeaveServiceRemote leaveServiceRemote ;
	EmployeeServiceRemote employeeServiceRemote;
	DepartmentServicesRemote departmentServicesRemote;
	int connectID;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	connectID=HomeManagerController.employeeConnected.getId();
   	  try {
			 context= new InitialContext();
			 leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
			 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
				departmentServicesRemote = (DepartmentServicesRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");

   	  } catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         ModeToggle.setSelected(true);
       imgSearch.setImage(new Image("assets/search.png"));
       imgDemand.setImage(new Image("assets/plus.png"));
       // imgPDF.setImage(new Image("assets/pdf.png"));
        HBox hbox = new HBox();
       data.clear();
       List<Leave> leaves =new ArrayList<Leave>();
       List<Department> departments = departmentServicesRemote.findAll();
		for (Department department : departments) {
			 for (Employee employee : department.getEmployees()) {
				   
				   if(employee.getId()==connectID)
				   {
					
				   //absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");   
					   for (Employee employee2 : department.getEmployees()) {
						     if(employee2.getId()!=connectID)
						    	 leaves.addAll(leaveServiceRemote.getLeavesByUserId(employee2.getId()));
					}
					  
				   
				   
				   }
				   
			}
			   
			   
		}
 // List<Leave> leaves = leaveServiceRemote.findAll();
      Collections.reverse(leaves);
      for (Leave leave : leaves) {
//    	   if(leave.getEmployee().getId()!= connectID)
    	  data.add(leave);
		}
              
               vacationList.setCellFactory(new Callback<ListView<Leave>, ListCell<Leave>>(){
           @Override
           public ListCell<Leave> call(ListView<Leave> args0) {
               Image img= new Image("assets/interview(1).png");
               ImageView imageview=new ImageView(img);
               Label label=new Label("");
               JFXButton buttAnnuler=new JFXButton();
               //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
               //buttAnnuler.setStyle("-fx-background-image: url('/assets/delete.png');-fx-font-size: 1em; ");
               buttAnnuler.setMinHeight(30);
               buttAnnuler.setMinWidth(30);
               JFXButton buttProfil=new JFXButton();
              // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
              buttProfil.setMinHeight(30);
               buttProfil.setMinWidth(32);
               JFXButton buttextend=new JFXButton();
               buttextend.setMinHeight(30);
               buttextend.setMinWidth(30);
               //buttextend.setStyle("-fx-background-image: url('/assets/extend.png');");
               buttextend.setAlignment(Pos.CENTER_RIGHT);
               HBox hbox = new HBox();
               hbox.getChildren().addAll(imageview, label,buttProfil,buttAnnuler,buttextend);
               
              
               
               
               
               ListCell<Leave> cell;
               int i = 0 ;
               cell = new ListCell<Leave>(){
                   
                   @Override
                   protected void updateItem(Leave r ,boolean b){
                       
                       super.updateItem(r,b);
                       
                         if(r != null){
                        	 if(r.isApproved()==false){
                         		
                          		 hbox.setStyle("-fx-background-color:#ff9999;");
                        	   }else{                         
                               
                        		 hbox.setStyle("-fx-background-color:#bef67a;");}
                             label.setText(r.getEmployee().getName() +" "+r.getEmployee().getFamilyName()+" Description: "+r.getDescription()+" From:"+r.getFromDate()+" To:"+r.getToDate());
                           setGraphic(hbox);}
                          
                         
                         
                      
                             
                           
                          
                           
                            
                           
                           
                       
                    
                       }
                       
                     
                
               };
               
               
            return cell;    
           }
            
          
       
                });
        vacationList.setItems(data);
        
       
        
        
     
           
    }    
    
 @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    } 
    
     @FXML
    public void SuivieInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("VacancesManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();
    
    }    
     @FXML
     public void absencInterface() throws IOException{
    	 
    	 
    	 System.out.println("kadhem");
    	 FXMLLoader loader = new FXMLLoader(getClass().getResource("absencesManagerSuivie.fxml"));
         Parent root = (Parent) loader.load();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
         Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
         // do what you have to do
         stage.close();

     }
     
     @FXML
     public void filter() throws NamingException { 
    	 context= new InitialContext();
			departmentServicesRemote = (DepartmentServicesRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/DepartmentServices!tn.esprit.b2.esprit1718b2hrboardbis.services.DepartmentServicesRemote");

     	JFXRadioButton radioJustification=(JFXRadioButton)vacationFilter.getSelectedToggle();
 	 	System.out.println(radioJustification.getText());
 	 	if(radioJustification.getText().equals("Passed")){
 	 		connectID=HomeManagerController.employeeConnected.getId();
 	    	  try {
 				 context= new InitialContext();
 				 leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
 				 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
 			} catch (NamingException e1) {
 				// TODO Auto-generated catch block
 				e1.printStackTrace();
 			}
 				
 	         Image img = new Image("assets/home.png");
 	         imgBtHome.setImage(img);
 	         img = new Image("assets/projectmenu.png");
 	         imgbtProjects.setImage(img);
 	         img = new Image("assets/lecture.png");
 	         imgBtTrainig.setImage(img);
 	         img = new Image("assets/luggage.png");
 	         imgBtVac.setImage(img);
 	         img = new Image("assets/calendar.png");
 	         imgBtAbsences.setImage(img);
 	         img = new Image("assets/users.png");
 	         imgBtProfile.setImage(img);
 	         img = new Image("assets/chatting.png");
 	        imgBtCommunication.setImage(img);
 	         img = new Image("assets/salary.png");
 	         imgBtSalaire.setImage(img);
 	          img = new Image("assets/logout.png");
 	         imgBtlogout.setImage(img);
 	          img = new Image("assets/hi.png");
 	         imgHi.setImage(img);
 	         img = new Image("assets/hrhead.jpg");
 	         headimg.setImage(img);
 	         img = new Image("assets/M2A.png");
 	         logoImg.setImage(img);
 	         
 	       imgSearch.setImage(new Image("assets/search.png"));
 	       imgDemand.setImage(new Image("assets/plus.png"));
 	        //imgPDF.setImage(new Image("assets/pdf.png"));
 	        HBox hbox = new HBox();
 	       data.clear();
 	      List<Leave> leaves =new ArrayList<Leave>();
 	       List<Department> departments = departmentServicesRemote.findAll();
 			for (Department department : departments) {
 				 for (Employee employee : department.getEmployees()) {
 					   
 					   if(employee.getId()==connectID)
 					   {
 						
 					   //absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");   
 						   for (Employee employee2 : department.getEmployees()) {
 							     if(employee2.getId()!=connectID)
 							    	 leaves.addAll(leaveServiceRemote.getLeavesByUserId(employee2.getId()));
 						}
 						  
 					   
 					   
 					   }
 					   
 				}
 				   
 				   
 			}
 	 // List<Leave> leaves = leaveServiceRemote.findAll();
 	      Collections.reverse(leaves);
 	      for (Leave leave : leaves) {
      	 if(leave.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now())))
 	    	  data.add(leave);
 			}
// 	             List<Leave> leaves = leaveServiceRemote.getLeavesByUserId(connectID);
// 	             Collections.reverse(leaves);
// 	             for (Leave leave : leaves) {
// 	            	 if(leave.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now())))
// 					data.add(leave);
// 				}
 	               vacationList.setCellFactory(new Callback<ListView<Leave>, ListCell<Leave>>(){
 	           @Override
 	           public ListCell<Leave> call(ListView<Leave> args0) {
 	               Image img= new Image("assets/interview(1).png");
 	               ImageView imageview=new ImageView(img);
 	               Label label=new Label("");
 	               JFXButton buttAnnuler=new JFXButton();
 	               //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
 	               buttAnnuler.setStyle("-fx-background-image: url('/assets/ te.png');-fx-font-size: 1em; ");
 	               buttAnnuler.setMinHeight(30);
 	               buttAnnuler.setMinWidth(30);
 	               JFXButton buttProfil=new JFXButton();
 	              // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
 	              buttProfil.setMinHeight(30);
 	               buttProfil.setMinWidth(32);
 	               JFXButton buttextend=new JFXButton();
 	               buttextend.setMinHeight(30);
 	               buttextend.setMinWidth(30);
 	               buttextend.setStyle("-fx-background-image: url('/assets/extend.png');");
 	               buttextend.setAlignment(Pos.CENTER_RIGHT);
 	               HBox hbox = new HBox();
 	               hbox.getChildren().addAll(imageview, label,buttProfil,buttAnnuler,buttextend);
 	               
 	               
 	               
 	           
 	               
 	               
 	               
 	               ListCell<Leave> cell;
 	               int i = 0 ;
 	               cell = new ListCell<Leave>(){
 	                   
 	                   @Override
 	                   protected void updateItem(Leave r ,boolean b){
 	                       
 	                       super.updateItem(r,b);
 	                       
 	                         if(r != null){
 	                        	 buttextend.setOnAction((event) -> {
 	                                 
 	                                 try {
 	                                     System.out.print("extend");
 	                                     FXMLLoader loader = new FXMLLoader(getClass().getResource("extendLeave.fxml"));
 	                                     
 	                                     Parent root = (Parent) loader.load();
 	                                     ExtendLeaveController ctrl = loader.getController();
 	                                     ctrl.setIdLeave(r.getId());
 	                                     
 	                                     Scene newScene = new Scene(root);
 	                                     Stage newStage = new Stage();
 	                                     newStage.setTitle("Extend Leave");
 	                                     newStage.setScene(newScene);
 	                                     newStage.show();
 	                                   
 	                                 } catch (IOException ex) {
 	                                     Logger.getLogger(VacancesController.class.getName()).log(Level.SEVERE, null, ex);
 	                                 }
 	                                              
 	                                                  
 	                     
 	                                         });
 	                        	 if(r.isApproved()==false){
 	                         		
 	                        		 hbox.setStyle("-fx-background-color:#ff9999;");
 	                      	   }else{                         
 	                             
 	                      		 hbox.setStyle("-fx-background-color:#bef67a;");}
 	                             label.setText(r.getDescription()+" From:"+r.getFromDate()+" To:"+r.getToDate());
 	                           setGraphic(hbox);
 	                           if(r.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now()))){
 	                        	   buttAnnuler.setDisable(true);
 	                           }
 	                           buttAnnuler.setOnAction((event) -> {
 	                                
 	                               
 	                               System.out.print("annuler");
 	                               
 	                                             Alert alert = new Alert(AlertType.CONFIRMATION);
 	             alert.setTitle("Confirm Deletion");
 	             alert.setHeaderText("you will delete  your leaving demand ");
 	             alert.setContentText("Are you ok with this?");
 	                   
 	             Optional<ButtonType> result = alert.showAndWait();
 	             if (result.get() == ButtonType.OK){
 	             	leaveServiceRemote.delete(r);
 	             	SendMail mail =new SendMail();
 	             	System.out.print("kadhem");
 	          	  mail.sendmail("kadhem94@gmail.com","m2adashbored@gmail.com", "kasouma1994", "Delete Leave\n"+r.getEmployee().getName()+" "+r.getEmployee().getFamilyName()+r.getDescription()+"\nFrom :"+r.getFromDate()+"\n To :"+r.getToDate());

 	      
 	             } else {
 	               
 	             }
 	                                                 
 	                    
 	                                        });
 	                             
 	                         
 	                         
 	                         }
 	                         
 	                         
 	                      
 	                             
 	                           
 	                          
 	                           
 	                            
 	                           
 	                           
 	                       
 	                    
 	                       }
 	                       
 	                     
 	                
 	               };
 	               
 	               
 	            return cell;    
 	           }
 	            
 	          
 	       
 	                });
 	        vacationList.setItems(data);
 	 		
 	 	}else if(radioJustification.getText().equals("Waiting")){
 	 		connectID=HomeManagerController.employeeConnected.getId();
 	    	  try {
 				 context= new InitialContext();
 				 leaveServiceRemote = (LeaveServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/LeaveService!tn.esprit.b2.esprit1718b2hrboardbis.services.LeaveServiceRemote");
 				 employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
 			} catch (NamingException e1) {
 				// TODO Auto-generated catch block
 				e1.printStackTrace();
 			}
 				
 	         Image img = new Image("assets/home.png");
 	         imgBtHome.setImage(img);
 	         img = new Image("assets/projectmenu.png");
 	         imgbtProjects.setImage(img);
 	         img = new Image("assets/lecture.png");
 	         imgBtTrainig.setImage(img);
 	         img = new Image("assets/luggage.png");
 	         imgBtVac.setImage(img);
 	         img = new Image("assets/calendar.png");
 	         imgBtAbsences.setImage(img);
 	         img = new Image("assets/users.png");
 	         imgBtProfile.setImage(img);
 	         img = new Image("assets/chatting.png");
 	        imgBtCommunication.setImage(img);
 	         img = new Image("assets/salary.png");
 	         imgBtSalaire.setImage(img);
 	          img = new Image("assets/logout.png");
 	         imgBtlogout.setImage(img);
 	          img = new Image("assets/hi.png");
 	         imgHi.setImage(img);
 	         img = new Image("assets/hrhead.jpg");
 	         headimg.setImage(img);
 	         img = new Image("assets/M2A.png");
 	         logoImg.setImage(img);
 	         
 	       imgSearch.setImage(new Image("assets/search.png"));
 	       imgDemand.setImage(new Image("assets/plus.png"));
 	       // imgPDF.setImage(new Image("assets/pdf.png"));
 	        HBox hbox = new HBox();
 	       data.clear();
 	      List<Leave> leaves =new ArrayList<Leave>();
	       List<Department> departments = departmentServicesRemote.findAll();
			for (Department department : departments) {
				 for (Employee employee : department.getEmployees()) {
					   
					   if(employee.getId()==connectID)
					   {
						
					   //absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");   
						   for (Employee employee2 : department.getEmployees()) {
							     if(employee2.getId()!=connectID)
							    	 leaves.addAll(leaveServiceRemote.getLeavesByUserId(employee2.getId()));
						}
						  
					   
					   
					   }
					   
				}
				   
				   
			}
	 // List<Leave> leaves = leaveServiceRemote.findAll();
	      Collections.reverse(leaves);
	      for (Leave leave : leaves) {
    	 if(leave.getFromDate().after(java.sql.Date.valueOf(java.time.LocalDate.now())))
	    	  data.add(leave);
			}
// 	             List<Leave> leaves = leaveServiceRemote.getLeavesByUserId(connectID);
// 	             Collections.reverse(leaves);
// 	             for (Leave leave : leaves) {
// 	            	 if(leave.getFromDate().after(java.sql.Date.valueOf(java.time.LocalDate.now())))
// 					data.add(leave);
// 				}
 	               vacationList.setCellFactory(new Callback<ListView<Leave>, ListCell<Leave>>(){
 	           @Override
 	           public ListCell<Leave> call(ListView<Leave> args0) {
 	               Image img= new Image("assets/interview(1).png");
 	               ImageView imageview=new ImageView(img);
 	               Label label=new Label("");
 	               JFXButton buttAnnuler=new JFXButton();
 	               //butt.setStyle("-fx-background-color: #ff5722; -fx-text-fill: white;");
 	               buttAnnuler.setStyle("-fx-background-image: url('/assets/delete.png');-fx-font-size: 1em; ");
 	               buttAnnuler.setMinHeight(30);
 	               buttAnnuler.setMinWidth(30);
 	               JFXButton buttProfil=new JFXButton();
 	              // buttProfil.setStyle("-fx-background-image: url('/assets/hi.png');");
 	              buttProfil.setMinHeight(30);
 	               buttProfil.setMinWidth(32);
 	               JFXButton buttextend=new JFXButton();
 	               buttextend.setMinHeight(30);
 	               buttextend.setMinWidth(30);
 	               buttextend.setStyle("-fx-background-image: url('/assets/extend.png');");
 	               buttextend.setAlignment(Pos.CENTER_RIGHT);
 	               HBox hbox = new HBox();
 	               hbox.getChildren().addAll(imageview, label,buttProfil,buttAnnuler,buttextend);
 	               
 	               
 	               
 	           
 	               
 	               
 	               
 	               ListCell<Leave> cell;
 	               int i = 0 ;
 	               cell = new ListCell<Leave>(){
 	                   
 	                   @Override
 	                   protected void updateItem(Leave r ,boolean b){
 	                       
 	                       super.updateItem(r,b);
 	                       
 	                         if(r != null){
 	                        	 buttextend.setOnAction((event) -> {
 	                                 
 	                                 try {
 	                                     System.out.print("extend");
 	                                     FXMLLoader loader = new FXMLLoader(getClass().getResource("extendLeave.fxml"));
 	                                     
 	                                     Parent root = (Parent) loader.load();
 	                                     ExtendLeaveController ctrl = loader.getController();
 	                                     ctrl.setIdLeave(r.getId());
 	                                     
 	                                     Scene newScene = new Scene(root);
 	                                     Stage newStage = new Stage();
 	                                     newStage.setTitle("Extend Leave");
 	                                     newStage.setScene(newScene);
 	                                     newStage.show();
 	                                   
 	                                 } catch (IOException ex) {
 	                                     Logger.getLogger(VacancesController.class.getName()).log(Level.SEVERE, null, ex);
 	                                 }
 	                                              
 	                                                  
 	                     
 	                                         });
 	                        	 if(r.isApproved()==false){
 	                         		
 	                        		 hbox.setStyle("-fx-background-color:#ff9999;");
 	                      	   }else{                         
 	                             
 	                      		 hbox.setStyle("-fx-background-color:#bef67a;");}
 	                             label.setText(r.getEmployee().getName()+" "+r.getEmployee().getFamilyName()+" "+r.getDescription()+" From:"+r.getFromDate()+" To:"+r.getToDate());
 	                           setGraphic(hbox);
 	                           if(r.getFromDate().before(java.sql.Date.valueOf(java.time.LocalDate.now()))){
 	                        	   buttAnnuler.setDisable(true);
 	                           }
 	                           buttAnnuler.setOnAction((event) -> {
 	                                
 	                               
 	                               System.out.print("annuler");
 	                               
 	                                             Alert alert = new Alert(AlertType.CONFIRMATION);
 	             alert.setTitle("Confirm Deletion");
 	             alert.setHeaderText("you will delete  your leaving demand ");
 	             alert.setContentText("Are you ok with this?");
 	                   
 	             Optional<ButtonType> result = alert.showAndWait();
 	             if (result.get() == ButtonType.OK){
 	             	leaveServiceRemote.delete(r);
 	             	SendMail mail =new SendMail();
 	             	System.out.print("kadhem");
 	          	  mail.sendmail("kadhem94@gmail.com","m2adashbored@gmail.com", "kasouma1994", "Delete Leave\n"+r.getEmployee().getName()+" "+r.getEmployee().getFamilyName()+r.getDescription()+"\nFrom :"+r.getFromDate()+"\n To :"+r.getToDate());

 	             	FXMLLoader loader = new FXMLLoader(getClass().getResource("Vacances.fxml"));
                     Parent root;
 					try {
 						root = (Parent) loader.load();
 					    Scene newScene = new Scene(root);
 	                    Stage newStage = new Stage();
 	                    
 					} catch (Exception e) {
 						// TODO Auto-generated catch block
 						e.printStackTrace();
 					}
 					 Stage stage = (Stage) imgBtAbsences.getScene().getWindow();
 			         // do what you have to do
 			         stage.close();
                 
 	             } else {
 	               
 	             }
 	                                                 
 	                    
 	                                        });
 	                             
 	                         
 	                         
 	                         }
 	                         
 	                         
 	                      
 	                             
 	                           
 	                          
 	                           
 	                            
 	                           
 	                           
 	                       
 	                    
 	                       }
 	                       
 	                     
 	                
 	               };
 	               
 	               
 	            return cell;    
 	           }
 	            
 	          
 	       
 	                });
 	        vacationList.setItems(data);
 	 		
 	 		
 	 	}
     }
    
}
