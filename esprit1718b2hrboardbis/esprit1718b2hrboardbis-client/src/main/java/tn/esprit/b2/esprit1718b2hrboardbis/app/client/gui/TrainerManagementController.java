package tn.esprit.b2.esprit1718b2hrboardbis.app.client.gui;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.TrainingServiceRemote;

/**
 * FXML Controller class
 *
 * @author kadhem
 */
public class TrainerManagementController implements Initializable {

    @FXML
    private ImageView imgBtHome;
    @FXML
    private ImageView imgbtProjects;
    @FXML
    private ImageView imgBtTrainig;
    @FXML
    private ImageView imgBtVac;
    @FXML
    private ImageView imgBtAbsences;
    @FXML
    private ImageView imgBtProfile;
    @FXML
    private ImageView imgBtCommunication;
    @FXML
    private ImageView imgBtSalaire;
    @FXML
    private ImageView imgBtlogout;
    @FXML
    private ImageView imgHi;
    @FXML
    private ImageView headimg;
    @FXML
    private ImageView logoImg;
    @FXML
    private TableView<Trainer> table;
    @FXML
    private TableColumn<Trainer, String> name;
    @FXML
    private TableColumn<Trainer, String> familyName;
    @FXML
    private TableColumn<Trainer, String> email;
    @FXML
    private TableColumn<Trainer, String> domain;
    @FXML
    private ImageView logo_delete;
    @FXML
    private  ImageView logo_add;
    @FXML
    private ImageView update;
  
    private ObservableList<Trainer> data;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         Image img = new Image("assets/home.png");
         imgBtHome.setImage(img);
         img = new Image("assets/hrhead.jpg");
         headimg.setImage(img);
         img = new Image("assets/M2A.png");
         logoImg.setImage(img);
         img = new Image("assets/projectmenu.png");
         imgbtProjects.setImage(img);
         img = new Image("assets/lecture.png");
         imgBtTrainig.setImage(img);
         img = new Image("assets/luggage.png");
         imgBtVac.setImage(img);
         img = new Image("assets/calendar.png");
         imgBtAbsences.setImage(img);
         img = new Image("assets/users.png");
         imgBtProfile.setImage(img);
         img = new Image("assets/chatting.png");
        imgBtCommunication.setImage(img);
         img = new Image("assets/salary.png");
         imgBtSalaire.setImage(img);
          img = new Image("assets/logout.png");
         imgBtlogout.setImage(img);
          img = new Image("assets/hi.png");
         imgHi.setImage(img);
          img = new Image("assets/newspape.png");
          logo_delete.setImage(new Image("assets/logo_delete.png"));
          logo_add.setImage(new Image("assets/logo_add.png"));
         update.setImage(new Image("assets/update.png"));
///////////////////////////////////////////
try {
	Context context = new InitialContext();
	TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");

	List<Trainer> trainers= new ArrayList<Trainer>();
	trainers=trainerServiceRemote.findAll();

	///////////////////////////////////////////
	///////////////////////////////////////////
	table.setEditable(false);
	data = FXCollections.observableArrayList();
	for ( int i = 0; i<table.getItems().size(); i++) 
	{
	table.getItems().clear();
	}
	name.setCellValueFactory(new PropertyValueFactory<>("name"));
	familyName.setCellValueFactory(new PropertyValueFactory<>("familyName"));
	email.setCellValueFactory(new PropertyValueFactory<>("email"));
	domain.setCellValueFactory(new PropertyValueFactory<>("domain"));

	for(Trainer t:trainers){
	t.setName(t.getName());
	t.setFamilyName(t.getFamilyName());
	t.setEmail(t.getEmail());
	t.setDomain(t.getDomain());
	data.add(t);
	t=null;
	}
	table.setItems(data);

} catch (NamingException e) {
	e.printStackTrace();
	}
	///////////////////////////////////////////  
	    }
       @FXML
    public void hometInterface() throws IOException { 
     FXMLLoader loader = new FXMLLoader(getClass().getResource("HomeHrManager.fxml"));
         Parent root = (Parent) loader.load();
         
         Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
         
         Scene newScene = new Scene(root);
         Stage newStage = new Stage();
         newStage.setScene(newScene);
         newStage.show();
    
    }    

    @FXML
    private void add(ActionEvent event) throws IOException {
    	Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("addTrainer.fxml"));
        Parent root = (Parent) loader.load();
        Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }

    @FXML
    private void delete(ActionEvent event) throws NamingException {
    	Context context = new InitialContext();
    	TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");
    	SeanceServiceRemote seanceServiceRemote = (SeanceServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/SeanceService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.SeanceServiceRemote");
    	seanceServiceRemote.deleteWhenTrainerDeleted(trainerServiceRemote.find(getIdTrainerFromTable()));
    	trainerServiceRemote.delete(trainerServiceRemote.find(getIdTrainerFromTable()));
		refreshTable();
    }

    @FXML
    private void update(ActionEvent event) throws IOException, NamingException {
    	
    	Context context = new InitialContext();
    	TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");
		
		
		Trainer trainer=trainerServiceRemote.find(getIdTrainerFromTable());
		
		Stage stage =(Stage)imgBtAbsences.getScene().getWindow();
	  	  stage.close();
	  	  
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("updateTrainer.fxml"));
        Parent root = (Parent) loader.load();
        
 		UpdateTrainerController ctrl=loader.<UpdateTrainerController>getController();
 		ctrl.setText(trainer.getName(),trainer.getFamilyName(),trainer.getEmail(),trainer.getPhoneNumber(),trainer.getDomain(),getIdTrainerFromTable());
 		Scene newScene = new Scene(root);
        Stage newStage = new Stage();
        newStage.setScene(newScene);
        newStage.show();
    }
    
    private int getIdTrainerFromTable() {
        table.setOnMousePressed(new  EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                int id=table.getSelectionModel().getSelectedItem().getId();
            }
            });
      return table.getSelectionModel().getSelectedItem().getId();
    }
    
    public void refreshTable() throws NamingException{
    	Context context = new InitialContext();
    	TrainerServiceRemote trainerServiceRemote = (TrainerServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/TrainerService!"
    			+ "tn.esprit.b2.esprit1718b2hrboardbis.services.TrainerServiceRemote");

    	List<Trainer> trainers= new ArrayList<Trainer>();
    	trainers=trainerServiceRemote.findAll();

    	///////////////////////////////////////////
    	///////////////////////////////////////////
    	table.setEditable(false);
    	data = FXCollections.observableArrayList();
    	for ( int i = 0; i<table.getItems().size(); i++) 
    	{
    	table.getItems().clear();
    	}
    	name.setCellValueFactory(new PropertyValueFactory<>("name"));
    	familyName.setCellValueFactory(new PropertyValueFactory<>("familyName"));
    	email.setCellValueFactory(new PropertyValueFactory<>("email"));
    	domain.setCellValueFactory(new PropertyValueFactory<>("domain"));

    	for(Trainer t:trainers){
    	t.setName(t.getName());
    	t.setFamilyName(t.getFamilyName());
    	t.setEmail(t.getEmail());
    	t.setDomain(t.getDomain());
    	data.add(t);
    	t=null;
    	}
    	table.setItems(data);
	}
    
   
}
