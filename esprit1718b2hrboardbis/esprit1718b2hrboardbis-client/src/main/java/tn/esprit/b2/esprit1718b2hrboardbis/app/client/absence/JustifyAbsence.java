package tn.esprit.b2.esprit1718b2hrboardbis.app.client.absence;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.hibernate.Hibernate;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Justification;
import tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote;
import tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote;

public class JustifyAbsence {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		Context context = new InitialContext();
		AbsenceServiceRemote absenceServiceRemote= (AbsenceServiceRemote)context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/AbsenceService!tn.esprit.b2.esprit1718b2hrboardbis.services.AbsenceServiceRemote");
		Absence absence =absenceServiceRemote.find(1);
		EmployeeServiceRemote employeeServiceRemote = (EmployeeServiceRemote) context.lookup("esprit1718b2hrboardbis-ear/esprit1718b2hrboardbis-service/EmployeeService!"
				+ "tn.esprit.b2.esprit1718b2hrboardbis.services.EmployeeServiceRemote");
	   Employee employee=	employeeServiceRemote.find(absence.getEmployee().getId());
		absence.setEmployee(employee);
		System.out.println(employee.getId());
		absence.setJustified(Justification.JUSTIFIED); 
        System.out.println();

		absenceServiceRemote.mergeAbsence(absence);
		
	}

}
