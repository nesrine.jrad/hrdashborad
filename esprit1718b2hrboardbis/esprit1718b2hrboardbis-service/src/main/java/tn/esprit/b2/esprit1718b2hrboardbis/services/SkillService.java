package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class SkillService
 */
@Stateless
@LocalBean
public class SkillService extends GenericDAO<Skill> implements SkillServiceRemote, SkillServiceLocal {
	@PersistenceContext
	EntityManager entityManager;
    /**
     * Default constructor. 
     */
    public SkillService() {
    	super(Skill.class);
    }
    public List<Skill> findJobOfferSkill(JobOffer jobOffer)
    {List<Skill> skills= new ArrayList<Skill>();
    	skills=entityManager.createQuery("select s from skill s where s.joboffer=:parm1").setParameter("param1", jobOffer).getResultList();
    return skills;
    }

}
