package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class InterviewServices
 */
@Stateless
@LocalBean
public class InterviewServices extends GenericDAO<Interview> implements InterviewServicesRemote, InterviewServicesLocal {
	@PersistenceContext
	private EntityManager entityManager;
    /**
     * Default constructor. 
     */
    public InterviewServices() {
super(Interview.class);    }
    
    public void deleteInterview(Interview interview){
    	String deleteQuery = "DELETE from Interview t where t.id=:idInterview";
		entityManager.createQuery(deleteQuery ).
		setParameter("idInterview", interview.getId()).
		executeUpdate();
    }
    

}
