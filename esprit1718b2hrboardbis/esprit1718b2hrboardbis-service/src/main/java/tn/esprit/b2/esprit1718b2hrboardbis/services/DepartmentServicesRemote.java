package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface DepartmentServicesRemote extends IGenericDAO<Department>{
		public List<Department> getEmployeeByManagerDepartementId(int i);
		public List<String> getName();
}
