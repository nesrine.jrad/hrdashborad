package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.Stateless;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.AdditionalHour;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class AdditionalHourService
 */
@Stateless
public class AdditionalHourService extends GenericDAO<AdditionalHour> implements AdditionalHourServiceRemote, AdditionalHourServiceLocal {

    /**
     * Default constructor. 
     */
    public AdditionalHourService() {
    	super(AdditionalHour.class);
    }

}
