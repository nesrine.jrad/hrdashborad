package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class DepartmentServices
 */
@Stateless
@LocalBean
public class DepartmentServices extends GenericDAO<Department>implements DepartmentServicesRemote, DepartmentServicesLocal {
	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	EmployeeService employeeService;
    /**
     * Default constructor. 
     */
	
    public DepartmentServices() {
super(Department.class);    }
    
    public List<Department> getEmployeeByManagerDepartementId(int i)
    {     Employee employee= employeeService.find(i);
       
    	 return entityManager.createQuery("select a.id from Department d join d.employees a where a.id = :param").setParameter("param", employee.getId()).getResultList();
    	
    	
    	
    }
    
public List<String> getName()
{
	return entityManager.createQuery("select d.name from Department d").getResultList();
}

}
