package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Status;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskPK;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class ProjectService
 */
@Stateless
@LocalBean
public class ProjectService extends GenericDAO<Project> implements ProjectServiceRemote, ProjectServiceLocal {
	@EJB
	TaskServiceLocal taskServiceLocal;
	@EJB
	EmployeeServiceLocal employeeServiceLocal;
	@PersistenceContext
	EntityManager entityManager;
    /**
     * Default constructor. 
     */
    public ProjectService() {
        super(Project.class);
    }
	@Override
	public void delete(Project entity) {
		List<Task> tasks = entity.getTasks();
		for(Task task : tasks)
		{			
			taskServiceLocal.delete(taskServiceLocal.find(task.getTaskPk()));
		}
		entity.setTasks(null);
		entityManager.remove(entityManager.merge(entity));
	}
	@Override
	public List<Task> getListTasksOfProjectByStatus(int idProject, StatusTask statusTask) {
		String queryStatement = "SELECT t from Task t where t.taskPk.idProject=:idProject and t.statusTask=:statusTask and t.taskPk.name!=''";
		List<Task> tasks = entityManager.createQuery(queryStatement).
				setParameter("idProject", idProject).
				setParameter("statusTask", statusTask).
				getResultList();
		return tasks;
	}
	@Override
	public List<Project> getProjectsByEmployee(int idEmployee) {
		Employee employee = employeeServiceLocal.find(idEmployee);
		String queryStatement = "SELECT Distinct t.project from Task t where t.taskPk.idEmployee=:idEmployee";
		List<Project> projects = new ArrayList<>();
		projects = entityManager.createQuery(queryStatement).setParameter("idEmployee", idEmployee).getResultList();
		return projects;
	}
	@Override
	public int findIdOfLastRecord() {
		String query="SELECT p FROM Project p order by p.id desc";
		List<Project> projects = entityManager.createQuery(query).setMaxResults(1).getResultList();
		if (projects.size()!=0){
			Project project = projects.get(0);
			int id = project.getId();
			return id;
		}
		return -1;
	}
	@Override
	public List<Employee> getListEmployees(int idProject) {
		String queryStatement = "SELECT Distinct t.employee from Task t where t.taskPk.idProject=:idProject";
		List<Employee> employees = new ArrayList<>();
		employees = entityManager.createQuery(queryStatement).setParameter("idProject", idProject).getResultList();
		return employees;
	}
	@Override
	public List<Project> getProjectsByStatus(Status status) {
		String queryStatement = "SELECT p from Project p where p.status=:status";
		List<Project> projects = new ArrayList<>();
		projects = entityManager.createQuery(queryStatement).setParameter("status", status).getResultList();
		return projects;
	}
	@Override
	public List<Project> getProjectsByEmployee(int idEmployee, Status status) {
		String queryStatement = "SELECT Distinct t.project from Task t JOIN t.project p "
				+ "WHERE t.taskPk.idProject=p.id "
				+ "and t.taskPk.idEmployee=:idEmployee "
				+ "and p.status=:status";
		List<Project> projects = new ArrayList<>();
		projects = entityManager.createQuery(queryStatement)
				.setParameter("status", status)
				.setParameter("idEmployee", idEmployee)
				.getResultList();
		return projects;
	}
	

    

    
}
