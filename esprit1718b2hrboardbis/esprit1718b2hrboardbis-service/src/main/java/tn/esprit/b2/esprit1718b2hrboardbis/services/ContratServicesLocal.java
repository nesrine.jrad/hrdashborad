package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.Local;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Contract;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface ContratServicesLocal extends IGenericDAO<Contract>{

}
