package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Status;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface ProjectServiceLocal extends IGenericDAO<Project>{
	public List<Task> getListTasksOfProjectByStatus(int idProject, StatusTask statusTask);
	public List<Project> getProjectsByEmployee(int idEmployee);
	public List<Project> getProjectsByEmployee(int idEmployee, Status status);
	public List<Project> getProjectsByStatus(Status status);
	public int findIdOfLastRecord();
	public List<Employee> getListEmployees(int idProject);
}
