package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

/**
 * Session Bean implementation class JobOfferServices
 */
@Stateless
@LocalBean
public class JobOfferServices extends GenericDAO<JobOffer> implements JobOfferServicesRemote, JobOfferServicesLocal {
@PersistenceContext
private EntityManager entityManager;
    /**
     * Default constructor. 
     */
    public JobOfferServices() {
    	 super(JobOffer.class);
    }
 
    public List<JobOffer> findJobOfferByName(String Name, String LastName)
	{	
		String query="Select e from JobOffer e where e.name=:Name and e.description=:LastName";
		return entityManager.createQuery(query).setParameter("Name", Name).setParameter("LastName", LastName).getResultList();
	
		
	}
}
