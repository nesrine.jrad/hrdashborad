package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskPK;

@Local
public interface TaskServiceLocal{
	public List<Task> getTasksByProject(Project project);
	public List<Task> getTasksByEmployeeAndStatusTask(int idEmployee, StatusTask statusTask);
	public List<Task> getTasksByEmployeeAndStatusTask(int idEmployee, StatusTask statusTask, int idProject);
	public Task find(TaskPK taskId);
	public void save(Task task);
	public void delete(Task task);
	public void updateName(Task task,String newName);
	public void updateEmployee(Task task, Employee newEmployee);
	public List<Task> findAll();
}
