package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface JobRequestServicesRemote extends IGenericDAO<JobRequest>{
	List<JobRequest> FindJobRequest(int id);
}
