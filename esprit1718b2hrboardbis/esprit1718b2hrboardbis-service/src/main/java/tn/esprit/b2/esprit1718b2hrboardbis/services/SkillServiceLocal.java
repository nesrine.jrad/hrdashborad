package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Skill;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface SkillServiceLocal extends IGenericDAO<Skill>{
	public List<Skill> findJobOfferSkill(JobOffer jobOffer);

}
