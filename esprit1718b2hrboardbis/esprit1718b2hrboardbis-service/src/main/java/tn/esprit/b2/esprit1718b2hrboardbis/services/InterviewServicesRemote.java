package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.Remote;
import javax.persistence.criteria.CriteriaBuilder.In;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Interview;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface InterviewServicesRemote extends IGenericDAO<Interview> {
	 public void deleteInterview(Interview interview);
}
