package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;

/**
 * Session Bean implementation class AssignEmployeesToProject
 */
@Stateful
@LocalBean
public class AssignEmployeesToProject implements AssignEmployeesToProjectRemote, AssignEmployeesToProjectLocal {
	@EJB
	EmployeeServiceLocal employeeServiceLocal;
	@EJB
	TaskServiceLocal taskServiceLocal;
	@EJB
	ProjectServiceLocal projectServiceLocal;
	private List<Employee> employees;
	
    /**
     * Default constructor. 
     */
    public AssignEmployeesToProject() {
        
    }
    @PostConstruct
    void init(){
    	employees = new ArrayList<>();
    }
    
    public void addEmployee(int idEmployee){
    	Employee employee = employeeServiceLocal.find(idEmployee);
    	employees.add(employee);
    }

    public void deleteEmlotee(Employee employee){
    	employees.remove(employee);
    }
	
    public List<Employee> getEmployees() {
		return employees;
	}
	
    public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
    
    public void validateEmployees(int idProject){
		Project project = projectServiceLocal.find(idProject);
		for (Employee employee : employees) {
			Task task = new Task(project, employee, " ", project.getDateOfBeginning()," ");
			//The employee get an empty task in order to assign it to this project
			task.setStatusTask(StatusTask.DONE);
			taskServiceLocal.save(task);
		}
		employees.removeAll(employees);
	}
}
