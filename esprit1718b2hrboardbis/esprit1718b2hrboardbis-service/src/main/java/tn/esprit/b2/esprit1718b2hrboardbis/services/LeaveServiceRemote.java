package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface LeaveServiceRemote extends IGenericDAO<Leave> {
	 public List<Leave> getLeavesByUserId(int id);
	 public void confirmLeave(int id);
	 public List<Leave> getLeaveByManagerDepartement(int id);
}
