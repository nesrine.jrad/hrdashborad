package tn.esprit.b2.esprit1718b2hrboardbis.services;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.sun.mail.util.QEncoderStream;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class EmployeeService
 */
@Stateless
@LocalBean
public class EmployeeService extends GenericDAO<Employee> implements EmployeeServiceRemote, EmployeeServiceLocal {

	@PersistenceContext
	private EntityManager entityManager;
    /**
     * Default constructor. 
     */
    public EmployeeService() {
    	super(Employee.class);
    }

	@Override
	public List<Employee> findEmployeesByRole(RoleEmployee roleEmployee) {
		String query = "SELECT e FROM Employee t Where e.roleEmployee=:roleEmployee";
		return entityManager.createQuery(query)
		.setParameter("roleEmployee", roleEmployee).getResultList();
	}
	
	@Override
	public List<Employee> findEmployees(String login,String pwd) {
		String query = "SELECT e FROM Employee e Where e.login=:param1 and e.password=:param2";
	 	return entityManager.createQuery(query)
				.setParameter("param1",login).setParameter("param2",pwd).getResultList();
	}
	@Override
	public List<Employee> findEmployeeByName(String Name, String LastName)
	{	
		String query="Select e from Employee e where e.name=:Name and e.familyName=:LastName";
		return entityManager.createQuery(query).setParameter("Name", Name).setParameter("LastName", LastName).getResultList();
	
		
	}
}
