package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Status;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface ProjectServiceRemote extends IGenericDAO<Project>{
	public List<Task> getListTasksOfProjectByStatus(int idProject, StatusTask statusTask);
	public List<Project> getProjectsByEmployee(int idEmployee);
	public List<Project> getProjectsByEmployee(int idEmployee, Status status);
	public int findIdOfLastRecord();
	public List<Project> getProjectsByStatus(Status status);
	//Returns the list of employees in a specific project
	public List<Employee> getListEmployees(int idProject);

}
