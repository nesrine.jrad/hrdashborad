package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface TrainingServiceRemote extends IGenericDAO<Training>{
	public List<String> findAllTitles();
}
