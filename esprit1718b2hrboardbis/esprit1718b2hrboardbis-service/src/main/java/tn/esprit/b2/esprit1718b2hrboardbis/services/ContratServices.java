package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Contract;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class ContratServices
 */
@Stateless
@LocalBean
public class ContratServices extends GenericDAO<Contract> implements ContratServicesRemote, ContratServicesLocal {

    /**
     * Default constructor. 
     */
    public ContratServices() {
    	super(Contract.class);
        // TODO Auto-generated constructor stub
    }

}
