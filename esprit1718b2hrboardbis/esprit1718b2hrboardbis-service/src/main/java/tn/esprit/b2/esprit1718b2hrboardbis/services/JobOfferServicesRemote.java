package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface JobOfferServicesRemote extends IGenericDAO<JobOffer> {
	 public List<JobOffer> findJobOfferByName(String Name, String LastName);

}
