package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.SeancePk;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface SeanceServiceLocal extends IGenericDAO<Seance>{
	public void deleteWhenTrainerDeleted(Trainer trainer);
	public void deleteWhenTrainingDeleted(Training training);
	public void deleteMyWay(SeancePk seance);
	public List<Seance>  findByTraining(Training training);
	public List<Seance>  findByEmployee(int id);
}
