package tn.esprit.b2.esprit1718b2hrboardbis.services;
import java.util.List;

import javax.ejb.Local;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface TrainerServiceLocal extends IGenericDAO<Trainer> {
	public List<String>findAllDomains();
}
