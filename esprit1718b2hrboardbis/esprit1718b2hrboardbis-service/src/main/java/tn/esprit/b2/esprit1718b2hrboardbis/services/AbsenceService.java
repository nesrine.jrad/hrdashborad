package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.sql.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobOffer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Justification;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class AbsenceService
 */
@Stateless
public class AbsenceService extends GenericDAO<Absence> implements AbsenceServiceRemote, AbsenceServiceLocal {
	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	EmployeeService employeeService;
    /**
     * Default constructor. 
     */
    public AbsenceService() {
    	 super(Absence.class);
    }
    
    public void insertAbsence(List<Absence> absences){
    	
    	for (Absence absence : absences) {
			this.save(absence);
		}
    }
    	
    	  public List<Absence> getAbsenceByUserId(int id){
    	    	 Employee employee= employeeService.find(id);
    	    
    		  return entityManager.createQuery("SELECT a FROM Absence a WHERE a.employee = :param").setParameter("param", employee).getResultList();
    	
    	
    }
    	  public void mergeAbsence(Absence absence){
    	    	entityManager.merge(absence);
    	    }
    	  
    	  
    	  public List<Absence> getAbsencebyDepartement(int id){
			
    		  return null;
    		  
    		  
    	  }
    	  
    	  
    	  public List<Absence> getNonJustifiedAbsenceByUserId(int id){
    		  Employee employee= employeeService.find(id);
 	    	
 		  List<Absence> absences=  entityManager.createQuery("SELECT a FROM Absence a WHERE a.employee = :param").setParameter("param", employee).getResultList();
    		for (Absence absence : absences) {
				if(absence.getJustified()!=Justification.NOT_JUSTIFIED){
					absences.remove(absence);
				
				}
			}
 		  return absences ;
    	  }

    	  public List<Absence> getJustifiedAbsenceByUserId(int id){
    		  Employee employee= employeeService.find(id);
 	 
 		  List<Absence> absences=  entityManager.createQuery("SELECT a FROM Absence a WHERE a.employee = :param").setParameter("param", employee).getResultList();
    		for (Absence absence : absences) {
				if(absence.getJustified()!=Justification.JUSTIFIED){
					absences.remove(absence);
				
				}
			}
 		  return absences ;
    	  }
}
