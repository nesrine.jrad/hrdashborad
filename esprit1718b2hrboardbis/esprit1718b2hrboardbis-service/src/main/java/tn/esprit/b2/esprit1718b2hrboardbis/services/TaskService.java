package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Project;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.StatusTask;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.TaskPK;

/**
 * Session Bean implementation class TaskService
 */
@Stateless
@LocalBean
public class TaskService implements TaskServiceRemote, TaskServiceLocal {
	
	@PersistenceContext
	private EntityManager entityManager;
    /**
     * Default constructor. 
     */
    public TaskService() {
    }
	@Override
	public List<Task> getTasksByProject(Project project) {
			String query = "SELECT t FROM Task t Where t.taskPk.idProject=:projectId and t.taskPk.name!=''";
			return entityManager.createQuery(query)
			.setParameter("projectId", project.getId()).getResultList();
			
		
	}
	
	@Override
	public Task find(TaskPK taskId) {
		return entityManager.find(Task.class, taskId);
	}
	@Override
	public void delete(Task task) {
		
		String deleteQuery = "DELETE from Task t where t.taskPk.idProject=:project and t.taskPk.idEmployee=:"
				+ "employee and t.taskPk.name=:name";
		entityManager.createQuery(deleteQuery ).
		setParameter("project", task.getProject().getId()).
		setParameter("employee", task.getEmployee().getId()).
		setParameter("name", task.getTaskPk().getName()).
		executeUpdate();
		//entityManager.remove(task);
		
	}
		
	@Override
	public void save(Task task) {
		entityManager.merge(task);
		
	}
	@Override
	public void updateName(Task task, String newName) {
		String updateQuery = "UPDATE from Task t SET t.taskPk.name=:newName where t.taskPk.idProject=:idProject and t.taskPk.idEmployee=:idEmployee and t.taskPk.name=:oldName";
		entityManager.createQuery(updateQuery).setParameter("newName", newName).setParameter("idProject", task.getProject().getId()).setParameter("idEmployee", task.getEmployee().getId()).setParameter("oldName", task.getTaskPk().getName()).executeUpdate();
		
	}
	@Override
	public void updateEmployee(Task task, Employee newEmployee) {
		String updateQuery = "UPDATE from Task t SET t.taskPk.idEmployee=:newEmployeeId where t.taskPk.idProject=:idProject and t.taskPk.idEmployee=:oldEmployee and t.taskPk.name=:name";
		entityManager.createQuery(updateQuery).setParameter("newEmployeeId", newEmployee.getId()).setParameter("idProject", task.getProject().getId()).setParameter("oldEmployee",  task.getEmployee().getId()).setParameter("name", task.getTaskPk().getName()).executeUpdate();
	}
	
	@Override
	public List<Task> findAll() {
		CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Task.class));
		return entityManager.createQuery(cq).getResultList();
	}
	@Override
	public List<Task> getTasksByEmployeeAndStatusTask(int idEmployee, StatusTask statusTask) {
		String query = "SELECT t FROM Task t Where t.taskPk.idEmployee=:idEmployee and t.taskPk.name!='' and t.statusTask=:statusTask";
		return entityManager.createQuery(query)
		.setParameter("idEmployee", idEmployee)
		.setParameter("statusTask", statusTask)
		.getResultList();
	}
	@Override
	public List<Task> getTasksByEmployeeAndStatusTask(int idEmployee, StatusTask statusTask, int idProject) {
		String query = "SELECT t FROM Task t Where t.taskPk.idProject=:idProject and t.taskPk.idEmployee=:idEmployee and t.taskPk.name!='' and t.statusTask=:statusTask";
		return entityManager.createQuery(query)
		.setParameter("idProject", idProject)
		.setParameter("idEmployee", idEmployee)
		.setParameter("statusTask", statusTask)
		.getResultList();
	}
	
	
	
	
    
    
    

}
