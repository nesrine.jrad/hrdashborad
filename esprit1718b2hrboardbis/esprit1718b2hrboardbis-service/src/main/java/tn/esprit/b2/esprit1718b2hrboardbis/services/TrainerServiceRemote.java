package tn.esprit.b2.esprit1718b2hrboardbis.services;
import java.util.List;

import javax.ejb.Remote;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface TrainerServiceRemote  extends IGenericDAO<Trainer>{
	public List<String>findAllDomains();
}
