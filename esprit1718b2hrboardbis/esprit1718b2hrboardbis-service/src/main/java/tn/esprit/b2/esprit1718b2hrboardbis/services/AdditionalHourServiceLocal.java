package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.Local;


import tn.esprit.b2.esprit1718b2hrboardbis.entities.AdditionalHour;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface AdditionalHourServiceLocal extends IGenericDAO<AdditionalHour> {

}
