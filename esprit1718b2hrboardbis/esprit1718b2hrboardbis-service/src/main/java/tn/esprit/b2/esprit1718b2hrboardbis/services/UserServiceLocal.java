package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.Local;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.User;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface UserServiceLocal extends IGenericDAO<User> {

	User login(String login, String password);

}
