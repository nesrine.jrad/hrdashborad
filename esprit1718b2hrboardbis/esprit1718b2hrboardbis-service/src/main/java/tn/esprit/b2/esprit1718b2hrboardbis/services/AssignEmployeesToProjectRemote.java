package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;

@Remote
public interface AssignEmployeesToProjectRemote {
	public void addEmployee(int idEmployee);
	public void deleteEmlotee(Employee employee);
	public List<Employee> getEmployees();
	public void setEmployees(List<Employee> employees);
	public void validateEmployees(int idProject);
}
