package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class TrainingService
 */
@Stateless
@LocalBean
public class TrainingService extends GenericDAO<Training> implements TrainingServiceRemote, TrainingServiceLocal {

	public TrainingService() {
    	super(Training.class);
    }
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<String> findAllTitles() {
		String query = "SELECT t.title FROM Training t";
		return entityManager.createQuery(query).getResultList();
	}

	

}
