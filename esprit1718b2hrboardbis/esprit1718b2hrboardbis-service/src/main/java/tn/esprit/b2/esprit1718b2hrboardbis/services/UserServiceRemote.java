package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.User;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface UserServiceRemote extends IGenericDAO<User> {

}
