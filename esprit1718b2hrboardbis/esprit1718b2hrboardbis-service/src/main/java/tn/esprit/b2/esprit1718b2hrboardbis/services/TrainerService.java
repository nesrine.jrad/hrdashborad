package tn.esprit.b2.esprit1718b2hrboardbis.services;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class TrainerService
 */
@Stateless
@LocalBean
public class TrainerService extends GenericDAO<Trainer> implements TrainerServiceRemote, TrainerServiceLocal {

    /**
     * Default constructor. 
     */
	
	@PersistenceContext
	private EntityManager entityManager;
	
    public TrainerService() {
    	 super(Trainer.class);
    }

	@Override
	public List<String> findAllDomains() {
			String query = "SELECT t.domain FROM Trainer t";
			return entityManager.createQuery(query).getResultList();
	}
	
}
