package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.RoleEmployee;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface EmployeeServiceRemote extends IGenericDAO<Employee> {
	List<Employee> findEmployeesByRole(RoleEmployee roleEmployee);
	public List<Employee> findEmployees(String login,String pwd) ;	
	List<Employee> findEmployeeByName(String Name, String LastName);
}
