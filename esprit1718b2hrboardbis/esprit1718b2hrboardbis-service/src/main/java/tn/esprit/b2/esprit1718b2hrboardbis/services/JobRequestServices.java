package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.JobRequest;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class JobRequestServices
 */
@Stateless
@LocalBean
public class JobRequestServices extends GenericDAO<JobRequest> implements JobRequestServicesRemote, JobRequestServicesLocal {
@PersistenceContext
EntityManager em;
    /**
     * Default constructor. 
     */
    public JobRequestServices() {
    	super(JobRequest.class);

    }
    public List<JobRequest> FindJobRequest(int id)
    {
    	//List<JobRequest> jobRequests=new ArrayList<JobRequest>();
    	TypedQuery<JobRequest> query=em.createQuery("select j.lastNameCandidate  from jobrequest j where j.jobOffer_id=:id" ,JobRequest.class);
    	query.setParameter(id, id);
    	return query.getResultList();
    }
};
