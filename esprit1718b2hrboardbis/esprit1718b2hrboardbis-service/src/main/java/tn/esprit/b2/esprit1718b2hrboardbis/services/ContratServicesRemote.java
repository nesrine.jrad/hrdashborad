package tn.esprit.b2.esprit1718b2hrboardbis.services;

import javax.ejb.Remote;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Contract;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Remote
public interface ContratServicesRemote extends IGenericDAO<Contract> {

}
