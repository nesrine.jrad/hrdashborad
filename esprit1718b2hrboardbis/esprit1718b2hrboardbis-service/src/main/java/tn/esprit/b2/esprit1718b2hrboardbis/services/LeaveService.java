package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Department;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Employee;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Leave;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;

/**
 * Session Bean implementation class LeaveService
 */
@Stateless
public class LeaveService extends GenericDAO<Leave> implements LeaveServiceRemote, LeaveServiceLocal {
	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	EmployeeService employeeService;
	@EJB 
	DepartmentServices departmentServices;
	
    /**
     * Default constructor. 
     */
    public LeaveService() {
    super(Leave.class);
    
}
    public List<Leave> getLeavesByUserId(int id){
   	 Employee employee= employeeService.find(id);
   	
	  return entityManager.createQuery("SELECT l FROM Leave l WHERE l.employee = :param").setParameter("param", employee).getResultList();


}
    
    public void confirmLeave(int id){
    	Leave leave= find(id);
		leave.setApproved(true);
		update(leave);
    	
    }
    
    public List<Leave> getLeaveByManagerDepartement(int id){
    	List<Leave> leaveList= new ArrayList<>();
    	List<Department> departments = departmentServices.findAll();
		for (Department department : departments) {
			
			   for (Employee employee : department.getEmployees()) {
				   
				   if(employee.getId()==id)
					  
				   leaveList.addAll(getLeavesByUserId(employee.getId()));
			}
			   
		}
		return leaveList;
    	
    	
    	
    }

}
