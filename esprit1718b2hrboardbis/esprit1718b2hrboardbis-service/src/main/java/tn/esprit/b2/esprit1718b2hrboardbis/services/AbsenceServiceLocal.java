package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Absence;

import tn.esprit.b2.esprit1718b2hrboardbis.utilities.IGenericDAO;

@Local
public interface AbsenceServiceLocal extends IGenericDAO<Absence> {
	public void insertAbsence(List<Absence> absences);
	List<Absence> getAbsenceByUserId(int id);
	 public void mergeAbsence(Absence absence);
	 public List<Absence> getJustifiedAbsenceByUserId(int id);
	 public List<Absence> getNonJustifiedAbsenceByUserId(int id);
}
