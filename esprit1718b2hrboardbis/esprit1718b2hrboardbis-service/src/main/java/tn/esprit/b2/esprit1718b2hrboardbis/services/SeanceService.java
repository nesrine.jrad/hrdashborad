package tn.esprit.b2.esprit1718b2hrboardbis.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.b2.esprit1718b2hrboardbis.entities.Seance;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.SeancePk;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Task;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Trainer;
import tn.esprit.b2.esprit1718b2hrboardbis.entities.Training;
import tn.esprit.b2.esprit1718b2hrboardbis.utilities.GenericDAO;
/**
 * Session Bean implementation class SeanceService
 */
@Stateless
@LocalBean
public class SeanceService extends GenericDAO<Seance>implements SeanceServiceRemote, SeanceServiceLocal {
	@PersistenceContext
	private EntityManager entityManager;
	
	public SeanceService() {
   	 super(Seance.class);
   }

	@Override
	public void deleteWhenTrainerDeleted(Trainer trainer) {
			String deleteQuery = "DELETE from Seance t where t.seancePk.idTrainer=:idTrainer";
			entityManager.createQuery(deleteQuery ).
			setParameter("idTrainer", trainer.getId()).
			executeUpdate();
	}

	@Override
	public void deleteWhenTrainingDeleted(Training training) {
		String deleteQuery = "DELETE from Seance t where t.seancePk.idTraining=:idTraining";
		entityManager.createQuery(deleteQuery ).
		setParameter("idTraining", training.getId()).
		executeUpdate();
	}

	@Override
	public void deleteMyWay(SeancePk seance) {
		String deleteQuery = "DELETE from Seance t where t.seancePk.idTrainer=:idTrainer and t.seancePk.idEmployee=:"
				+ "idEmployee and t.seancePk.idTraining=:idTraining and t.seancePk.dateSeance=:date";
		entityManager.createQuery(deleteQuery ).
		setParameter("idTrainer", seance.getIdTrainer()).
		setParameter("idEmployee", seance.getIdEmployee()).
		setParameter("idTraining", seance.getIdTraining()).
		setParameter("date", seance.getDateSeance()).
		executeUpdate();
		//entityManager.remove(task);
	}

	@Override
	public List<Seance> findByTraining(Training training) {
		String query = "SELECT t FROM Seance t Where t.seancePk.idTraining=:idTraining";
		return entityManager.createQuery(query)
		.setParameter("idTraining", training.getId())
		.getResultList();
	}

	@Override
	public List<Seance> findByEmployee(int id) {
		String query = "SELECT t FROM Seance t Where t.seancePk.idEmployee=:idEmployee";
		return entityManager.createQuery(query)
		.setParameter("idEmployee", id)
		.getResultList();
	}
}

